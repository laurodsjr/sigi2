$sourcePath = "D:\projects\sigi2\TesteDeMesa"
$destinationPath = "R:\Lauro\SiGI2\TesteDeMesa"

# Verificar se o diretório de origem existe
if (-not (Test-Path $sourcePath)) {
    Write-Host "O diretório de origem não foi encontrado."
    exit
}

# Verificar se o diretório de destino existe, caso contrário, criá-lo
if (-not (Test-Path $destinationPath)) {
    New-Item -ItemType Directory -Path $destinationPath | Out-Null
}

# Obter todos os arquivos do diretório de origem, excluindo a pasta "node_modules" e o arquivo "commit-teste-de-mesa.ps1"
$files = Get-ChildItem $sourcePath -Recurse -File | Where-Object {
    $_.FullName -notlike "*\node_modules\*" -and $_.Name -ne "commit-teste-de-mesa.ps1"
}

# Copiar cada arquivo para o diretório de destino
foreach ($file in $files) {
    $destinationFile = Join-Path $destinationPath $file.FullName.Substring($sourcePath.Length + 1)
    $destinationDirectory = Split-Path $destinationFile -Parent

    # Verificar se o diretório de destino existe, caso contrário, criá-lo
    if (-not (Test-Path $destinationDirectory)) {
        New-Item -ItemType Directory -Path $destinationDirectory | Out-Null
    }

    # Copiar o arquivo
    Copy-Item $file.FullName -Destination $destinationFile -Force
}

Write-Host "A cópia dos arquivos foi concluída."
