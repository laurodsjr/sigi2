import { InitialSigi2 } from './modules/hello-world/InitialSigi2.js';
import { modulos } from './modules/ativacao-de-modulos.js';
import { DB, InitDB } from './modules/migrations/init-indexeddb.js';
import { InserirFuncaoDeUsuario } from './modules/migrations/usuarios/adiciona-funcoes-de-usuario.js';
import { InserirUsuario } from './modules/migrations/usuarios/adciona-usuario.js';
import { CarregaTiposDeObservacoes } from './modules/migrations/observacoes-de-solicitacoes/tipos.js';
import { CarregaTiposDeSolicitacoes } from './modules/migrations/solicitacoes/tipos.js';
import {OrigensDeSolicitacoes} from './modules/migrations/solicitacoes/origem.js';
import { StatusDeSolicitacoes } from './modules/migrations/solicitacoes/status.js';
import { StatusDeReunioes } from './modules/migrations/reunioes/status.js';
import { StatusDeVisitasTecnicas } from './modules/migrations/visitas_tecnicas/status.js';
import { TiposDeOrcamento } from './modules/migrations/orcamentos/tipos.js';
import { StatusDeOrcamento } from './modules/migrations/orcamentos/status.js';
import { CarregaOrigensDeSolicitacoes } from './modules/migrations/solicitacoes/carrega-origens.js';
import { CarregaTiposDeDocSolicitacoes } from './modules/migrations/solicitacoes/carrega-tipos-de-documento.js';
import { CadastraSolicitacao } from './modules/migrations/solicitacoes/cadastra-solicitacao.js';
import { CarregaDadosDeSolicitacao } from './modules/migrations/solicitacoes/carrega-dados-de-solicitacao.js';
import { EditaSolicitacao } from './modules/migrations/solicitacoes/edita-solicitacao.js';
import { ExcluirSolicitacao } from './modules/migrations/solicitacoes/excluir-solicitacao.js';
import { CarregaStatusDeSolicitacoes } from './modules/migrations/solicitacoes/carrega-status.js';
import { FiltrarListagemSolicitacoes } from './modules/migrations/solicitacoes/filtrar-listagem.js';
import { CarregaSolicitacoes } from './modules/migrations/comum/carrega-solicitacoes.js';
import { CadastraEmpreendimento } from './modules/migrations/empreendimentos/cadastra-empreendimento.js';
import { ListagemEmpreendimentos } from './modules/migrations/empreendimentos/listagem.js';
import { FiltrarListagemEmpreendimentos } from './modules/migrations/empreendimentos/filtrar-listagem.js';
import { CarregaDadosDeEmpreedimento } from './modules/migrations/empreendimentos/carrega-dados-de-empreendimento.js';
import { EditaEmpreendimento } from './modules/migrations/empreendimentos/edita-empreendimento.js';
import { ExcluirEmpreendimento } from './modules/migrations/empreendimentos/excluir-empreendimento.js';
import { SelectBaseadoEmTipo } from './modules/migrations/observacoes-de-solicitacoes/selecionador-baseado-no-tipo.js';
import { ListaDeOpcoes } from './modules/migrations/observacoes-de-solicitacoes/lista-de-opcoes.js';
import { CadastraObservacao } from './modules/migrations/observacoes-de-solicitacoes/cadastra-observacao.js';
import { FiltrarListagemObservacoes } from './modules/migrations/observacoes-de-solicitacoes/filtrar-listagem.js';
import { ListagemObservacoes } from './modules/migrations/observacoes-de-solicitacoes/listagem.js';
import { CarregaDadosDeObservacao } from './modules/migrations/observacoes-de-solicitacoes/carrega-dados-de-observacao.js';
import { CarregaSolicitacoesDeReuniao } from './modules/migrations/solicitacoes-de-reuniao/carrega-solicitacoes-de-reuniao.js';
import { CadastraSolicitacaoDeReuniao } from './modules/migrations/solicitacoes-de-reuniao/cadastra-solicitacao-de-reuniao.js';
import { ListagemSolicitacoesDeReunioes } from './modules/migrations/solicitacoes-de-reuniao/listagem.js';
import { FiltrarListagemSolicitacoesDeReunioes } from './modules/migrations/solicitacoes-de-reuniao/filtrar-listagem.js';
import { CarregaDadosDeSolicitacaoDeReuniao } from './modules/migrations/solicitacoes-de-reuniao/carrega-dados-de-solicitacao-de-reuniao.js';
import { EditaSolicitacaoDeReuniao } from './modules/migrations/solicitacoes-de-reuniao/edita-solicitacao-de-reuniao.js';
import { ExcluirSolicitacaoDeReuniao } from './modules/migrations/solicitacoes-de-reuniao/excluir-solicitacao-de-reuniao.js';
import { InsereStatusDeContrato } from './modules/migrations/contratos/insere-status-de-contrato.js';
import { InsereDepartamentosIniciais } from './modules/migrations/contratos/insere-departamentos.js';
import { CarregaDepartamentos } from './modules/migrations/contratos/carrega-departamentos.js';
import { CarregaPrestadoresDeServicos } from './modules/migrations/contratos/carrega-prestadores-de-servicos.js';
import { CarregaAtividades } from './modules/migrations/contratos/carrega-atividades.js';
import { CadastraContrato } from './modules/migrations/contratos/cadastra-contrato.js';
import { ListagemContratos } from './modules/migrations/contratos/listagem.js';
import { ExcluirContrato } from './modules/migrations/contratos/excluir-contrato.js';
import { LimitadorDeTextarea } from './modules/migrations/contratos/limitador-de-textarea.js';
import { CarregaNumerosDeContratoDoSAP } from './modules/migrations/contratos/carrega-dados-de-numeros-de-contrato-sap.js';
import { CarregaDadosDoContrato } from './modules/migrations/contratos/carrega-dados-de-contrato.js';
import { EditaContrato } from './modules/migrations/contratos/edita-contrato.js';
import { FiltrarListagemContratos } from './modules/migrations/contratos/filtrar-listagem.js';
import { CarregaLocais } from './modules/migrations/atividades/carrega-dados-de-locais.js';
import { CarregaObras } from './modules/migrations/atividades/carrega-dados-de-obras.js';
import { CarregaObrasComplemento } from './modules/migrations/atividades/carrega-dados-de-obras-complemento.js';
import { CadastraAtividade } from './modules/migrations/atividades/cadastra-atividade.js';
import { ListagemAtividades } from './modules/migrations/atividades/listagem.js';
import { ExcluirAtividade } from './modules/migrations/atividades/excluir-atividade.js';
import { CarregaDadosDaAtividade } from './modules/migrations/atividades/carrega-dados-de-atividade.js';
import { AtalhoObservacoesDoContrato } from './modules/migrations/contratos/atalho-observacoes-do-contrato.js';
import { CategoriasDeAEIC } from './modules/migrations/aeic/carrega-categorias-aeic.js';
import { CarregaListaDeContratos } from './modules/migrations/aeic/carrega-lista-de-contratos.js';
import { ListaReferenciasAEIC } from './modules/migrations/aeic/lista-referencias-aeic.js';
import { VerificaAtividadesPorContrato } from './modules/migrations/aeic/verifica-atividades-por-contrato.js';
import { ChecaCategoriaDeAEIC } from './modules/migrations/aeic/checa-categoria-de-aeic.js';
import { MostrarCampos } from './modules/migrations/aeic/mostrar-campos.js';
import { VerificaAEICExistente } from './modules/migrations/aeic/verifica-aeic-existente.js';
import { CadastraAEIC } from './modules/migrations/aeic/cadastra-aeic.js';
import { ListagemAEIC } from './modules/migrations/aeic/listagem.js';
import { ExcluirAEIC } from './modules/migrations/aeic/excluir-contrato.js';
import { CarregaDadosDoAEIC } from './modules/migrations/aeic/carrega-dados-de-aeic.js';
import { EditaAEIC } from './modules/migrations/aeic/edita-aeic.js';
import { FiltrarListagemAEICs } from './modules/migrations/aeic/filtrar-listagem.js';



import { CarregaParticipantes } from './modules/migrations/reunioes/carrega-participantes.js';
import { CadastraReuniao } from './modules/migrations/reunioes/cadastra-reuniao.js';
import { InserirLocais } from './modules/migrations/atividades/adiciona-locais.js';
import { InserirObras } from './modules/migrations/atividades/adiciona-obras.js';
import { InserirObrasComplemento } from './modules/migrations/atividades/adiciona-obras-complemento.js';import { CarregaUsuariosPorFuncao } from './modules/migrations/comum/carrega-usuarios-por-funcao.js';
import { EditaAtividade } from './modules/migrations/atividades/edita-atividade.js';
import { FiltrarListagemAtividades } from './modules/migrations/atividades/filtrar-listagem.js';



/**
 * Interação de back com front
 */
import { AutenticaUsuario } from './modules/autentica-usuario.js';
import { DadosDaSessao, ChecaSessaoAtiva } from './modules/checa-sessao-ativa.js';
import { EncerraSessao } from './modules/encerra-sessao.js';

/**
 * Customização
 */
import { AlternarBarraDePesquisa } from './modules/customizacao/alternador-de-pesquisa.js';
import { BtnIncluir } from './modules/customizacao/incluir-solicitacao.js';
import { ListagemSolicitacoes } from './modules/migrations/solicitacoes/listagem.js';
import { MontaLink } from './modules/customizacao/monta-link.js';
import { MontaNavegacao } from './modules/customizacao/monta-navegacao.js';
import { observarAlteracoesNoSelect, SelectCustomizado } from './modules/migrations/comum/select-customizado.js';
import { ControladorDeSecaoDeFormulario } from './modules/customizacao/controlador-de-secoes-form.js';
import { CarregaOpcaoPorURL } from './modules/migrations/observacoes-de-solicitacoes/carrega-contrato-por-url.js';
import { AtalhoNovaObservacaoDoContrato } from './modules/migrations/contratos/atalho-nova-observacao-de-contrato.js';
import { Welcome } from './modules/customizacao/welcome.js';
import { DropdownMobile, SlideMenuMobile } from './modules/customizacao/dropdown-menu-mobile.js';
import { CarregaManifest } from './modules/customizacao/carrega-manifesto.js';
import { InstaladorPWA } from './modules/customizacao/instalador-pwa.js';
import { DataEHora } from './modules/migrations/comum/carrega-data-e-hora.js';
import { NotFound } from './modules/customizacao/404.js';
import { ValorMonetario } from './modules/customizacao/valor-monetario.js';
import { MascaraDePorcentagem } from './modules/customizacao/mascara-de-porcentagem.js';

// Ativando o Tooltip do Bootstrap
document.addEventListener('DOMContentLoaded', function() {
    //const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    //const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

    const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    const tooltipList = tooltipTriggerList.map(function(element){
        return new bootstrap.Tooltip(element);
    });
})

//InitialSigi2();
DB;
Welcome();
InitDB();
CarregaManifest();
InserirFuncaoDeUsuario();
InserirUsuario();
MontaNavegacao();
SlideMenuMobile();
DropdownMobile();
DataEHora();
NotFound();


/**
 * Atribuindo a função de autenticação ao click de login
 */
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('btn-login') !== null) {
        const botaoLogin = document.getElementById('btn-login');
        if (botaoLogin !== null) {
            botaoLogin.addEventListener('click', AutenticaUsuario);
        }
    }    
});
// Carrega dados da sessão ativa, se houver
DadosDaSessao;
// Valida a sessão
// Executa a validação da sessão quando a página é carregada
window.addEventListener("load", ChecaSessaoAtiva);
// Encerra a sessão
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('btn-logout') !== null) {
        const botaoLogout = document.getElementById('btn-logout');
        if (botaoLogout !== null) {
            botaoLogout.addEventListener('click', EncerraSessao);
        }
    }
});
//Barra de pesquisa (Exibir e ocultar)
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById("header-caixa-arquitetura") !== null) {
        let headerElement = document.getElementById("header-caixa-arquitetura");
        headerElement.addEventListener("click", AlternarBarraDePesquisa);
    }
});
// Link com página de inclusão
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById("btn-incluir") !== null) {
        let btnSelecionado = document.getElementById("btn-incluir");
        btnSelecionado.addEventListener("click", BtnIncluir);
    }
});
// Botão para voltar
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById("btn-voltar") !== null) {
        let btnVoltar = document.getElementById("btn-voltar");
        btnVoltar.addEventListener("click", ()=>{
            history.go(-1)
        });
    }
});

// Links
// Seleciona todos os links com a classe "link-prototipo"
const links = document.querySelectorAll('.link-prototipo') ? document.querySelectorAll('.link-prototipo') : '';
let link_atual = window.location.href;
console.log('Link: ',link_atual);
let grupo_atual;
let monta_url;
let pagina_atual;

link_atual = link_atual.split('/');
if (link_atual.includes(".netlify.app")) {
    console.log('Ambiente atual: online');
    grupo_atual = link_atual[link_atual.length - 2];
    pagina_atual = link_atual[link_atual.length - 1].split('?id=');
    pagina_atual = pagina_atual[0];
    monta_url = '';
} else {
    console.log('Ambiente atual: local');
    grupo_atual = link_atual[link_atual.length - 2];
    pagina_atual = link_atual[link_atual.length - 1].split('?id=');
    pagina_atual = pagina_atual[0];
    monta_url = '';
}
InstaladorPWA({
    pagina: pagina_atual
});
console.log('PAGE: ',pagina_atual)

// Botão home da breadcrumb e navbar
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById("home-breadcrumb") !== null) {
        console.log('teste: ',pagina_atual)
        let btnVoltar = document.getElementById("home-breadcrumb");
        btnVoltar.addEventListener("click", (e)=>{
            e.preventDefault();
            let link_atual = window.location.href.split('/');
            let monta_url = link_atual.includes(".netlify.app") ? 'sigi2.netlify.app/' : '';
            let limit;
            
            if (pagina_atual !== 'lista.html') {
                limit = link_atual.includes(".netlify.app") ? 0 : 2;
            } else {
                limit = link_atual.includes(".netlify.app") ? 1 : 3;
            }
            for (let contador = 0; contador <= (link_atual.length - limit);contador++) {
                monta_url += link_atual[contador]+'/';
            }
            //console.log('URL: ',monta_url)
            location.href=`${monta_url}index.html`;
        });
    }

    if (document.getElementById("home-breadcrumb-2") !== null) {
        let btnVoltar = document.getElementById("home-breadcrumb-2");
        btnVoltar.addEventListener("click", (e)=>{
            e.preventDefault();
            let link_atual = window.location.href.split('/');
            let monta_url = link_atual.includes(".netlify.app") ? 'sigi2.netlify.app/' : '';
            let limit;
            if (pagina_atual != 'lista.html') {
                limit = link_atual.includes(".netlify.app") ? 0 : 2;
            } else {
                limit = link_atual.includes(".netlify.app") ? 1 : 3;
            }
            for (let contador = 0; contador <= (link_atual.length - limit);contador++) {
                monta_url += link_atual[contador]+'/';
            }
            location.href=`${monta_url}index.html`;
        });
    }

    if (document.getElementById("home-breadcrumb-3") !== null) {
        let btnVoltar = document.getElementById("home-breadcrumb-3");
        btnVoltar.addEventListener("click", (e)=>{
            e.preventDefault();
            let link_atual = window.location.href.split('/');
            let monta_url = link_atual.includes(".netlify.app") ? 'sigi2.netlify.app/' : '';
            let limit;
            if (pagina_atual != 'lista.html') {
                limit = link_atual.includes(".netlify.app") ? 0 : 2;
            } else {
                limit = link_atual.includes(".netlify.app") ? 1 : 3;
            }
            for (let contador = 0; contador <= (link_atual.length - limit);contador++) {
                monta_url += link_atual[contador]+'/';
            }
            location.href=`${monta_url}index.html`;
        });
    }
});
for (let contador = 0; contador <= (link_atual.length-3);contador++) {
    monta_url += link_atual[contador]+'/';
}
// Itera sobre cada link e modifica o atributo href
links.forEach(link => {
  const currentHref = link.getAttribute('href'); // Obtém o valor atual do atributo href
  const newHref = monta_url + currentHref; // Acrescenta o prefixo ao valor atual do href
  link.setAttribute('href', newHref); // Define o novo valor do atributo href
});
/**
 * Máscara de moeda
 */
document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementsByClassName("moeda")) {
        String.prototype.reverse = function(){
            return this.split('').reverse().join(''); 
        };
        function mascaraMoeda(evento) {
            var campo = evento.target;
            var tecla = evento.which || evento.keyCode;
            var valor = campo.value.replace(/[^\d]+/gi, '').reverse();
            var resultado = "";
            var mascara = "##.###.###,##".reverse();
            for (var x = 0, y = 0; x < mascara.length && y < valor.length;) {
              if (mascara.charAt(x) != '#') {
                resultado += mascara.charAt(x);
                x++;
              } else {
                resultado += valor.charAt(y);
                y++;
                x++;
              }
            }
            campo.value = resultado.reverse();
        }
    
        let campoMoeda = document.querySelectorAll(".moeda");

        campoMoeda.forEach(function(campo) {
            campo.addEventListener('keyup', mascaraMoeda);
        });
    }
});


const params = new URLSearchParams(window.location.search);
if (document.getElementById("breadcrumbs") !== null) {
    const breadcrumbsElement = document.getElementById("breadcrumbs");
    const listaElement = breadcrumbsElement.querySelector("ul");
    const linksBreadcrumb = listaElement.getElementsByTagName("a");
    for (let i = 0; i < linksBreadcrumb.length; i++) {
      const link = linksBreadcrumb[i];
      if (!link.href.includes('index.html')) {
        const href = new URL(link.href);
        href.search = params.toString();
        link.href = href.href;
      }  
    }
}


/**
 * Menu dropdown
 */
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      for (var i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.style.display === 'block') {
          openDropdown.style.display = 'none';
        }
      }
    }
}

// Controlador de seções de formulário
ControladorDeSecaoDeFormulario()


if (modulos?.solicitacoes) {

    CarregaTiposDeSolicitacoes();
    OrigensDeSolicitacoes();
    StatusDeSolicitacoes();

    /**
     * CRUD de Solicitações
     */

    // Carrega lista de origens em no formulário
    CarregaOrigensDeSolicitacoes();
    // Carrega lista de tipos de documentos de solicitação
    CarregaTiposDeDocSolicitacoes();
    // Carrega lista de tipos de documentos de solicitação
    CarregaStatusDeSolicitacoes();
    // Cadastra solicitação
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("salvar-solicitacao") !== null) {
            let botao = document.getElementById("salvar-solicitacao");
            botao.addEventListener("click", CadastraSolicitacao);
        }
    });
    // Carrega solicitações
    ListagemSolicitacoes();
    // Carrega dados de visualização
    CarregaDadosDeSolicitacao();
    // Checa mudanças de origem na edição da solicitação
    document.addEventListener("change", function() {
        if (document.getElementById("lista-origens") !== null) {
            // Obtém uma referência para o elemento select
            let selectElement = document.getElementById("lista-origens");
            // Adiciona um ouvinte de evento para o evento "change"
            selectElement.addEventListener("change", function() {
                // Obtém o valor selecionado
                let selectedValue = selectElement.value;
                document.getElementById("lista-origens-edit").value = selectedValue
            });
        }
    })
    // Checa mudanças de tipo na edição da solicitação
    document.addEventListener("change", function() {
        if (document.getElementById("lista-documentos") !== null) {
            // Obtém uma referência para o elemento select
            let selectElement = document.getElementById("lista-documentos");

            // Adiciona um ouvinte de evento para o evento "change"
            selectElement.addEventListener("change", function() {
                // Obtém o valor selecionado
                let selectedValue = selectElement.value;
                document.getElementById("lista-documentos-edit").value = selectedValue
            });
        }
    })
    // Edita solicitação
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("editar-solicitacao") !== null) {
            let botao = document.getElementById("editar-solicitacao");
            botao.addEventListener("click", EditaSolicitacao);
        }
    });
    // Exclui solicitação
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('listagem-de-solicitacoes') !== null) {
            let listagem = document.getElementById('listagem-de-solicitacoes');
            listagem.addEventListener('click', function(event) {
                if (event.target.matches('#excluir')) {
                    let identificador = event.target.getAttribute('identificador');
                    console.log('Solicitação de exlução do identificador: ',identificador);
                    ExcluirSolicitacao(identificador);
                }
            })
        }
    });
    // Filtra lista de solicitações
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("pesquisar-solicitacao") !== null) {
            let botao = document.getElementById("pesquisar-solicitacao");
            botao.addEventListener("click", FiltrarListagemSolicitacoes);
        }
    });
    // Limpa filtros de pesquisa de solicitações
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("limpar-listagem-solicitacao") !== null) {
            let botao = document.getElementById("limpar-listagem-solicitacao");
            botao.addEventListener("click", ListagemSolicitacoes);
        }
    });

    /**
     * Uso comum
     */
    CarregaSolicitacoes();

}

if (modulos?.empreendimentos) {
    /**
     * CRUD Empreendimentos
     */
    // Cadastra empreendimento
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("salvar-empreendimento") !== null) {
            let botao = document.getElementById("salvar-empreendimento");
            botao.addEventListener("click", CadastraEmpreendimento);
        }
    });
    // Carrega empreendimentos
    ListagemEmpreendimentos();
    // Filtra lista de empreendimentos
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("pesquisar-empreendimento") !== null) {
            let botao = document.getElementById("pesquisar-empreendimento");
            botao.addEventListener("click", FiltrarListagemEmpreendimentos);
        }
    });
    // Limpa filtros de pesquisa de empreendimentos
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("limpar-listagem-empreendimentos") !== null) {
            let botao = document.getElementById("limpar-listagem-empreendimentos");
            botao.addEventListener("click", ListagemEmpreendimentos);
        }
    });
    // Carrega dados individuais de visualização e edição Empreendimentos
    CarregaDadosDeEmpreedimento();
    // Checa mudanças de solicitação na edição do empreendimento
    document.addEventListener("change", function() {
        if (document.getElementById("lista-solicitacoes") !== null) {
            // Obtém uma referência para o elemento select
            let selectElement = document.getElementById("lista-solicitacoes");
            // Adiciona um ouvinte de evento para o evento "change"
            selectElement.addEventListener("change", function() {
                // Obtém o valor selecionado
                let selectedValue = selectElement.value;
                document.getElementById("lista-solicitacoes-edit").value = selectedValue
            });
        }
    })
    // Edita empreendimento
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("editar-empreendimento") !== null) {
            let botao = document.getElementById("editar-empreendimento");
            botao.addEventListener("click", EditaEmpreendimento);
        }
    });
    // Exclui empreendimento
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('listagem-de-empreendimentos') !== null) {
            let listagem = document.getElementById('listagem-de-empreendimentos');
            listagem.addEventListener('click', function(event) {
                if (event.target.matches('#excluir')) {
                    let identificador = event.target.getAttribute('identificador');
                    console.log('Solicitação de exclução do identificador de empreendimento: ',identificador);
                    ExcluirEmpreendimento(identificador);
                }
            })
        }
    });
}

if (modulos?.observacoes && grupo_atual === 'observacoes') {

    CarregaTiposDeObservacoes();
    let parametros_url = new URLSearchParams(window.location.search);
    let tipo = parametros_url.get('tipo');
    pagina_atual = pagina_atual.split('?tipo=');
    pagina_atual = pagina_atual[0];
    SelectBaseadoEmTipo({
        tipo,
        pagina: pagina_atual
    });
    ListaDeOpcoes({
        tipo
    });

    //Limitador de textarea
    LimitadorDeTextarea({
        limite_de_caracteres: 256
    });

    /**
     * CRUD Observações
     */

    // Cadastra observações
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("salvar-observacao") !== null) {
            let botao = document.getElementById("salvar-observacao");
            botao.addEventListener("click", CadastraObservacao);
        }
    });
    // Carrega observação
    ListagemObservacoes({
        tipo
    });
    // Filtra lista de observações
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("pesquisar-observacoes") !== null) {
            let botao = document.getElementById("pesquisar-observacoes");
            botao.addEventListener("click", () => FiltrarListagemObservacoes({
                tipo
            }));
        }
    });
    // Limpa filtros de pesquisa de observações
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("limpar-listagem-observacoes") !== null) {
            let botao = document.getElementById("limpar-listagem-observacoes");
            botao.addEventListener("click", ()=>{
                document.getElementsByClassName('field-select').value = '';
                document.getElementById('texto-observacao').value = '';
                ListagemObservacoes({
                    tipo
                });
            });
        }
    });
    // Carrega dados individuais de visualização e edição de observação
    CarregaDadosDeObservacao();
    // Checa mudanças de solicitação na edição do observação
    document.addEventListener("change", function() {
        if (document.getElementById("lista-solicitacoes") !== null) {
            // Obtém uma referência para o elemento select
            let selectElement = document.getElementById("lista-solicitacoes");
            // Adiciona um ouvinte de evento para o evento "change"
            selectElement.addEventListener("change", function() {
                // Obtém o valor selecionado
                let selectedValue = selectElement.value;
                document.getElementById("lista-solicitacoes-edit").value = selectedValue
            });
        }
    })

    CarregaOpcaoPorURL({
        pagina: pagina_atual
    });

    //const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');
    const tipo_de_observacao = parametros_url.get('tipo');

    if (tipo_de_observacao == 'contrato' && identificador > 0 && pagina_atual == 'lista.html') {
        document.getElementById('header-arquitetura').classList.add('d-none');
        ListagemObservacoes({
            id: identificador
        })
    }

}

if (modulos?.solicitacoes_de_reuniao) {

    StatusDeReunioes();

    /**
     * CRUD Solicitações de Reuniões
     */

    // Carregando os engenheiros
    CarregaUsuariosPorFuncao(Number(3));
    // Cadastra solicitação de reunião
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("salvar-solicitacao-de-reuniao") !== null) {
            let botao = document.getElementById("salvar-solicitacao-de-reuniao");
            botao.addEventListener("click", CadastraSolicitacaoDeReuniao);
        }
    });
    // Listagem de solicitações de reunião
    ListagemSolicitacoesDeReunioes();
    // Filtragem de solicitações de reunião
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("pesquisar-solicitacoes-de-reuniao") !== null) {
            let botao = document.getElementById("pesquisar-solicitacoes-de-reuniao");
            botao.addEventListener("click", FiltrarListagemSolicitacoesDeReunioes);
        }
    });
    // Limpando filtros de solicitações de reunião
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("limpar-filtro-solicitacoes-de-reuniao") !== null) {
            let botao = document.getElementById("limpar-filtro-solicitacoes-de-reuniao");
            botao.addEventListener("click", ()=>{
                document.getElementById('lista-solicitacoes').value = 0;
                document.getElementById('lista-engenheiros').value = 0;
                document.getElementById('data-programada-inicial').value = 0;
                document.getElementById('data-programada-final').value = 0;
                ListagemSolicitacoesDeReunioes();
            });
        }
    });
    // Carregando dados únicos de solicitação de reunião
    CarregaDadosDeSolicitacaoDeReuniao();
    // Edita solicitação de reunião
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("editar-solicitacao-de-reuniao") !== null) {
            let botao = document.getElementById("editar-solicitacao-de-reuniao");
            botao.addEventListener("click", EditaSolicitacaoDeReuniao);
        }
    });
    // Exclui solicitação de reunião
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('listagem-de-solicitacoes-de-reuniao') !== null) {
            let listagem = document.getElementById('listagem-de-solicitacoes-de-reuniao');
            listagem.addEventListener('click', function(event) {
                if (event.target.matches('#excluir')) {
                    let identificador = event.target.getAttribute('identificador');
                    console.log('Solicitação de exclução do identificador de solicitação de reunião: ',identificador);
                    ExcluirSolicitacaoDeReuniao(identificador);
                }
            })
        }
    });
}

if (modulos?.reuniao) {
    /**
     * CRUD Reuniões
     */

    // Carrega participantes
    CarregaParticipantes();
    // Carrega solicitações de reunião
    CarregaSolicitacoesDeReuniao();
    // Cadastra observações
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("salvar-reuniao") !== null) {
            let botao = document.getElementById("salvar-reuniao");
            botao.addEventListener("click", CadastraReuniao);
        }
    });
}

if(modulos?.visitas_tecnicas) {
    StatusDeVisitasTecnicas();
}

if (modulos?.orcamentos && grupo_atual === 'orcamentos') {
    TiposDeOrcamento();
    StatusDeOrcamento();
}

if (modulos?.status_do_contrato && modulos?.contratos && grupo_atual === 'contratos') {
    InsereStatusDeContrato();
    InsereDepartamentosIniciais();

    if (pagina_atual === 'incluir.html' || pagina_atual === 'lista.html') {

        CarregaNumerosDeContratoDoSAP({
            id_select: 'opcoes-numero-do-contrato',
            placeholder: pagina_atual === 'lista.html' ? 'Escolha um contrato' : 'Escolha ou digite um nº de contrato',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-numero-do-contrato',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaPrestadoresDeServicos({
            id_select: 'opcoes-de-empresa-do-contrato',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-de-empresa-do-contrato',
                placeholder: 'Escolha uma empresa',
                sem_resultados: 'Sem mais empresas!',
                sem_resultados_na_pesquia: 'Nenhuma empresa encontrada!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))
    
        CarregaUsuariosPorFuncao({
            id_select: 'opcoes-para-responsavel',
            placeholder: 'Escolha um responsável'
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-para-responsavel',
                placeholder: 'Escolha o responsável',
                sem_resultados: 'Sem mais responsável!',
                sem_resultados_na_pesquia: 'Nenhum responsável encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))    
    
    
        CarregaDepartamentos('opcoes-dep-responsavel')
        .then(opcoes => {
            SelectCustomizado({
                id_do_select: 'opcoes-dep-responsavel',
                placeholder: 'Escolha o dep. responsável',
                sem_resultados: 'Sem mais dep. responsáveis!',
                sem_resultados_na_pesquia: 'Nenhum departamento encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        if (document.getElementById('opcoes-atividades-do-contrato') !== null) {
            CarregaAtividades('opcoes-atividades-do-contrato')
            .then(opcoes => {
                SelectCustomizado({
                    id_do_select: 'opcoes-atividades-do-contrato',
                    placeholder: 'Escolha uma ou mais atividades',
                    sem_resultados: 'Sem mais atividades!',
                    sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                    sugestao_de_adicao: 'Clique para selecionar',
                    adicionavel: true,
                    customizar: pagina_atual === 'lista.html' ? false : true
                });
            }).catch(e=>console.error('Erro: ',e))
        }

        // Exclui contrato
        ExcluirContrato();

    }

    if (pagina_atual === 'incluir.html' || pagina_atual === 'lista.html' || pagina_atual === "editar.html") {
        //Limitador de textarea
        LimitadorDeTextarea({
            limite_de_caracteres: 256
        });
    }


    /**
     * CRUD Contratos
     */

    // Cadastra contrato
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("salvar-contrato") !== null) {
            let botao = document.getElementById("salvar-contrato");
            botao.addEventListener("click", CadastraContrato);
        }
    });
    // Listagem de contratos
    ListagemContratos();
    // Carrega dados do contrato
    CarregaDadosDoContrato(pagina_atual);

    // Edita dados do contrato
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("editar-contrato") !== null) {
            let botao = document.getElementById("editar-contrato");
            botao.addEventListener("click", EditaContrato);
        }
    });
    
    // Filtra lista de contratos
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("pesquisar-contrato") !== null) {
            let botao = document.getElementById("pesquisar-contrato");
            botao.addEventListener("click", FiltrarListagemContratos);
        }
    });

    // Limpa filtros de pesquisa de contratos
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("limpar-listagem-contrato") !== null) {
            let botao = document.getElementById("limpar-listagem-contrato");
            botao.addEventListener("click", () => {
                document.getElementById('opcoes-numero-do-contrato').value = '';
                document.getElementById('opcoes-de-empresa-do-contrato').value = '';
                document.getElementById('opcoes-para-responsavel').value = '';
                document.getElementById('opcoes-dep-responsavel').value= '';
                document.getElementById('opcoes-numero-do-contrato-selecionado').value = '';
                document.getElementById('rotulo-do-contrato').value = '';
                document.getElementById('opcoes-de-empresa-do-contrato-selecionado').value = '';
                document.getElementById('opcoes-para-responsavel-selecionado').value = '';
                document.getElementById('opcoes-dep-responsavel-selecionado').value= '';
                ListagemContratos
            });
        }
    });

    // Atalho para abrir as observações do contrato
    AtalhoObservacoesDoContrato()

    // Atalho para criar nova observação de contrato
    AtalhoNovaObservacaoDoContrato()
}

if (modulos?.atividades && grupo_atual === 'atividades') {
    InserirLocais();
    InserirObras();
    InserirObrasComplemento();   

    if (pagina_atual === 'incluir.html') {
        CarregaLocais({
            id_select: 'opcoes-locais-da-atividade',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-locais-da-atividade',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhum local de atividade encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaObras({
            id_select: 'opcoes-obras-da-atividade',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-obras-da-atividade',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhuma obra encontrada!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaObrasComplemento({
            id_select: 'opcoes-obras-complemento-da-atividade',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-obras-complemento-da-atividade',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhuma obra/complemento encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaUsuariosPorFuncao({
            id_select: 'opcoes-gestor-da-atividade',
            placeholder: 'Escolha um gestor',
            funcao: 2,
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-gestor-da-atividade',
                placeholder: 'Escolha um gestor',
                sem_resultados: 'Sem mais gestores!',
                sem_resultados_na_pesquia: 'Nenhum gestor encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaUsuariosPorFuncao({
            id_select: 'opcoes-coordenador-da-atividade',
            placeholder: 'Escolha um coordenador',
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-coordenador-da-atividade',
                placeholder: 'Escolha um coordenador',
                sem_resultados: 'Sem mais coordenadores!',
                sem_resultados_na_pesquia: 'Nenhum coordenador encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaDepartamentos('opcoes-fiscalizacao-da-atividade')
        .then(opcoes => {
            SelectCustomizado({
                id_do_select: 'opcoes-fiscalizacao-da-atividade',
                placeholder: 'Escolha o dep. responsável',
                sem_resultados: 'Sem mais dep. responsáveis!',
                sem_resultados_na_pesquia: 'Nenhum departamento encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

        /*
        CarregaAtividades('opcoes-atividades-do-contrato')
        .then(opcoes => {
            console.log('Teste')
            SelectCustomizado({
                id_do_select: 'opcoes-atividades-do-contrato',
                placeholder: 'Escolha uma ou mais atividades',
                sem_resultados: 'Sem mais atividades!',
                sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))*/

        //

        // Cadastra atividade
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("salvar-atividade") !== null) {
                let botao = document.getElementById("salvar-atividade");
                botao.addEventListener("click", CadastraAtividade);
            }
        });
    }

    if (pagina_atual === 'lista.html') {

        CarregaLocais({
            id_select: 'opcoes-locais-da-atividade',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-locais-da-atividade',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhum local de atividade encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: pagina_atual === 'lista.html' ? false : true
            });
        }).catch(e=>console.error('Erro: ',e))

         // Filtra lista de atividade
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("pesquisar-atividade") !== null) {
                let botao = document.getElementById("pesquisar-atividade");
                botao.addEventListener("click", FiltrarListagemAtividades);
            }
        });

        // Limpa filtros de pesquisa de atividade
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("limpar-listagem-atividade") !== null) {
                let botao = document.getElementById("limpar-listagem-atividade");
                botao.addEventListener("click", () => {
                    document.getElementById('opcoes-locais-da-atividade').value = '';
                    document.getElementById('opcoes-locais-da-atividade-selecionado').value = '';
                    ListagemAtividades
                });
            }
        });

        //
        ListagemAtividades();

        //
        ExcluirAtividade();
    }

    if (pagina_atual === 'incluir.html' || pagina_atual === 'lista.html' || pagina_atual === "editar.html") {
        //Limitador de textarea
        LimitadorDeTextarea({
            limite_de_caracteres: 256
        });
    }

    if ( pagina_atual === "visualizar.html" || pagina_atual === "editar.html" ) {
        // Carrega dados da atividade
        CarregaDadosDaAtividade(pagina_atual);
    }

    if ( pagina_atual === "editar.html" ) {
        // Edita dados do atividade
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("editar-atividade") !== null) {
                let botao = document.getElementById("editar-atividade");
                botao.addEventListener("click", EditaAtividade);
            }
        });
    }

}

if (modulos?.aeic && grupo_atual === 'aeic') {
    
    CategoriasDeAEIC();

    if (pagina_atual === 'incluir.html') {

        MostrarCampos();
    
        CarregaListaDeContratos({
            id_select: 'opcoes-aeic-contrato',
            placeholder: 'Escolha um contrato',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-aeic-contrato',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: false,
                customizar: false
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaAtividades('opcoes-aeic-atividade','Escolha uma atividade')
        .then(opcoes => {
            SelectCustomizado({
                id_do_select: 'opcoes-aeic-atividade',
                placeholder: 'Escolha uma atividade',
                sem_resultados: 'Sem mais atividades!',
                sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: false,
                customizar: false
            });
        }).catch(e=>console.error('Erro: ',e))

        observarAlteracoesNoSelect({
            id_do_select: 'opcoes-aeic-atividade',
            placeholder: 'Escolha uma atividade',
            sem_resultados: 'Sem mais atividades!',
            sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
            sugestao_de_adicao: 'Clique para selecionar',
            adicionavel: false,
            customizar: false
        })

        ListaReferenciasAEIC({
            id_select: 'opcoes-referencia-do-aeic',
            placeholder: 'Escolha ou digite referência',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-referencia-do-aeic',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: true,
                customizar: true
            });
        }).catch(e=>console.error('Erro: ',e))

        SelectCustomizado({
            id_do_select: 'opcoes-categoria-do-aeic',
            placeholder: 'Escolha uma categoria',
            sem_resultados: 'Sem mais categorias!',
            sem_resultados_na_pesquia: 'Nenhuma categoria encontrada!',
            sugestao_de_adicao: 'Clique para selecionar',
            adicionavel: false,
            customizar: false
        });

        ValorMonetario({
            id: 'valor-aeic'
        })

        ValorMonetario({
            id: 'valor-de-reajuste'
        })

        MascaraDePorcentagem({
            id: 'reajuste',
            calcular: 1,
            campo_do_calculo: 'valor-aeic'
        });

        /**
         * Checando as atividades vinculadas ao contrato
         */
        VerificaAtividadesPorContrato();

        /**
         * Checando a categoria de AEIC para exibição de campos
         */
        ChecaCategoriaDeAEIC();

        VerificaAEICExistente();
        /**
         * Cadastra AEIC
         */
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("salvar-aeic") !== null) {
                let botao = document.getElementById("salvar-aeic");
                botao.addEventListener("click", CadastraAEIC);
            }
        });

    }

    if (pagina_atual === 'lista.html') {
        ListagemAEIC();

        CarregaListaDeContratos({
            id_select: 'opcoes-aeic-contrato',
            placeholder: 'Escolha um contrato',
            pagina: pagina_atual
        })
        .then (()=>{
            SelectCustomizado({
                id_do_select: 'opcoes-aeic-contrato',
                placeholder: 'Escolha ou digite um número aqui',
                sem_resultados: 'Sem mais números!',
                sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: false,
                customizar: false
            });
        }).catch(e=>console.error('Erro: ',e))

        CarregaAtividades('opcoes-aeic-atividade','Escolha uma atividade')
        .then(opcoes => {
            SelectCustomizado({
                id_do_select: 'opcoes-aeic-atividade',
                placeholder: 'Escolha uma atividade',
                sem_resultados: 'Sem mais atividades!',
                sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: false,
                customizar: false
            });
        }).catch(e=>console.error('Erro: ',e))

        SelectCustomizado({
            id_do_select: 'opcoes-categoria-do-aeic',
            placeholder: 'Escolha uma categoria',
            sem_resultados: 'Sem mais categorias!',
            sem_resultados_na_pesquia: 'Nenhuma categoria encontrada!',
            sugestao_de_adicao: 'Clique para selecionar',
            adicionavel: false,
            customizar: false
        });

        // Filtra lista de A.E.I.C.s
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("pesquisar-aeic") !== null) {
                let botao = document.getElementById("pesquisar-aeic");
                botao.addEventListener("click", FiltrarListagemAEICs);
            }
        });

        // Limpa filtros de pesquisa de A.E.I.C.s
        document.addEventListener('DOMContentLoaded', function() {
            if (document.getElementById("limpar-listagem-aeic") !== null) {
                let botao = document.getElementById("limpar-listagem-aeic");
                botao.addEventListener("click", () => {
                    document.getElementById('opcoes-aeic-contrato-selecionado').value = '';
                    document.getElementById('opcoes-aeic-atividade-selecionado').value = '';
                    document.getElementById('mes-ano-aeic').value = '';
                    document.getElementById('opcoes-categoria-do-aeic-selecionado').value = 0;
                    ListagemAEIC();
                });
            }
        });

        /**
         * Exclusão de A.E.I.C.s
         */
        ExcluirAEIC();
    }

    // Carrega dados do A.E.I.C.
    CarregaDadosDoAEIC(pagina_atual)

    // Edita dados do A.E.I.C.
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById("editar-aeic") !== null) {
            let botao = document.getElementById("editar-aeic");
            botao.addEventListener("click", ()=>{
                console.log('Clicado')
                EditaAEIC()
            });
        }
    });

}