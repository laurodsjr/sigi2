import { DB } from './migrations/init-indexeddb';

export function AutenticaUsuario() {

    let matricula = document.getElementById("matriculaLogin"); 
    let senha = document.getElementById("senhaLogin"); 

    /**
     * Validação básica de matrícula e senha
     */
    if (matricula.value == '' || senha.value == '') {
        alert(`Mátrícula e senha são campos obrigatórios!`);
    } else {
    
        const valida = DB.usuarios.where({
            matricula: matricula.value,
            senha: senha.value
        }).toArray()
        .then(reposta => {
            if (reposta.length > 0) {
                console.log(`Autenticação bem sucedida!`,reposta);

                /**
                 * Salvando os dados de autenticação no sessionStorage
                 */
                sessionStorage.setItem('usuario_autenticado', JSON.stringify(reposta[0]) );
                /**
                 * Enviando o usuário para a index
                 */
                location.href='index.html';

            } else {
                console.error(`Erro ao altenticar!`)
            }
        })
        .catch(e => {
            console.log('Erro na tentativa de login!',e)
        })
        

    }

}