function redirecionarParaLogin() {
    // Obtém o caminho atual da URL
    let caminhoAtual;

    let ambiente = window.location.href;
    let prefixo_url;
    
    if (ambiente.includes(".netlify.app")) {
        prefixo_url = 'sigi2.netlify.app/'
    } else {
        caminhoAtual = window.location.pathname.split('TesteDeMesa/');
        prefixo_url = 'TesteDeMesa/'
    }
    let trataCaminho = `${caminhoAtual[0]}${prefixo_url}/login.html`;
    console.error('Sessão inválida!');
    location.href = `${trataCaminho}`;
}

export async function DadosDaSessao() {
    document.addEventListener('DOMContentLoaded', function() {
        let dados = sessionStorage.getItem('usuario_autenticado');
        dados = dados == '' || dados === null ? {
            matricula: ''
        } : JSON.parse(dados);
        return dados
    });
}


export function ChecaSessaoAtiva() {
    if (window.location.href.indexOf("login.html") === -1) {
        if (!sessionStorage.getItem("usuario_autenticado")) {
            redirecionarParaLogin();
        } else {
            console.log('Sessão de usuário checada e válida!');
        }
    }
}