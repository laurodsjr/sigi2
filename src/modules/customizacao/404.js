export function NotFound() {
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('404') !== null) {
        
            const body_welcome = document.getElementById('404');

            const content = `<section class="container">
                <div class="row mb-md-0">
                    <div class="col-12 col-md-12 pt-3 pt-md-5">
                        <div class="alert alert-warning w-100 text-center" role="alert">
                            <h1 class="text-center">Ops! Página não encontrada</h1>

                            <p>Este é um conteúdo não existe ou foi removido!</p>
                        </div>
                    </div>
                </div>
            </section>`; 

            body_welcome.innerHTML = content

        }
    })
}