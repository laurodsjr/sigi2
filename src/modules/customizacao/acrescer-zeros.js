export function AcrescentadorDeZeros(numero,limite = 4) {
    // Converte o número para uma string
    let numeroString = String(numero);
    
    // Verifica se o número já possui pelo menos 4 dígitos
    if (numeroString.length >= limite) {
      return numeroString;
    }
    
    // Calcula o número de zeros a serem acrescentados
    let zerosFaltantes = limite - numeroString.length;
    
    // Acrescenta os zeros à esquerda
    for (let i = 0; i < zerosFaltantes; i++) {
      numeroString = '0' + numeroString;
    }
    
    return numeroString;
}