export function CarregaManifest() {
    let currentDomain = window.location.href;

    // Verifica se o domínio corresponde ao domínio desejado
    if (currentDomain.includes('https://sigi2.netlify.app/')) {
      console.log('Manifest carregado!');
      // Criação dinâmica da tag <link> para o manifesto
      let manifestLink = document.createElement('link');
      manifestLink.rel = 'manifest';
      manifestLink.href = 'https://sigi2.netlify.app/manifest.json';

      // Adiciona a tag <link> ao cabeçalho do documento
      document.head.appendChild(manifestLink);
    }
}