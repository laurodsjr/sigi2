export function ControladorDeSecaoDeFormulario() {

    if (document.querySelectorAll('.secao-de-formulario') !== null) {
        // Obtém todos os elementos com a classe 'secao-de-formulario'
        const secoes = document.querySelectorAll('.secao-de-formulario');

        // Percorre cada seção
        secoes.forEach(secao => {
            // Obtém o título da seção
            const titulo = secao.querySelector('h3');

            // Adiciona um ouvinte de evento de clique ao título
            titulo.addEventListener('click', () => {
                // Percorre todas as seções novamente
                secoes.forEach(secao => {
                    // Oculta o conteúdo de todas as seções
                    const conteudo = secao.querySelector('div');
                    conteudo.classList.add('d-none');
                });

                // Mostra apenas o conteúdo da seção clicada
                const conteudo = secao.querySelector('div');
                conteudo.classList.remove('d-none');
            });
        });
    }

}