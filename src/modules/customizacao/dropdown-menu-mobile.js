export function DropdownMobile() {
    const dropdowns = document.querySelectorAll('.dropdown');
    
    // Verificar se o dispositivo é um dispositivo móvel
    if (window.innerWidth <= 768) {
        // Percorrer todos os elementos .dropdown
        dropdowns.forEach(function(dropdown) {
            const dropbtn = dropdown.querySelector('.dropbtn');
            const dropdownContent = dropdown.querySelector('.dropdown-content');

            // Adicionar evento de clique ao dropbtn dentro do contexto do dropdown atual
            dropbtn.addEventListener('click', function() {
                // Alternar a classe para exibir/ocultar o conteúdo do dropdown do contexto atual
                dropdownContent.classList.toggle('show');
            });
        });
    }
}

export function SlideMenuMobile() {

    if (document.getElementById('menu-mobile') !== null && document.getElementById('menu-toggle') !== null && document.getElementById('menu-hide-area') !== null) {

        const menuMobile = document.getElementById('menu-mobile');
        const menuToggle = document.getElementById('menu-toggle');
        const menuHideArea = document.getElementById('menu-hide-area');
        let initialX = null;
    
        menuMobile.addEventListener('touchstart', function(event) {
        initialX = event.touches[0].clientX;
        });
    
        menuMobile.addEventListener('touchmove', function(event) {
        if (initialX === null) {
            return;
        }
    
        const currentX = event.touches[0].clientX;
        const diffX = currentX - initialX;
    
        if (diffX > 0) {
            menuMobile.classList.add('show');
            menuHideArea.classList.add('show');
        } else {
            menuMobile.classList.remove('show');
            menuHideArea.classList.remove('show');
        }
    
        initialX = null;
        });
    
        // Função para alternar a visibilidade do menu mobile
        function toggleMenuMobile() {
        menuMobile.classList.toggle('show');
        menuHideArea.classList.toggle('show');
        }
    
        // Evento de clique no ícone do menu
        menuToggle.addEventListener('click', toggleMenuMobile);
    
        // Evento de clique na área para ocultar o menu
        menuHideArea.addEventListener('click', function(event) {
        if (event.target === menuHideArea) {
            toggleMenuMobile();
        }
        });

    }
}