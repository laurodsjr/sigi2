export function ConverteData(data_indicada = '2023-06-07') {
    const dataPreCarregada = new Date(data_indicada);
    const dia = String(dataPreCarregada.getDate()).padStart(2, '0');
    const mes = String(dataPreCarregada.getMonth() + 1).padStart(2, '0');
    const ano = dataPreCarregada.getFullYear();

    const dataFormatada = Number(dia)+1 + '/' + mes + '/' + ano;

    return dataFormatada;
}