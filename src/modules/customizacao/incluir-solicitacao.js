export function BtnIncluir() {

    let parametros_url = new URLSearchParams(window.location.search);
    let tipo = parametros_url.get('tipo');

    let btnIncluir = document.getElementById("btn-incluir");
    var link = btnIncluir.getAttribute("incluir");
    //location.href=`incluir-${link}.html`;
    location.href=`incluir.html${tipo ? `?tipo=${tipo}` : ''}`;
}