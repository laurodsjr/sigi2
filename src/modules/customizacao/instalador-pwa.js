export function InstaladorPWA(parametros = {
    pagina: ''
}) {

    const {
        pagina = ''
    } = parametros

    let currentDomain = window.location.href;

    // Verifica se o domínio corresponde ao domínio desejado
    if (currentDomain === 'https://sigi2.netlify.app/') {

        self.addEventListener('install', function(event) {
            event.waitUntil(
            caches.open('cache').then(function(cache) {
                return cache.addAll([
                './index.html',
                // lista de arquivos que você deseja armazenar em cache
                ]);
            })
            );
        });
        
        self.addEventListener('fetch', function(event) {
            event.respondWith(
            caches.match(event.request).then(function(response) {
                return response || fetch(event.request);
            })
            );
        });

        if (pagina === 'index.html' || pagina === 'login.html') {
            if ('serviceWorker' in navigator) {
                window.addEventListener('load', function() {
                  navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                    console.log('Service Worker registrado com sucesso:', registration);
                  }, function(error) {
                    console.log('Falha ao registrar o Service Worker:', error);
                  });
                });
            }
        }


        // Verifica se o navegador suporta a instalação de PWAs
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            window.addEventListener('load', function() {
                
                // Registra o Service Worker
                navigator.serviceWorker.register('service-worker.js')
                .then(function(registration) {
                    console.log('Service Worker registrado com sucesso:', registration);
                })
                .catch(function(error) {
                    console.log('Falha ao registrar o Service Worker:', error);
                });

                if (pagina === 'index.html' || pagina === 'login.html') {
                    // Verifica se o PWA já está instalado
                    if (window.matchMedia('(display-mode: standalone)').matches || window.navigator.standalone) {
                        console.log('O PWA já está instalado.');
                    } else {
                        // Exibe o botão de instalação
                        var installButton = document.createElement('button');
                        installButton.textContent = '<i class="fas fa-download"></i> Instalar';
                        installButton.classList.add('install-button')
                        installButton.addEventListener('click', () => {
                            // Prompt de instalação do PWA
                            window.addEventListener('beforeinstallprompt', function(event) {
                                event.preventDefault(); // Impede o prompt de exibição automática
                                event.prompt(); // Exibe o prompt de instalação manualmente
                            });
                            document.body.appendChild(installButton);
                        });
                    }

                }

            });
        }
        
    }

}