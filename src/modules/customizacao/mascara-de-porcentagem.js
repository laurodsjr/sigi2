import { ReverteValorMonetario, ValorMonetario } from "./valor-monetario";

export function MascaraDePorcentagem(parametros = {
    id,
    calcular,
    campo_do_calculo
}) {
    const {
        id,
        calcular = false,
        campo_do_calculo
    } = parametros

    // Impedir a digitação de letras ou caracteres inválidos no input
    if (document.getElementById(id) !== null) {
        const percentInput = document.getElementById(id)
        percentInput.addEventListener('input', () => {
            percentInput.value = percentInput.value.replace(/[^0-9.]/g, '');

            // Limitar o número de casas decimais para 4
            const decimalIndex = percentInput.value.indexOf('.');
            if (decimalIndex !== -1) {
              const decimalPart = percentInput.value.substring(decimalIndex + 1);
              percentInput.value = percentInput.value.substring(0, decimalIndex + 1) + decimalPart.substring(0, 4);
            }

            if (calcular) {
                let valor = document.getElementById(campo_do_calculo).value;
                document.getElementById('valor-de-reajuste').value = ValorMonetario({
                    tipo: 'converter',
                    numero: ReverteValorMonetario(valor) * percentInput.value
                });
            }
        });
    }    
}