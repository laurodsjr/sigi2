import { links } from "../links";

export function MontaNavegacao() {

    if (document.getElementById('navbar') !== null) {

        let ambiente = window.location.href;
        let prefixo_url;
        if (ambiente.includes(".netlify.app")) {
            prefixo_url = 'sigi2.netlify.app/'
        } else {
            prefixo_url = 'TesteDeMesa/'
        }


        const dados_da_sessao = JSON.parse(sessionStorage.getItem("usuario_autenticado"));

        if (dados_da_sessao === null) {
            location.href = 'login.html';
        } 

        let funcao_do_usuario = dados_da_sessao.funcao;

        function ConverteParametrosEmURL(parametros = undefined) {
            if (parametros != undefined) {
                const parametrosURL = Object.entries(parametros)
                    .map(([chave, valor]) => `${encodeURIComponent(chave)}=${encodeURIComponent(valor)}`)
                    .join('&');
                const resultado = `?${parametrosURL}`;
                return resultado;
            } else {
                return '';
            }
        }

        function Menu(filtro = {
            opcao_unica: '',
            excluir_opcoes: [],
            mostrar_apenas: [],
            parametros: undefined
        }) {

            const { opcao_unica, excluir_opcoes, mostrar_apenas, parametros = undefined } = filtro;

            if (!opcao_unica  && !excluir_opcoes && !mostrar_apenas) {

                let menu = window.location.href.indexOf("index.html") !== -1 ? links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && item.status ? `<li>
                        <a ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                item.link != 'index.html' ? `${prefixo_url}${item.link}/lista.html${ConverteParametrosEmURL(parametros)}` : item.link
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone} me-1"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('') : links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && item.status ? `<li>
                        <a 
                            ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                `${item.link}/lista.html${ConverteParametrosEmURL(parametros)}`
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone}"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('');
    
                return menu;

            } 
                
            if (opcao_unica) {

                let menu = window.location.href.indexOf("index.html") !== -1 ? links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && opcao_unica == item.link && item.status ? `<li>
                        <a ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                item.link != 'index.html' ? `${prefixo_url}${item.link}/lista.html${ConverteParametrosEmURL(parametros)}` : item.link
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone} me-1"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('') : links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && opcao_unica == item.link && item.status ? `<li>
                        <a 
                            ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                `${item.link}/lista.html${ConverteParametrosEmURL(parametros)}`
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone}"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('')

                return menu;

            }  
            
            if (excluir_opcoes) {

                const novos_links = links.filter(link => !excluir_opcoes.includes(link.link));

                let menu = window.location.href.indexOf("index.html") !== -1 ? novos_links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && item.status ? `<li>
                        <a ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                item.link != 'index.html' ? `${prefixo_url}${item.link}/lista.html${ConverteParametrosEmURL(parametros)}` : item.link
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone} me-1"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('') : novos_links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && item.status ? `<li>
                        <a 
                            ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                `${item.link}/lista.html${ConverteParametrosEmURL(parametros)}`
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone}"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('')

                return menu;

            }

            if (mostrar_apenas) {

                // Filtrar e reorganizar o array de objetos com base nos títulos
                const novos_links = links.filter(link => {
                    return mostrar_apenas.includes(link.link);
                }).sort((a, b) => {
                    return mostrar_apenas.indexOf(a.link) - mostrar_apenas.indexOf(b.link);
                });
                
                // Mostrar apenas os títulos filtrados e reorganizados
                //const novos_links = titulos_filtrados.map(link => link.titulo);

                let menu = window.location.href.indexOf("index.html") !== -1 ? novos_links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && item.status ? `<li>
                        <a ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                item.link != 'index.html' ? `${prefixo_url}${item.link}/lista.html${ConverteParametrosEmURL(parametros)}` : item.link
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone} me-1"></i>${item.titulo}` : item.titulo }
                        </a>
                    </li>` : ``
                }).join('') : novos_links.map(item => {
                    return item.quem_acessa.includes(funcao_do_usuario) && item.status ? `<li>
                        <a 
                            ${item.id_css != '' ? 'id="'+item.id_css+'"' : ''} ${item.classe_css != '' ? 'class="'+item.classe_css+' d-flex align-items-center"' : 'class="d-flex align-items-center"'} 
                            href="${
                                `${item.link}/lista.html${ConverteParametrosEmURL(parametros)}`
                            }"
                        >
                            ${ item.icone != '' ? `<i class="${item.icone}"></i>${item.titulo}` : item.titulo }
                        </a> 
                    </li>` : ``
                }).join('')

                return menu;
            }

        }

        function CaixaDeOpcoes(propriedades = {
            titulo: '',
            lista : '',
            status: 0,
            classes: ''
        }) {

            const { titulo, lista, status = 1, classes = ''} = propriedades;

            return status ? `<li class="dropdown${classes !== '' ? ' '+classes : ''}">
                <button class="dropbtn${classes !== '' ? ' '+classes : ''}">${titulo}</button>
                <div class="dropdown-content">
                    <ul>
                        ${classes.includes('desativado') == false ? lista : ''}
                    </ul>
                </div>
            </li>` : ``;

        }

        document.getElementById('navbar').innerHTML = `<div id="menu-destop" class="container d-none d-md-flex">
            <ul>
                <li>
                    <a id="home-breadcrumb" href="index.html">
                        <i class="fa-solid fa-house"></i> Home
                    </a>
                </li>

                ${Menu({
                    opcao_unica: 'Empreendimentos'
                })}

                ${CaixaDeOpcoes({
                    titulo: 'Pré-contratação',
                    lista: Menu({
                        excluir_opcoes: [
                            'Empreendimentos',
                            'Contrato'
                        ]
                    }),
                    status: 1,
                    classes: 'desativado'
                })}

                ${CaixaDeOpcoes({
                    titulo: 'Contratação',
                    lista: Menu({
                        excluir_opcoes: [
                            'Empreendimentos',
                            'Contrato'
                        ]
                    }),
                    status: 1,
                    classes: 'desativado'
                })}

                

                ${CaixaDeOpcoes({
                    titulo: 'Gestão de contratos',
                    lista: [
                        Menu({
                            mostrar_apenas: [
                                'atividades',
                                'contratos',
                                'aeic'
                            ]
                        }),
                        Menu({
                            opcao_unica: [
                                'observacoes'
                            ],
                            parametros: {
                                tipo: 'contrato'
                            }
                        })
                    ].join('')
                })}

                ${
                    Menu({
                        opcao_unica: [
                            'perguntas-frequentes'
                        ]
                    })
                }

                ${
                    Menu({
                        opcao_unica: [
                            'tutoriais'
                        ]
                    })
                }

            </ul>
            
            <ul class="ms-auto">
                <li>
                    <a id="btn-logout">
                        <i class="fa-solid fa-right-from-bracket"></i> Sair
                    </a>
                </li>    
            </ul>
        </div>
        
        <div id="menu-mobile" class="container d-block d-md-none">
            <ul>
                <li>
                    <a id="home-breadcrumb-3" href="index.html">
                        <i class="fa-solid fa-house"></i> Home
                    </a>
                </li>

                ${Menu({
                    opcao_unica: 'Empreendimentos'
                })}

                ${CaixaDeOpcoes({
                    titulo: 'Pré-contratação',
                    lista: Menu({
                        excluir_opcoes: [
                            'Empreendimentos',
                            'Contrato'
                        ]
                    }),
                    status: 1,
                    classes: 'desativado'
                })}

                ${CaixaDeOpcoes({
                    titulo: 'Contratação',
                    lista: Menu({
                        excluir_opcoes: [
                            'Empreendimentos',
                            'Contrato'
                        ]
                    }),
                    status: 1,
                    classes: 'desativado'
                })}

                

                ${CaixaDeOpcoes({
                    titulo: 'Gestão de contratos',
                    lista: [
                        Menu({
                            mostrar_apenas: [
                                'atividades',
                                'contratos',
                                'aeic'
                            ]
                        }),
                        Menu({
                            opcao_unica: [
                                'observacoes'
                            ],
                            parametros: {
                                tipo: 'contrato'
                            }
                        })
                    ].join('')
                })}

                ${
                    Menu({
                        opcao_unica: [
                            'perguntas-frequentes'
                        ]
                    })
                }

                ${
                    Menu({
                        opcao_unica: [
                            'tutoriais'
                        ]
                    })
                }

            </ul>
            
            <ul class="ms-auto">
                <li>
                    <a id="btn-logout">
                        <i class="fa-solid fa-right-from-bracket"></i> Sair
                    </a>
                </li>    
            </ul>
        </div>
        <div id="menu-hide-area"></div>`;

    }

}