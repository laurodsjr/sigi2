export function TextoResumido(parametros = {
    texto: '',
    limite_de_letras: 120
}) {
    const {
        texto = '',
        limite_de_letras = 120
    } = parametros

    if (texto.length > Number(limite_de_letras)) {
      return texto.substring(0, Number(limite_de_letras)) + "...";
    } else {
      return texto;
    }
}