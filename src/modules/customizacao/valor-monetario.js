export function ValorMonetario(propriedades = {
    id: '',
    moeda: 'BRL', // Moeda padrão é o real brasileiro
    simbolo: 'R$', // Símbolo da moeda padrão é o real brasileiro
    tipo,
    numero
}) {
    const {
        id,
        moeda = 'BRL',
        simbolo = 'R$',
        tipo = '',
        numero = 0
    } = propriedades;

    // Função para formatar valor como moeda no padrão especificado
    function formatarMoeda(valor) {
        if (!moeda) {
            return `${simbolo} ${valor.toFixed(2).replace('.', ',')}`;
        } else {
            return valor.toLocaleString('pt-BR', { style: 'currency', currency: moeda });
        }
    } 

    if (tipo !== '') {
        return formatarMoeda(numero);
    } else {
    
        const inputElement = document.getElementById(id);

        if (!inputElement && tipo === '') {
            console.error(`Elemento com ID "${id}" não encontrado.`);
            return;
        }

        // Função para remover caracteres não numéricos e formatar como moeda
        function adicionarMascara(event) {
            const valor = event.target.value;
            const valorNumerico = valor.replace(/\D/g, '');

            // Verifica se o valor é vazio ou igual a zero
            if (valorNumerico === '') {
                event.target.value = '';
            } else {
                const valorFormatado = formatarMoeda(Number(valorNumerico) / 100);
                event.target.value = valorFormatado;
            }
        }

        // Evento para chamar a função de adicionar máscara em tempo de digitação
        inputElement.addEventListener('input', adicionarMascara);

        // Evento para impedir a inserção de caracteres não numéricos
        inputElement.addEventListener('keydown', function (event) {
            const teclaPermitida = [
                'Backspace', 'ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown', 'Tab', 'Delete', 'Enter', 'Home', 'End'
            ];

            if (!teclaPermitida.includes(event.key) && isNaN(event.key)) {
                event.preventDefault();
            }
        });

    }
}

export function ReverteValorMonetario(valor = 0) {
    console.log('VALOR: ',valor)
    if (valor === '' || valor === undefined || valor === null) {
        return 0;
    }

    if (typeof valor !== 'string') {
        return valor
    } else {
        return parseFloat(valor.replace(/[^\d,]/g, '').replace(',', '.'));
    }
    
}

