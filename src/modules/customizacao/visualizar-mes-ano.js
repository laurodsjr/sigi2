export function ConverteMesAno(dataString) {
    // Separa o ano e o mês da string original
    const [ano, mes] = dataString.split('-');
  
    // Monta a nova data no formato desejado 'MM/YY'
    const novaData = `${mes}/${ano.slice(-2)}`;
  
    return novaData;
}