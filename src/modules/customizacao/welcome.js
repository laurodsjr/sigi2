import { DB } from "../migrations/init-indexeddb";

export function Welcome() {
    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('page-home') !== null) {

            

            const servicos = {
                atividades: 0,
                contratos: 0,
                aeic: 0,
                observacoes: 0
            };

            StatusDoServico({
                servico: 'atividades'
            })
            .then(registros => {
                servicos['atividades'] = registros.registros;
            
                StatusDoServico({
                    servico: 'contratos'
                })
                .then(registros => {
                    servicos['contratos'] = registros.registros;

                    StatusDoServico({
                        servico: 'aeic'
                    })
                    .then(registros => {
                        servicos['aeic'] = registros.registros;

                        StatusDoServico({
                            servico: 'observacoes'
                        })
                        .then(registros => {
                            servicos['observacoes'] = registros.registros;

                            const body_welcome = document.getElementById('page-home');

                            const content = `<section class="container">
                                <div class="row mb-md-0">
                                    <div class="col-12 col-md-12 pt-3 pt-md-5">
                                        <div class="alert alert-info w-100" role="alert">
                                            <h1 class="text-center">Seja bem vindo(a) ao Protótipo Interativo do Projeto SiGi II!</h1>

                                            <p>Este é um ambiente de simulação e testes de mesa dos processos, com o puro e simples objetivo de validar os processos para a construção definitiva do futueo sistema qeu será desenvolvido futuramente, após sucessivas e exaustivas análises, com rodas de feedbacks e correções e ajustes necessários até a chegada de um consenso comum!</p>
                                            <p><strong>OBS:</strong> Tudo aqui é apenas uma simulação, ou seja, nada fica salvo para todos, somente no seu computador ou dispositivo acessado. Podem haver travamentos ou bugs indesejados, justamente por ser um ambiente de testes e validação!</p>
                                            <p>Apagar cache e dados, irá realmente resetar tudo o que fora inserido aqui!</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pb-3 pb-md-5 d-flex align-items-stretch">
                                    <div class="col-12 col-md-4 pt-0 pt-md-3">
                                        <div class="alert alert-warning w-100 h-100" role="alert">
                                            <h2 class="h3">Módulos pausados:</h2>

                                            <ul>
                                                <li>Solicititações</li>
                                                <li>Empreendimentos</li>
                                                <li>Solicitações de Reunião</li>
                                                <li>Reuniões</li>
                                                <li>Solicitações de visita técnica</li>
                                                <li>Visitas técnicas</li>
                                                <li>Solicitações de Orçamento</li>
                                                <li>Orçamentos</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-4 pt-3 pt-md-3">
                                        <div class="alert alert-success w-100 h-100" role="alert">
                                            <h2 class="h3">Módulos desenvolvidos:</h2>

                                            <ul>
                                                <li>Atividades</li>
                                                <li>Contratos</li>
                                                <li>Observações</li>
                                                <li>A.E.I.C.</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-4 pt-3 pt-md-3">
                                        <div class="alert alert-danger w-100 h-100" role="alert">
                                            <h2 class="h3">Módulos à desenvolver:</h2>

                                            <p>Nenhum módulo a desenvolver no momento!</p>

                                            <!--
                                            <ul>
                                                
                                            </ul>
                                            -->
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3 mb-md-3">
                                    <div class="col-12 col-md-12">
                                        <div class="w-100">
                                            <h1 class="text-center">Status</h1>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3 mb-md-5 px-md-0 d-md-flex align-items-md-stretch">

                                    <div class="col-6 col-md-3 mb-3 mb-md-0">
                                        <div id="status-atividades" class="w-100 bg-success text-white rounded p-2 block-select">
                                            <h4>
                                                Atividades
                                            </h4>
                                            <span id="status-atividades" class="status-welcome">
                                                ${servicos.atividades}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-6 col-md-3 mb-3 mb-md-0">
                                        <div id="status-contratos" class="w-100 bg-success text-white rounded p-2 block-select">
                                            <h4>
                                                Contratos
                                            </h4>
                                            <span class="status-welcome">
                                                ${servicos.contratos}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-6 col-md-3 mb-3 mb-md-0">
                                        <div id="status-aeic" class="w-100 bg-success text-white rounded p-2 block-select">
                                            <h4>
                                                A.E.I.C.s
                                            </h4>
                                            <span class="status-welcome">
                                                ${servicos.aeic}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-6 col-md-3">
                                        <div id="status-observacoes" class="w-100 bg-success text-white rounded p-2 block-select">
                                            <h4>
                                                Observações
                                            </h4>
                                            <span class="status-welcome">
                                                ${servicos.observacoes}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </section>`; 

                            body_welcome.innerHTML = content


                        }).catch(erro => console.log(`Erro: ${erro}`))
                    }).catch(erro => console.log(`Erro: ${erro}`))

                }).catch(erro => console.log(`Erro: ${erro}`))
            
            }).catch(erro => console.log(`Erro: ${erro}`))

        }
    })
}

function StatusDoServico(parametros = {}) {
    const {
        servico
    } = parametros

    return new Promise(function(resolve, reject) {
        if (!servico) {
          reject("O nome da tabela não foi fornecido nos parâmetros.");
          return;
        }
    
        if (!DB[servico]) {
          reject(`A tabela "${servico}" não existe no banco de dados.`);
          return;
        }
    
        DB[servico]
          .count()
          .then(registros => {
            resolve({
              registros: registros
            });
          })
          .catch(erro => {
            console.error(`Houve um erro ao contar os registros da tabela ${servico}! Erro: ${erro}`);
            reject(`Erro ao contar os registros da tabela ${servico}.`);
          });
    });
}