export function EncerraSessao(e) {
    console.log('Sessão encerrada!')
    sessionStorage.removeItem('usuario_autenticado');
    location.href='login.html';
}