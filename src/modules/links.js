/**
 * Níveis de acess
 * 
 *  1: Administrador
 *  2: Gestor
 *  3: Engenheiro
 *  4: Gerente
 */

/**
 * Status do link
 * 
 * 0 : Inativo
 * 1 : Ativo
 */

export const links = [
    {
        link:           'solicitacoes',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Solicitações',
        quem_acessa:    [1,2,3],
        status:         0
    },
    {
        link:           'empreendimentos',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Empreendimentos',
        quem_acessa:    [1,2,3],
        status:         0
    },
    {
        link:           'observacoes',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Observações',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'solicitacoes-de-reuniao',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Solicitações de Reuniões',
        quem_acessa:    [1,2],
        status:         0
    },
    {
        link:           'reunioes',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Reuniões',
        quem_acessa:    [1,2,3,4],
        status:         0
    },
    {
        link:           'solicitacoes-de-visitas-tecnicas',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Solicitações visita técnica',
        quem_acessa:    [1,2],
        status:         0
    },
    {
        link:           'visitas-tecnicas',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Visitas técnicas',
        quem_acessa:    [1,2,3,4],
        status:         0
    },
    {
        link:           'solicitacoes-de-orcamentos',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Solicitações de orçamentos',
        quem_acessa:    [1,2],
        status:         0
    },
    {
        link:           'orcamentos',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Orçamentos',
        quem_acessa:    [1,2,3,4],
        status:         0
    },
    {
        link:           'contratos',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Contratos',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'atividades',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Atividades',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'aeic',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'A.E.I.C.',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'atividades-de-contrato',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Atividades de contrato',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'observacoes-de-contrato',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         'Observações',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'perguntas-frequentes',
        id_css:         'faq',
        classe_css:     'link-prototipo',
        icone:          'fas fa-question-circle me-1',
        titulo:         'Perguntas frequentes',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    {
        link:           'tutoriais',
        id_css:         'faq',
        classe_css:     'link-prototipo',
        icone:          'fas fa-book me-1',
        titulo:         'Tutoriais',
        quem_acessa:    [1,2,3,4],
        status:         1
    },
    /*{
        link:           '',
        id_css:         '',
        classe_css:     'link-prototipo',
        icone:          '',
        titulo:         '',
        quem_acessa:    [],
        status:         0
    }*/
];