import { ReverteValorMonetario } from "../../customizacao/valor-monetario";
import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";
import { IncluiNovaReferencia } from "./inclui-nova-referencia";

export function CadastraAEIC() {

    /**
     * Recebendo os valores dos campos
     */
    let 
        aeic_do_contrato = document.getElementById("opcoes-aeic-contrato").value,
        aeic_do_contrato_selecionado = document.getElementById("opcoes-aeic-contrato-selecionado").value,
        atividade_do_aeic = document.getElementById("opcoes-aeic-atividade").value,
        atividade_do_aeic_selecionado = document.getElementById("opcoes-aeic-atividade-selecionado").value,
        mes_ano = document.getElementById("data-incicio-execucao-contrato").value,
        referencia = document.getElementById("opcoes-referencia-do-aeic").value,
        referencia_selecionado = document.getElementById("opcoes-referencia-do-aeic-selecionado").value,
        categoria = document.getElementById("opcoes-categoria-do-aeic").value,
        categoria_selecionado = document.getElementById("opcoes-categoria-do-aeic-selecionado").value,
        valor = ReverteValorMonetario(document.getElementById("valor-aeic").value),
        reajuste = document.getElementById("reajuste").value,
        valor_de_reajuste = ReverteValorMonetario(document.getElementById("valor-de-reajuste").value)
    ;

    let field_aeic_do_contrato = document.getElementById("field-opcoes-aeic-contrato");
    let aviso = field_aeic_do_contrato.querySelector('.text-danger');
    
    let field_atividade_do_aeic = document.getElementById("field-opcoes-aeic-atividade");
    let aviso2 = field_atividade_do_aeic.querySelector('.text-danger');

    let field_referencia = document.getElementById("field-opcoes-referencia-do-aeic");
    let aviso3 = field_referencia.querySelector('.text-danger');

    let field_categoria = document.getElementById("field-opcoes-categoria-do-aeic");
    let aviso4 = field_categoria.querySelector('.text-danger');

    let field_valor = document.getElementById("field-valor-aeic");
    let aviso5 = field_valor.querySelector('.text-danger');

    let field_mes_ano = document.getElementById("field-data-incicio-execucao-contrato");
    let aviso6 = field_mes_ano.querySelector('.text-danger');

    if (
        ( categoria == 3 &&
            (
                ( aeic_do_contrato == '' || aeic_do_contrato_selecionado == '' ) || 
                ( atividade_do_aeic == '' || atividade_do_aeic_selecionado == '' ) || 
                ( mes_ano == '' ) || 
                ( referencia == '' || referencia_selecionado == '' ) ||
                ( categoria == '' || categoria_selecionado == '' ) ||
                ( valor == '' || valor <= 0 ) 
            )
        ) || 
        (
            ( categoria == 2 || categoria == 1 ) &&
            (
                ( aeic_do_contrato == '' || aeic_do_contrato_selecionado == '' ) || 
                ( atividade_do_aeic == '' || atividade_do_aeic_selecionado == '' ) || 
                ( mes_ano == '' ) || 
                ( categoria == '' || categoria_selecionado == '' ) ||
                ( valor == '' || valor <= 0 ) 
            )
        )        
    ) {

        if (
            ( categoria == 3  &&
                (
                    ( aeic_do_contrato == '' || aeic_do_contrato_selecionado == '' ) && 
                    ( atividade_do_aeic == '' || atividade_do_aeic_selecionado == '' ) && 
                    ( mes_ano == '' ) && 
                    ( referencia == '' || referencia_selecionado == '' ) &&
                    ( categoria == '' || categoria_selecionado == '' ) &&
                    ( valor == '' || parseFloat(valor) <= 0 )
                )
            ) || 
            ( 
                ( categoria == 2 || categoria == 1 ) &&
                (
                    ( aeic_do_contrato == '' || aeic_do_contrato_selecionado == '' ) && 
                    ( atividade_do_aeic == '' || atividade_do_aeic_selecionado == '' ) && 
                    ( mes_ano == '' ) && 
                    ( categoria == '' || categoria_selecionado == '' ) &&
                    ( valor == '' || valor <= 0 )
                )
            ) 
        ) {
            console.error(Erros.EE0008); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
            aviso3.classList.remove('d-none');
            aviso4.classList.remove('d-none');
            aviso5.classList.remove('d-none');
            aviso6.classList.remove('d-none');
        } 
        
        if ( 
            ( 
                ( categoria == 3 || categoria == 2 || categoria == 1 ) &&
                ( aeic_do_contrato == '' || aeic_do_contrato_selecionado == '' ) 
            )
        ) {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if ( 
            ( 
                ( categoria == 3 || categoria == 2 || categoria == 1 ) &&
                ( atividade_do_aeic == '' || atividade_do_aeic_selecionado == '' )
            )
        ) {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 
        
        if ( 
            ( categoria == 3 ) && 
            ( referencia == '' || referencia_selecionado == '' )
        ) {
            aviso3.classList.remove('d-none');
        } else {
            aviso3.classList.add('d-none');
        }

        if ( 
            ( categoria == '' || categoria_selecionado == '' )
        ) {
            aviso4.classList.remove('d-none');
        } else {
            aviso4.classList.add('d-none');
        }

        if ( 
            ( 
                ( categoria == 3 || categoria == 2 || categoria == 1 ) &&
                ( valor == '' || parseFloat(valor) <= 0 )
            )
        ) {
            aviso5.classList.remove('d-none');
        } else {
            aviso5.classList.add('d-none');
        }

        if ( 
            ( 
                ( categoria == 3 || categoria == 2 || categoria == 1 || categoria == '' ) &&
                ( mes_ano == '' )
            )
        ) {
            aviso6.classList.remove('d-none');
        } else {
            aviso6.classList.add('d-none');
        }

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');
        aviso3.classList.add('d-none');
        aviso4.classList.add('d-none');
        aviso5.classList.add('d-none');
        aviso6.classList.add('d-none');

        let timestamp = Date.now();
        let criacao = new Date(timestamp);
        let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));

        ValidaEntradaDeAEIC(
            aeic_do_contrato_selecionado, 
            atividade_do_aeic_selecionado, 
            mes_ano, 
            categoria_selecionado
        )
        .then((resultado) => {
            if (resultado) {
                // Se a validação for bem-sucedida e não houver registro com categoria superior ou igual no mesmo 'mes_ano'
                // Prossiga com a inserção do novo registro
                // Faça aqui a chamada para a função que insere o registro
                // Exemplo: inserirRegistro(contrato, atividade, mes_ano, categoria, valor);

                let categoria = parseInt(categoria_selecionado);

                let link_atual = window.location.href.split('/');
                let monta_url = '';
                for (let contador = 0; contador <= (link_atual.length-2);contador++) {
                    monta_url += link_atual[contador]+'/';
                }



                let dados_verificados_para_cadastrar = categoria == 3 ? {
                    contrato: parseInt(aeic_do_contrato_selecionado),
                    atividade: parseInt(atividade_do_aeic_selecionado),
                    mes_ano,
                    referencia: parseInt(referencia_selecionado),
                    categoria: parseInt(categoria_selecionado),
                    valor: ReverteValorMonetario(String(valor)),
                    reajuste,
                    reajuste: ReverteValorMonetario(String(valor_de_reajuste))
                } : categoria == 2 ? {
                    contrato: parseInt(aeic_do_contrato_selecionado),
                    atividade: parseInt(atividade_do_aeic_selecionado),
                    mes_ano,
                    categoria: parseInt(categoria_selecionado),
                    valor: ReverteValorMonetario(String(valor)),
                    reajuste,
                    reajuste: ReverteValorMonetario(String(valor_de_reajuste))
                } : {
                    contrato: parseInt(aeic_do_contrato_selecionado),
                    atividade: parseInt(atividade_do_aeic_selecionado),
                    mes_ano,
                    categoria: parseInt(categoria_selecionado),
                    valor: ReverteValorMonetario(String(valor))
                }

                if (referencia_selecionado != '') {
                    IncluiNovaReferencia({
                        valor: referencia_selecionado
                    })
                    .then(retorno => {

                        dados_verificados_para_cadastrar = {
                            contrato: parseInt(aeic_do_contrato_selecionado),
                            atividade: parseInt(atividade_do_aeic_selecionado),
                            mes_ano,
                            referencia: retorno.id,
                            categoria: parseInt(categoria_selecionado),
                            valor: ReverteValorMonetario(String(valor)),
                            reajuste,
                            reajuste: ReverteValorMonetario(String(valor_de_reajuste))
                        }

                        DB.aeic.add(dados_verificados_para_cadastrar)
                        .then((resposta)=>{
                            alert(`Tudo certo! A.E.I.C. cadastrado!`);
                            location.href=monta_url+'lista.html';
                        })
                        .catch((erro)=>{
                            console.error(`${Erros.EE0009} \n Erro: `,erro);
                            alert(`${Erros.EE0009} \n Erro: `,erro);
                        })
                    })
                    .catch(erro => {
                        console.error(` ${erro}`)
                    })
                } else {
                    DB.aeic.add(dados_verificados_para_cadastrar)
                    .then((resposta)=>{
                        alert(`Tudo certo! A.E.I.C. cadastrado!`);
                        location.href=monta_url+'lista.html';
                    })
                    .catch((erro)=>{
                        console.error(`${Erros.EE0009} \n Erro: `,erro);
                        alert(`${Erros.EE0009} \n Erro: `,erro);
                    })
                }
            } else {
                // Se a validação falhar e existir um registro com categoria superior ou igual no mesmo 'mes_ano'
                // Não prossiga com a inserção e informe ao usuário que a categoria é inválida
                console.log("A categoria é inválida para este mês e contrato.");
                alert(`A categoria é inválida para este mês e contrato!`);
            }
        })
        .catch((error) => {
            // Se ocorrer um erro durante a execução da função, trate o erro aqui
            console.error(Erros.EQ0033, error);
        });

    }    

}

async function ValidaEntradaDeAEIC(contrato, atividade, mes_ano, categoria) {

    console.log('CAT: ',parseInt(categoria))

    try {
        // Verificar se existe algum registro com o mesmo 'mes_ano' e uma 'categoria' inferior
        const registrosInferiores = await DB.aeic
            .where({
                contrato: parseInt(contrato),
                atividade: parseInt(atividade),
                mes_ano: mes_ano,
            })
            .toArray();

        const hasCategoriaInferior = registrosInferiores.some(item => item.categoria == parseInt(categoria));

        // Verificar se existe algum registro com a mesma 'categoria' e 'mes_ano' para o mesmo contrato e atividade
        const existeRegistroMesmoMesAnoCategoria = await DB.aeic
            .where({
                contrato: parseInt(contrato),
                atividade: parseInt(atividade),
                mes_ano: mes_ano,
                categoria: parseInt(categoria)
            })
            .first();

            console.log('CAT: ',hasCategoriaInferior,existeRegistroMesmoMesAnoCategoria)

        if (hasCategoriaInferior || String(existeRegistroMesmoMesAnoCategoria) !== 'undefined') {
            // Se houver registros com uma 'categoria' inferior ou se já existir um registro com a mesma 'categoria' e 'mes_ano',
            // retorne false para impedir a inserção
            return false;
        } else {
            return true; // Nenhum registro com uma categoria inferior ou com a mesma categoria e 'mes_ano' foi encontrado, a inserção pode prosseguir
        }
    } catch (error) {
        console.error("Erro ao verificar registro existente:", error);
        return false;
    }
}