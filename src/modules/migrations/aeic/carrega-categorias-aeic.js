import { DB } from "../init-indexeddb";

export function CategoriasDeAEIC() {
    DB.categorias_de_aeic.count()
    .then(registros => {
        if (registros < 1) {
            DB.categorias_de_aeic.bulkAdd([
                {rotulo: 'Baseline', descricao: ''},
                {rotulo: 'Previsto', descricao: ''},
                {rotulo: 'Medido', descricao: ''}
            ]).then((e) => {
    
                console.log(`Categorias de A.E.I.C. carregadas!`,e);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar categorias de A.E.I.C.! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em categorias de A.E.I.C.!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de categoria de A.E.I.C. pré-cadastrados!`);
    });
}