import { DB } from "../init-indexeddb";
import { observarAlteracoesNoSelect, SelectCustomizado } from "../comum/select-customizado";
import { ListaReferenciasAEIC } from "./lista-referencias-aeic";
import { ValorMonetario } from "../../customizacao/valor-monetario";
import { MascaraDePorcentagem } from "../../customizacao/mascara-de-porcentagem";
import { VerificaAtividadesPorContrato } from "./verifica-atividades-por-contrato";
import { ChecaCategoriaDeAEIC } from "./checa-categoria-de-aeic";
import { VerificaAEICExistente } from "./verifica-aeic-existente";
import { CarregaAtividades } from "../contratos/carrega-atividades";
import { Erros } from "../../erros/erros";
import { CarregaListaDeContratos } from "./carrega-lista-de-contratos";

export async function CarregaDadosDoAEIC(pagina_atual = '') {

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);
    
    if (grupo == 'aeic') {
    
        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
                                    
                async function RetornaDados(id) {
                    const retorno = await DB.aeic.get(Number(id))
                    return retorno
                }

                RetornaDados(identificador)
                .then(dados => {

                    CarregaListaDeContratos({
                        id_select: 'opcoes-aeic-contrato',
                        placeholder: 'Escolha um contrato',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-aeic-contrato',
                            placeholder: 'Escolha ou digite um número aqui',
                            sem_resultados: 'Sem mais números!',
                            sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: false,
                            customizar: pagina_atual === 'lista.html' ? false : false,
                            pre_selecionado: parseInt(dados.contrato),
                            pagina: pagina_atual
                        });
                    }).catch(e=>console.error('Erro: ',e))


                    CarregaAtividades('opcoes-aeic-atividade','Escolha uma atividade')
                    .then(opcoes => {
                        SelectCustomizado({
                            id_do_select: 'opcoes-aeic-atividade',
                            placeholder: 'Escolha uma atividade',
                            sem_resultados: 'Sem mais atividades!',
                            sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: false,
                            customizar: pagina_atual === 'lista.html' ? false : false,
                            pre_selecionado: parseInt(dados.atividade),
                            pagina: pagina_atual
                        });
                    }).catch(e=>console.error('Erro: ',e))

                    observarAlteracoesNoSelect({
                        id_do_select: 'opcoes-aeic-atividade',
                        placeholder: 'Escolha uma atividade',
                        sem_resultados: 'Sem mais atividades!',
                        sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                        sugestao_de_adicao: 'Clique para selecionar',
                        adicionavel: false,
                        customizar: pagina_atual === 'lista.html' ? false : false,
                        pre_selecionado: parseInt(dados.atividade),
                        pagina: pagina_atual
                    })

                    ListaReferenciasAEIC({
                        id_select: 'opcoes-referencia-do-aeic',
                        placeholder: 'Escolha ou digite referência',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-referencia-do-aeic',
                            placeholder: 'Escolha ou digite um número aqui',
                            sem_resultados: 'Sem mais números!',
                            sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: parseInt(dados.referencia),
                            pagina: pagina_atual
                        });
                    }).catch(e=>console.error('Erro: ',e))

                    SelectCustomizado({
                        id_do_select: 'opcoes-categoria-do-aeic',
                        placeholder: 'Escolha uma categoria',
                        sem_resultados: 'Sem mais categorias!',
                        sem_resultados_na_pesquia: 'Nenhuma categoria encontrada!',
                        sugestao_de_adicao: 'Clique para selecionar',
                        adicionavel: false,
                        customizar: pagina_atual === 'lista.html' ? false : false,
                        pre_selecionado: Number(dados.categoria),
                        pagina: pagina_atual
                    });

                    ValorMonetario({
                        id: 'valor-aeic'
                    })
            
                    ValorMonetario({
                        id: 'valor-de-reajuste'
                    })

                    MascaraDePorcentagem({
                        id: 'reajuste',
                        calcular: 1,
                        campo_do_calculo: 'valor-aeic'
                    });

                    /**
                     * Checando as atividades vinculadas ao contrato
                     */
                    VerificaAtividadesPorContrato();

                    /**
                     * Checando a categoria de AEIC para exibição de campos
                     */
                    ChecaCategoriaDeAEIC();

                    VerificaAEICExistente();

                    /**
                     * Campos para carregar os dados
                     */
                    document.getElementById("identificador").value = identificador;
                    document.getElementById("identificador").value = identificador;
                    document.getElementById("opcoes-aeic-atividade-selecionado").value = dados.atividade;
                    document.getElementById("opcoes-categoria-do-aeic-selecionado").value = dados.categoria;
                    document.getElementById("opcoes-aeic-contrato-selecionado").value = dados.contrato;
                    document.getElementById("data-incicio-execucao-contrato").value = dados.mes_ano;
                    document.getElementById("reajuste").value = dados.reajuste;
                    document.getElementById("valor-aeic").value = ValorMonetario({
                        moeda: '',
                        simbolo: 'R$',
                        tipo: 'html',
                        numero: dados?.valor || 0
                    });
                    document.getElementById("valor-de-reajuste").value =  ValorMonetario({
                        moeda: '',
                        simbolo: 'R$',
                        tipo: 'html',
                        numero: dados?.reajustado || 0
                    });


                    let mes_ano_div = document.getElementById('area-data-incicio-execucao-contrato');
                    let atividade_div = document.getElementById('area-opcoes-aeic-atividade');
                    let categoria_div = document.getElementById('area-opcoes-categoria-do-aeic');
                    let valor_div = document.getElementById('area-valor-aeic');
                    let reajuste_div = document.getElementById('area-reajuste');
                    let reajustado_div = document.getElementById('area-valor-de-reajuste');
                    let referencia_div = document.getElementById('area-opcoes-referencia-do-aeic');

                    atividade_div.classList.remove('hide-aeic');
                    mes_ano_div.classList.remove('hide-aeic');
                    categoria_div.classList.remove('hide-aeic');
                    valor_div.classList.remove('hide-aeic');

                    if (dados.categoria == 2 || dados.categoria == 3) {
                        reajuste_div.classList.remove('hide-aeic');
                        reajustado_div.classList.remove('hide-aeic');
                    }

                    if (dados.categoria == 3) {
                        referencia_div.classList.remove('hide-aeic');
                    }


                    console.log(`Dados do A.E.I.C. ${identificador} - ${dados.rotulo} carregados com sucesso!`);
                })
                .catch(e => {
                    console.error(`${Erros.EL0010}\n Mensagem: `,e);
                });

    
            })
    
        }

    }

}