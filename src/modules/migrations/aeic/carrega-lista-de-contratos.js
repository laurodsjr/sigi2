import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaListaDeContratos(propriedades = {
    id_select,
    placeholder,
    pagina
}) {

    const {
        id_select = '',
        placeholder = null,
        pagina = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        // Checa se existem departamentos cadastrados
        DB.contratos.count()
        .then(registros => {
            if (registros > 0) {
                const contratos_sap = DB.contratos.toArray()
                .then( (opcao) => { 

                    let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha um contrato`}</option>`;
                    opcao.forEach((item) => {
                        exibir_lista += `<option value="${item.id}">${item.rotulo}</option>`;
                    });

                    // Atualizar o elemento HTML com a lista de origens
                    const listaOrigens = document.getElementById(id_select);
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de opções de contrato carregada!`);
                    resolve();
                })
                .catch(erro=>reject(`${Erros.EQ0018} \n Erro: ${erro}`));
            } else {
                let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Inclua um novo contrato`}</option>`;
                const listaOrigens = document.getElementById(id_select);
                if (pagina == 'lista.html') {
                    document.getElementById(`area-${id_select}`).classList.add('d-none');
                }
                listaOrigens.innerHTML = exibir_lista;
                console.log(`Não há opções de contrato para listar!`);
                resolve();
            }
        })
        .catch(erro=>reject(`${Erros.EQ0042} \n Erro: ${erro}`));

    })

}