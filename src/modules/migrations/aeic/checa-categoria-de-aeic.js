export function ChecaCategoriaDeAEIC() {
    // Função para capturar as mudanças no select e obter o valor selecionado como número
    function capturarValor() {
        var selectElement = document.getElementById('opcoes-categoria-do-aeic');
        var categoriaSelecionada = parseInt(selectElement.value); // Ou parseFloat(selectElement.value) se quiser um valor decimal

        // Faça o que precisar com o valor selecionado aqui, por exemplo:
        // Alguma operação ou exibição na tela
        let referencia = document.getElementById('area-opcoes-referencia-do-aeic');
        let reajuste = document.getElementById('area-reajuste');
        let valor_reajuste = document.getElementById('area-valor-de-reajuste');

        if (categoriaSelecionada == 1) {
            referencia.classList.add('hide-aeic');
            reajuste.classList.add('hide-aeic');
            valor_reajuste.classList.add('hide-aeic');
        }
        
        if (categoriaSelecionada == 2) {
            referencia.classList.add('hide-aeic');
            reajuste.classList.remove('hide-aeic');
            valor_reajuste.classList.remove('hide-aeic');
        }

        if (categoriaSelecionada == 3) {
            referencia.classList.remove('hide-aeic');
            reajuste.classList.remove('hide-aeic');
            valor_reajuste.classList.remove('hide-aeic');
        }

    }

    // Adicionando o evento 'change' ao elemento select
    document.getElementById('opcoes-categoria-do-aeic').addEventListener('change', capturarValor);
}