import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";
import { ListagemAEIC } from "./listagem";

export function ExcluirAEIC() {

    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('listagem-de-aeic') !== null) {
            let listagem = document.getElementById('listagem-de-aeic');
            listagem.addEventListener('click', function(event) {
                if (event.target.matches('#excluir')) {
                    let id = event.target.getAttribute('identificador');
                    console.log('Solicitação de exclusão do identificador de AEIC: ',id);

                    let confirmacao = confirm('Você tem certeza que deseja excluir este A.E.I.C.?');
                    if (confirmacao) {
                        DB.aeic.delete(Number(id))
                        .then(resposta => { 
                            alert(`A.E.I.C. ${id} excluido com sucesso!`);
                            console.log(`A.E.I.C ${id} excluido com sucesso!`);
                            ListagemAEIC()
                        })
                        .catch(e => {
                            console.error(`${Erros.ED0003}\n Erro: `,e);
                        })
                    } else {
                        console.log('Operação cancelada pelo usuário!');
                    }
                    
                }
            })
        }
    });

}