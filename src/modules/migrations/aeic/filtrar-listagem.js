import { MontaLink } from "../../customizacao/monta-link";
import { ValorMonetario } from "../../customizacao/valor-monetario";
import { ConverteMesAno } from "../../customizacao/visualizar-mes-ano";
import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";  

export function FiltrarListagemAEICs() {

    const
        contrato    = document.getElementById('opcoes-aeic-contrato-selecionado')?.value,
        atividade   = document.getElementById('opcoes-aeic-atividade-selecionado')?.value,
        mes_ano     = document.getElementById('mes-ano-aeic')?.value,
        categoria   = document.getElementById('opcoes-categoria-do-aeic-selecionado')?.value
    ;

    DB.referencias_aeic.toArray()
    .then(referencias => {

        function Referencias(id) {
            if (referencias.length <= 0 || String(Number(id)) === "NaN") { return `${id} [Inconsistente]`; } 
            else {
                let referencias_filtradas = referencias.filter(item => {
                    if (item.id == Number(id)) {
                        return item?.numero || item?.id
                    }
                });
                return {
                    status: Number(referencias_filtradas[0]?.status) === 0 ? false : true,
                    resposta: `${referencias_filtradas[0]?.id} # ${referencias_filtradas[0]?.rotulo}${Number(referencias_filtradas[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                };
            }
        }

        DB.contratos.toArray()
        .then(contratos => {

            function Contratos(id) {
                if (contratos.length <= 0 || String(Number(id)) === "NaN") { return `${id} [Inconsistente]`; } 
                else {
                    let contratos_filtrados = contratos.filter(item => {
                        if (item.id == Number(id)) {
                            return item?.numero || item?.id
                        }
                    });
                    return {
                        status: Number(contratos_filtrados[0]?.status) === 0 ? false : true,
                        resposta: `${contratos_filtrados[0]?.id} # ${contratos_filtrados[0]?.rotulo}${Number(contratos_filtrados[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                    };
                }
            }

            DB.atividades.toArray()
            .then(atividades => {

                function Atividades(id) {

                    if (atividades.length <= 0) { return id; } 
                    else {
                        let atividade_filtrada = atividades.filter(item => {
                            if (item.id == Number(id)) {
                                return item?.nome || item?.id
                            }
                        });
                        //console.log(prestador_filtrado);
                        return {
                            status: Number(atividade_filtrada[0]?.status) === 0 ? false : true,
                            resposta: `${atividade_filtrada[0]?.rotulo}${Number(atividade_filtrada[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                        };
                    }
                }

                function Categoria(id) {
                    if (id == 1) {
                        return `<small class="bg-warning rounded w-100 d-block py-1 px-2 text-center block-select">
                            Baseline
                        </small>`;
                    }

                    if (id == 2) {
                        return `<small class="bg-info rounded w-100 d-block py-1 px-2 text-center text-white block-select">
                            Previsto
                        </small>`;
                    }

                    if (id == 3) {
                        return `<small class="bg-success rounded w-100 d-block text-white py-1 px-2 text-center block-select">
                            Medido
                        </small>`;
                    }
                }

                function FiltrarAEICs(parametros = {}) {
                    const {
                      contrato = 0,
                      atividade = 0,
                      mes_ano = '',
                      categoria = 0
                    } = parametros;

                    console.log('Par: ',parametros)
                  
                    return new Promise(function(resolve, reject) {

                        let consulta = DB.aeic;
            
                        if (parseInt(contrato) > 0) {
                            consulta = consulta.filter(registro => registro.contrato === parseInt(contrato));
                        }
                    
                        if (parseInt(atividade) > 0) {
                            consulta = consulta.filter(registro => registro.atividade === parseInt(atividade));
                        }
                    
                        if (mes_ano !== '') {
                            consulta = consulta.filter(registro => registro.mes_ano === mes_ano);
                        }
                    
                        if (parseInt(categoria) > 0) {
                            consulta = consulta.filter(registro => registro.categoria === parseInt(categoria));
                        }

                        /**
                         * 
                            .orderBy('mes_ano') 
                            .reverse()
                         */

                        //console.log('Cont: ',parseInt(contrato))

                        consulta               
                        .toArray()
                        .then(aeic => {

                            /*
                            const resultadoOrdenado = aeic.sort((a, b) => {
                                return new Date(b.mes_ano) - new Date(a.mes_ano);
                            });

                            console.log('Result: ',aeic)
                            resolve({
                                registros: resultadoOrdenado.length,
                                dados: resultadoOrdenado
                            });*/

                            aeic.sort((a, b) => {
                                // Primeiro, ordenamos por categoria em ordem crescente
                                if (a.categoria < b.categoria) return -1;
                                if (a.categoria > b.categoria) return 1;
                                // Se as categorias forem iguais, então ordenamos por mes_ano em ordem decrescente
                                return new Date(b.mes_ano) - new Date(a.mes_ano);
                            });

                            //console.log('Result: ',aeic)
                            resolve({
                                registros: aeic.length,
                                dados: aeic
                            });
                        })
                        .catch(erro => {
                            console.error(`Houve um erro ao tentar filtrar os A.E.I.C.s! Erro: ${erro}`);
                            resolve({
                                registros: 0,
                                dados: []
                            });
                        });
                    });
                } 

                FiltrarAEICs({
                    contrato,
                    atividade,
                    mes_ano,
                    categoria
                })
                .then(retorno => {
                    if (retorno.registros > 0) {
                        FiltrarAEICs({
                            contrato,
                            atividade,
                            mes_ano,
                            categoria
                        })
                        .then((retorno) => {    
                        
                            let lista = ReordenadorDeLista(retorno.dados);
                                
                            let exibir_tabela = `<table id="table-list" class="w-100">
                                <thead id="table-title">
                                    <tr>
                                        <th class="p-2 block-select">ID</th>
                                        <th class="p-2 block-select">CONTRATO</th>
                                        <th class="p-2 block-select">ATIVIDADE</th>
                                        <th class="p-2 block-select">MÊS/ANO</th>
                                        <th class="p-2 block-select">REFERÊNCIA</th>
                                        <th class="p-2 block-select">VALOR</th>
                                        <th class="p-2 block-select">REAJUSTE (%)</th>
                                        <th class="p-2 block-select">VALOR REAJUSTE</th>
                                        <th class="p-2 block-select">TIPO</th>
                                        <th class="p-2 block-select last">AÇÕES</th>
                                    </tr>
                                </thead>
                            <tbody>`;

                            lista.forEach((item, key) => {

                                let contrato = Contratos(item.contrato).resposta;
                                let atividade = Atividades(item.atividade).resposta;
                                let categoria = Categoria(item.categoria);
                                let referencia = Referencias(item?.referencia).resposta

                                console.log('Teste: ',contrato,atividade);

                                exibir_tabela += `<tr class="${key % 2 !== 0 ? 'even-line' : ''}">
                                    <td class="${'p-2'}">${item.id}</td>
                                    <td class="${'p-2'}">${contrato}</td>
                                    <td class="${'p-2'}">${atividade}</td>
                                    <td class="${'p-2'}">${ConverteMesAno(item.mes_ano)}</td>
                                    <td class="${'p-2'}">${referencia || '-'}</td>
                                    <td class="${'p-2'}">${ValorMonetario({
                                        tipo: 'html',
                                        numero: item.valor
                                    })}</td>
                                    <td class="${'p-2'}">${item?.reajuste || '0%'}</td>
                                    <td class="${'p-2'}">${ValorMonetario({
                                        tipo: 'html',
                                        numero: item.valor_reajuste
                                    })}</td>
                                    <td class="${'p-2'}">${categoria}</TD>
                                    <td class="${'p-2 actions block-select'}">
                                        <a href="${MontaLink(`aeic/visualizar.html?id=${item.id}`)}">
                                            <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="aeic" identificador="${item.id}"></i>
                                        </a>
                                        <a href="${MontaLink(`aeic/editar.html?id=${item.id}`)}">
                                            <i class="fa-solid fa-pen-to-square" id="editar" tipo="aeic" identificador="${item.id}"></i>
                                        </a>
                                        <i class="fa-solid fa-trash-can" id="excluir" tipo="aeic" identificador="${item.id}"></i>
                                    </td>
                                </tr>`;
                            });

                            exibir_tabela += `</tdoby></table>`;

                            // Atualizar o elemento HTML com a lista de origens
                            if (document.getElementById("listagem-de-aeic") !== null) {
                                const listaOrigens = document.getElementById("listagem-de-aeic");
                                listaOrigens.innerHTML = exibir_tabela;
                                console.log(`Lista de aeic carregada!`);
                            }
                            
                        }).catch(e => {
                
                            console.error(`${Erros.EE0009}\n Mensagem: ${e}`);
                    
                        })

                    } else {
                        
                        if (document.getElementById("listagem-de-aeic") !== null) {
                            const listaOrigens = document.getElementById("listagem-de-aeic");
                            listaOrigens.innerHTML = `<p class="text-center text-italic p-3">Não há A.E.I.C's cadastrados!</p>`;
                            console.log(`Não há A.E.I.C's cadastrados!`);
                        }

                    }
                })
                .catch(e => {
                    console.error(`${Erros.EQ0041}\n Mensagem: `,e);
                });


            }).catch(e => console.error(`${Erros.EQ0040} Erro: `,e));

        }).catch(e => console.error(`${Erros.EQ0039} Erro: `,e));

    }).catch(e => console.error(`${Erros.EQ0038} Erro: `,e));

}

function ReordenadorDeLista(dados) {
    // Função para organizar os objetos pela categoria em ordem decrescente
    function sortByCategoriaDesc(a, b) {
        return b.categoria - a.categoria;
    }

    // Organiza o Array conforme as instruções
    let reorganizado = {};

    dados.forEach((item) => {
        const { mes_ano } = item;
        if (!reorganizado[mes_ano]) {
            reorganizado[mes_ano] = [];
        }
        reorganizado[mes_ano].push(item);
    });

    // Ordena cada grupo de acordo com a categoria em ordem decrescente
    for (const month in reorganizado) {
        if (Array.isArray(reorganizado[month])) {
            reorganizado[month].sort(sortByCategoriaDesc);
        }
    }

    // Faz o merge dos subarrays em um único array
    const mergedArray = Object.values(reorganizado).reduce((acc, curr) => acc.concat(curr), []);

    return mergedArray;
}