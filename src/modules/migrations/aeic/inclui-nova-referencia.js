import { DB } from "../init-indexeddb";

export function IncluiNovaReferencia(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    //console.log('ID: ',valor)

    return new Promise(function(resolve, reject) {
    
        if (String(Number(valor)) == 'NaN') {
            DB.referencias_aeic
            .add({
                rotulo: valor,
                status: Number(0)
            })
            .then( (primaryKey) => { 
                console.log(`Nova referência incluída para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir a nova referência! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.referencias_aeic.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Esta referência já foi revisada e aprovada!` : `Esta referência já foi cadastrada e será revisada!` );
                resolve({
                    id: valor,
                    consolidado: dados.status ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem da referência! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}