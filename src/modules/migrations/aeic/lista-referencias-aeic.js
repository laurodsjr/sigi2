import { DB } from "../init-indexeddb";

export function ListaReferenciasAEIC(propriedades = {
    id_select,
    placeholder,
    pagina
}) {

    const {
        id_select = '',
        placeholder = null,
        pagina = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        // Checa se existem departamentos cadastrados
        DB.referencias_aeic.count()
        .then(registros => {
            if (registros > 0) {
                const contratos_sap = DB.referencias_aeic.toArray()
                .then( (opcao) => { 

                    let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha uma referência`}</option>`;
                    opcao.forEach((item) => {
                        exibir_lista += `<option value="${item.id}">${item.rotulo}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                    });

                    // Atualizar o elemento HTML com a lista de origens
                    const listaOrigens = document.getElementById(id_select);
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de opções de referências de A.E.I.C. carregada!`);
                    resolve();
                })
                .catch(erro=>reject(`Houve um erro ao tentar listar as opções de referências de A.E.I.C. disposníveis! \n Erro: ${erro}`));
            } else {
                let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Inclua uma nova referência`}</option>`;
                const listaOrigens = document.getElementById(id_select);
                if (pagina == 'lista.html') {
                    document.getElementById(`area-${id_select}`).classList.add('d-none');
                }
                listaOrigens.innerHTML = exibir_lista;
                console.log(`Não há opções de referências de A.E.I.C. para listar!`);
                resolve();
            }
        })
        .catch(erro=>reject(`Houve um erro ao tentar contar as opções de referências de A.E.I.C. disposníveis! \n Erro: ${erro}`));

    })

}