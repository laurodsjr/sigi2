export function MostrarCampos() {
    function capturarValor() {
        var selectElement = document.getElementById('opcoes-aeic-contrato');
        var contratoSelecionado = parseInt(selectElement.value); // Ou parseFloat(selectElement.value) se quiser um valor decimal

        // Faça o que precisar com o valor selecionado aqui, por exemplo:
        // Alguma operação ou exibição na tela
        let mes_ano = document.getElementById('area-data-incicio-execucao-contrato');
        let categoria = document.getElementById('area-opcoes-categoria-do-aeic');
        let valor = document.getElementById('area-valor-aeic');

        if (contratoSelecionado ) {
            mes_ano.classList.remove('hide-aeic');
            categoria.classList.remove('hide-aeic');
            valor.classList.remove('hide-aeic');
        }

    }

    // Adicionando o evento 'change' ao elemento select
    document.getElementById('opcoes-aeic-contrato').addEventListener('change', capturarValor);
}