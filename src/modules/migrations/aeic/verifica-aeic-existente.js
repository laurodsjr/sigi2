import { ValorMonetario } from "../../customizacao/valor-monetario";
import { DB } from "../init-indexeddb";

export function VerificaAEICExistente() {
    // Obtém uma referência para o elemento de input
    const 
        mes_ano = document.getElementById('data-incicio-execucao-contrato')
    ;

    let 
        baseline = document.getElementById('aeic-baseline'),
        previsto = document.getElementById('aeic-previsto'),
        medido = document.getElementById('aeic-medido')
    ;
    baseline    = baseline.querySelector('.info');
    previsto    = previsto.querySelector('.info');
    medido      = medido.querySelector('.info');

    async function VerificarAEICs(dados = {}) {
        try {
            //const categoriaAnterior = categoria == 1 ? 1 : categoria - 1;
            const registro = await DB.aeic
            .where(dados)
            .toArray();
            return registro;
        } catch (error) {
          console.error("Erro ao verificar registro existente:", error);
          return false;
        }
    }

    // Adiciona um event listener para o evento 'input'
    mes_ano.addEventListener('input', () => {
        // Quando o valor é alterado, esta função será chamada
        // Você pode acessar o valor selecionado através da propriedade 'value'
        const valor = mes_ano.value;

        const 
            aeic_do_contrato = document.getElementById("opcoes-aeic-contrato-selecionado").value,
            atividade_do_aeic = document.getElementById("opcoes-aeic-atividade-selecionado").value,
            categoria_selecionado = document.getElementById("opcoes-categoria-do-aeic-selecionado").value
        ;

        // Aqui você pode realizar as ações desejadas com o valor selecionado
        let dados = {
            contrato: parseInt(aeic_do_contrato),
            atividade: parseInt(atividade_do_aeic),
            //mes_ano: mes_ano.value,
            //categoria: parseInt(categoria_selecionado),
        }

        //console.log('Tratados: ',dados)
        VerificarAEICs(dados)
        .then(retorno => {
            Organizador(retorno);
        })
        .catch(erro => {
            console.error(`Erro: ${erro}`);
            LimpaPreview();
        })

    });
}

function Organizador(dados) {

    const 
        baseline = document.getElementById('aeic-baseline'),
        previsto = document.getElementById('aeic-previsto'),
        medido = document.getElementById('aeic-medido')
    ;

    // Organizar os dados por mês em ordem crescente
    dados.sort((a, b) => (a.mes_ano > b.mes_ano) ? 1 : -1);

    // Função para converter a data no formato 'AAAA-MM' para 'MM/AA'
    function formatarData(data) {
        const [ano, mes] = data.split('-');
        return `${mes}/${ano.slice(2)}`;
    }

    // Função para carregar os dados no HTML de acordo com a categoria
    function carregarDadosPorCategoria() {

        let 
            baseline = document.getElementById('aeic-baseline'),
            previsto = document.getElementById('aeic-previsto'),
            medido = document.getElementById('aeic-medido')
        ;
        const baselineDiv    = baseline.querySelector('.info');
        const previstoDiv    = previsto.querySelector('.info');
        const medidoDiv      = medido.querySelector('.info');

        let baselineHTML = '';
        let previstoHTML = '';
        let medidoHTML = '';

        dados.forEach((item) => {
            const dataFormatada = formatarData(item.mes_ano);
            const valorFormatado = ValorMonetario({
                tipo: 'html',
                numero: item.valor
            });

            switch (item.categoria) {
            case 1:
                baselineHTML += `${dataFormatada} - ${valorFormatado}<br>`;
                break;
            case 2:
                previstoHTML += `${dataFormatada} - ${valorFormatado}<br>`;
                break;
            case 3:
                medidoHTML += `${dataFormatada} - ${valorFormatado}<br>`;
                break;
            // Adicione mais cases se houver mais categorias
            default:
                break;
            }
        });

        baselineDiv.innerHTML = baselineHTML;
        previstoDiv.innerHTML = previstoHTML;
        medidoDiv.innerHTML = medidoHTML;

        // Remover a classe '.d-none' dos elementos se houver dados para a categoria
        if (baselineHTML !== '') {
            baseline.classList.remove('d-none');
        }
  
        if (previstoHTML !== '') {
            previsto.classList.remove('d-none');
        }
  
        if (medidoHTML !== '') {
            medido.classList.remove('d-none');
        }

    }

    // Chamar a função para carregar os dados
    carregarDadosPorCategoria();

}

function LimpaPreview() {
    const 
        baseline = document.getElementById('aeic-baseline'),
        previsto = document.getElementById('aeic-previsto'),
        medido = document.getElementById('aeic-medido')
    ;

    const baselineDiv = document.getElementById('baseline');
    const previstoDiv = document.getElementById('previsto');
    const medidoDiv = document.getElementById('medido');

    baselineDiv.innerHTML = '';
    previstoDiv.innerHTML = '';
    medidoDiv.innerHTML = '';

    // Adiciona a classe '.d-none' dos elementos das categorias
    if (baselineHTML !== '') {
        baseline.classList.add('d-none');
    }

    if (previstoHTML !== '') {
        previsto.classList.add('d-none');
    }

    if (medidoHTML !== '') {
        medido.classList.add('d-none');
    }
}