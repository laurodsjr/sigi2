import { SelectCustomizado } from "../comum/select-customizado";
import { CarregaAtividades } from "../contratos/carrega-atividades";

export function VerificaAtividadesPorContrato() {

    // Função para capturar as mudanças no select e obter o valor selecionado como número
    function capturarValor() {
        var selectElement = document.getElementById('opcoes-aeic-contrato');
        var contratoSelecionado = parseInt(selectElement.value); // Ou parseFloat(selectElement.value) se quiser um valor decimal

        // Faça o que precisar com o valor selecionado aqui, por exemplo:
        // Alguma operação ou exibição na tela        
        CarregaAtividades('opcoes-aeic-atividade','Escolha uma atividade',contratoSelecionado)
        .then(opcoes => {
            /*SelectCustomizado({
                id_do_select: 'opcoes-aeic-atividadee',
                placeholder: 'Escolha uma atividade',
                sem_resultados: 'Sem mais atividades!',
                sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                sugestao_de_adicao: 'Clique para selecionar',
                adicionavel: false,
                customizar: false
            });*/
            // Remover a classe 'hide-aeic' do elemento após selecionar um valor
            var areaOpcoesElement = document.getElementById('area-opcoes-aeic-atividade');
            areaOpcoesElement.classList.remove('hide-aeic');
        }).catch(e=>console.error('Erro: ',e))

    }

    // Adicionando o evento 'change' ao elemento select
    document.getElementById('opcoes-aeic-contrato').addEventListener('change', capturarValor);

}