import { DB } from "../init-indexeddb";

export function CadastraAtividadesDoContrato(parametros = {
    atividades: '',
    contrato: 0
}) {
    const {
        atividades = '',
        contrato = 0
    } = parametros

    const atividades_em_array = atividades.split(',');
    let atividades_do_contrato = []
    
    atividades_em_array.map((string)=>{
        atividades_do_contrato.push({
            id_contrato: Number(contrato),
            id_atividade: Number(string)
        })
    });

    return new Promise(function(resolve, reject) {
    
        if (contrato == 0) {
            reject(`Contrato não identificado!`);
        }

        //console.log('Teste: ',atividades_do_contrato);

        if (atividades_do_contrato.length > 0) {

            DB.atividades_dos_contratos.bulkAdd(atividades_do_contrato)
            .then((dados) => {
        
                console.log(`Atividades atribuidas ao contrato!`,dados);
                resolve()
        
            }).catch(e => {
                reject(`Houve um erro ao tentar atribuir as atividades ao contrato! Erro: ${e}`);
            })       

        }        

    })
    
}