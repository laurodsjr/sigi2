import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaAtividadesDoContrato(parametros = {
    contrato: 0
}) {
    const {
        contrato = 0
    } = parametros

    let atividades_do_contrato = []

    return new Promise(function(resolve, reject) {
    
        if (contrato == 0) {
            reject(`Contrato não identificado!`);
        }


        //DB.atividades_dos_contratos.get(atividades_do_contrato)
        let consulta = DB.atividades_dos_contratos;
        if (contrato) {
            consulta.filter(registro => registro.id_contrato === contrato);
        }
        consulta.toArray()     
        .then(dados => {

            dados.map((item)=>{
                atividades_do_contrato.push(String(item.id_atividade))
            });
    
            resolve(atividades_do_contrato)
    
        }).catch(e => {
            reject(`${Erros.EL0006}! Erro: ${e}`);
        })

    })
    
}