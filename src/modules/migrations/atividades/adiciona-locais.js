import { DB } from "../init-indexeddb";

export function InserirLocais() {
    DB.locais.count()
    .then(registros => {
        if (registros < 1) {
            DB.locais.bulkAdd([
                {rotulo: 'SE Itabera', descricao: '', status: 1},
                {rotulo: 'CTE', descricao: '', status: 1},
                {rotulo: 'Estação Rep - Rota Itaipú', descricao: '', status: 1},
                {rotulo: 'LT 345 KV - Itapetí - Nordeste', descricao: '', status: 1},
                {rotulo: 'LT 750 KV - Itab - Tij I - Ref', descricao: '', status: 1},
                {rotulo: 'LT 750 KV - Itab - Tij II - Ref', descricao: '', status: 1},
                {rotulo: 'LT 750 KV - Iva - Itab I - Ref', descricao: '', status: 1},
                {rotulo: 'LT 750 KV - Iva - Itab II - Ref', descricao: '', status: 1},
                {rotulo: 'LT MASCARENHAS/LINHARES', descricao: '', status: 1},
                {rotulo: 'Rede Telemetrica', descricao: '', status: 1},
                {rotulo: 'SE Araraquara', descricao: '', status: 1}
            ]).then((e) => {
    
                console.log(`Locais iniciais carregados!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar locais inciais! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em locais!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de locais pré-cadastrados!`);
    });
}