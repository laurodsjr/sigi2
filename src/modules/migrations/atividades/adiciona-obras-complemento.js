import { DB } from "../init-indexeddb";

export function InserirObrasComplemento() {
    DB.obras_complemento.count()
    .then(registros => {
        if (registros < 1) {
            DB.obras_complemento.bulkAdd([
                {rotulo: 'Banco Capacitores', descricao: '', status: 1},
                {rotulo: '', descricao: '', status: 1},
                {rotulo: 'LT Foz-Guaíra 500kV', descricao: '', status: 1},
                {rotulo: '15kV', descricao: '', status: 1},
                {rotulo: '1ª Etapa', descricao: '', status: 1},
                {rotulo: '2ª Etapa', descricao: '', status: 1},
                {rotulo: '345kV', descricao: '', status: 1},
                {rotulo: '3ª Etapa', descricao: '', status: 1},
                {rotulo: '4ª Etapa', descricao: '', status: 1},
                {rotulo: '9R - Apoio a Fiscalização', descricao: '', status: 1},
                {rotulo: 'Ampliação', descricao: '', status: 1}   
            ]).then((e) => {
    
                console.log(`Obras/complementos iniciais carregados!`,e);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar obras/complementos inciais! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em obras/complementos!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de obras/complementos pré-cadastrados!`);
    });
}