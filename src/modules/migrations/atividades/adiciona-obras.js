import { DB } from "../init-indexeddb";

export function InserirObras() {
    DB.obras.count()
    .then(registros => {
        if (registros < 1) {
            DB.obras.bulkAdd([
                { rotulo: '8R', descricao: '', status: 1 },
                { rotulo: '10OP', descricao: '', status: 1 },
                { rotulo: '10R', descricao: '', status: 1 },
                { rotulo: '10R - Fiscalização', descricao: '', status: 1 },
                { rotulo: '11R', descricao: '', status: 1 },
                { rotulo: '12R', descricao: '', status: 1 },
                { rotulo: '13R', descricao: '', status: 1 },
                { rotulo: '14R', descricao: '', status: 1 },
                { rotulo: '15 RD', descricao: '', status: 1 },
                { rotulo: '15R', descricao: '', status: 1 }   
            ]).then((e) => {
    
                console.log(`Obras iniciais carregados!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar obras inciais! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em obras!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de obras pré-cadastrados!`);
    });
}