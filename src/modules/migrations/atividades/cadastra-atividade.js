import { DB } from "../init-indexeddb";
import { IncluiNovaObra } from "./inclui-nova-obra";
import { IncluiNovaObraComplemento } from "./inclui-nova-obra-complemento";
import { IncluiNovoLocal } from "./inclui-novo-local";
import { IncluiNovoDepResponsavel } from "../contratos/inclui-novo-dep-responsavel";
import { IncluiNovoGestor } from "./inclui-novo-gestor";
import { IncluiNovoCoordenador } from "./inclui-novo-coordenador";
import { Erros } from "../../erros/erros";

export function CadastraAtividade() {

    /**
     * Recebendo os valores dos campos
     */
    let 
        rotulo_da_atividade = document.getElementById("rotulo-da-atividade").value,
        local_da_atividade = document.getElementById("opcoes-locais-da-atividade").value,
        local_da_atividade_selecionada = document.getElementById("opcoes-locais-da-atividade-selecionado").value,
        obra_da_atividade = document.getElementById("opcoes-obras-da-atividade").value,
        obra_da_atividade_selecionada = document.getElementById("opcoes-obras-da-atividade-selecionado").value,
        obra_complemento_da_atividade = document.getElementById("opcoes-obras-complemento-da-atividade").value,
        obra_complemento_da_atividade_selecionada = document.getElementById("opcoes-obras-complemento-da-atividade-selecionado").value,
        classificacao_contabil_da_atividade = document.getElementById("classificacao-contabil-da-atividade").value,
        escopo_da_atividade = document.getElementById("escopo-da-atividade").value,
        inicio_previsto_da_atividade = document.getElementById("data-inicio-previsto-da-atividade").value,
        termino_previsto_da_atividade  = document.getElementById("data-termino-previsto-da-atividade").value,
        inicio_real_da_atividade = document.getElementById("data-inicio-real-da-atividade").value,
        termino_real_da_atividade  = document.getElementById("data-termino-real-da-atividade").value,
        meta_energizacao_da_atividade = document.getElementById("meta-energizacao-da-atividade").value,
        doc_autorizacao_da_atividade = document.getElementById("doc-autorizacao-da-atividade").value,
        informacoes_complementares_da_atividade = document.getElementById("informacoes-complementares-da-atividade").value,
        gestor_da_atividade = document.getElementById("opcoes-gestor-da-atividade").value,
        gestor_da_atividade_selecionado = document.getElementById("opcoes-gestor-da-atividade-selecionado").value,
        coordenador_da_atividade = document.getElementById("opcoes-coordenador-da-atividade").value,
        coordenador_da_atividade_selecionado = document.getElementById("opcoes-coordenador-da-atividade-selecionado").value,
        fiscalizacao_da_atividade = document.getElementById("opcoes-fiscalizacao-da-atividade").value,
        fiscalizacao_da_atividade_selecionada = document.getElementById("opcoes-fiscalizacao-da-atividade-selecionado").value
    ;


    let fieldLocalAtividade = document.getElementById("field-opcoes-locais-da-atividade");
    let aviso = fieldLocalAtividade.querySelector('.text-danger');
    
    let fieldObraAtividade = document.getElementById("field-opcoes-obras-da-atividade");
    let aviso2 = fieldObraAtividade.querySelector('.text-danger');

    let fieldObraComplementoAtividade = document.getElementById("field-opcoes-obras-complemento-da-atividade");
    let aviso3 = fieldObraComplementoAtividade.querySelector('.text-danger');

    let fieldEscopoAtividade = document.getElementById("field-escopo-da-atividade");
    let aviso4 = fieldEscopoAtividade.querySelector('.text-danger');

    if (
        ( local_da_atividade == '' || local_da_atividade_selecionada == '' ) || 
        escopo_da_atividade == '' || 
        ( obra_da_atividade == '' || obra_da_atividade_selecionada == '') ||
        ( obra_complemento_da_atividade == '' || obra_complemento_da_atividade_selecionada == '') 
    ) {

        if (
            (local_da_atividade == '' || local_da_atividade_selecionada== '') && 
            escopo_da_atividade == '' && 
            ( obra_da_atividade == '' || obra_da_atividade_selecionada == '') && 
            ( obra_complemento_da_atividade == '' || obra_complemento_da_atividade_selecionada == '') 
        ) {
            console.error(Erros.EE0001); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
            aviso3.classList.remove('d-none');
            aviso4.classList.remove('d-none');
        } 
        
        if ( local_da_atividade == '' || local_da_atividade_selecionada == '' ) {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if ( obra_da_atividade == '' || obra_da_atividade_selecionada == '') {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 
        
        if ( obra_complemento_da_atividade == '' || obra_complemento_da_atividade_selecionada == '') {
            aviso3.classList.remove('d-none');
        } else {
            aviso3.classList.add('d-none');
        }

        if (escopo_da_atividade == '') {
            aviso4.classList.remove('d-none');
        } else {
            aviso4.classList.add('d-none');
        }

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');
        aviso3.classList.add('d-none');
        aviso4.classList.add('d-none');

        let timestamp = Date.now();
        let criacao = new Date(timestamp);
        let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));

        /**
         * Falta fazer as inclusões de campos adicionados e logica de inclusão
         */

        IncluiNovoLocal({
            valor: local_da_atividade_selecionada
        }).then((retorno_local) => {

            let local_checado = retorno_local.id;

            IncluiNovaObra({
                valor: obra_da_atividade_selecionada
            })
            .then((retorno_obra) => {

                let obra_checada = retorno_obra.id;

                IncluiNovaObraComplemento({
                    valor: obra_complemento_da_atividade_selecionada
                }).then((retorno_obra_complemento) => {

                    let obra_complemento_checada = retorno_obra_complemento.id;

                    IncluiNovoGestor({
                        valor: gestor_da_atividade_selecionado
                    })
                    .then((retorno_gestor) => {

                        let gestor_checado = retorno_gestor.id;

                        IncluiNovoCoordenador({
                            valor: coordenador_da_atividade_selecionado
                        }).then((retorno_coordenador)=>{

                            let coordenador_checado = retorno_coordenador.id

                            IncluiNovoDepResponsavel({
                                valor: fiscalizacao_da_atividade_selecionada
                            })
                            .then((retorno_fiscalizacao) => {
        
                                let fiscalizacao_checado = retorno_fiscalizacao.id;
        
                                //let consolidado = retorn_numero_de_contrato_sap.consolidado && retorno_prestador.consolidado && retorno_resp.consolidado && retorno_dep_resp.consolidado ? true : false;
        
                                let dados_verificados_para_cadastrar = {
                                    rotulo: rotulo_da_atividade,
                                    local: local_checado,
                                    obra: obra_checada,
                                    obra_complemento: obra_complemento_checada,
                                    classificacao_contabil_pep: classificacao_contabil_da_atividade,
                                    escopo: escopo_da_atividade,
                                    data_inicio_previsto: inicio_previsto_da_atividade,
                                    data_termnino_previsto: termino_previsto_da_atividade,
                                    data_inicio_real: inicio_real_da_atividade,
                                    data_termnino_real: termino_real_da_atividade,
                                    energizacao: meta_energizacao_da_atividade,
                                    tipo_de_autorizacao: doc_autorizacao_da_atividade,
                                    informacoes_complementares: informacoes_complementares_da_atividade,
                                    gestor: gestor_checado,
                                    coordenador: coordenador_checado,
                                    responsavel_fiscalizacao: fiscalizacao_checado,
                                    autor: dados_autor.id,
                                    data_de_publicacao: criacao
                                };
                        
                                //console.log(`Para input:`,dados_verificados_para_cadastrar);
                                let link_atual = window.location.href.split('/');
                                let monta_url = '';
                                for (let contador = 0; contador <= (link_atual.length-2);contador++) {
                                    monta_url += link_atual[contador]+'/';
                                }
                        
                                DB.atividades.add(dados_verificados_para_cadastrar)
                                .then((resposta)=>{
                                    console.log(`Atividade cadastrada com êxito!`);
                                    alert(`Tudo certo! Atividade cadastrada!`);
                                    location.href=monta_url+'lista.html';
                                })
                                .catch((erro)=>{
                                    console.error(`${Erros.EE0003}: \n Erro: `,erro);
                                })
                            
                            }).catch(erro => console.error(erro))

                        }).catch(erro => console.error(erro))


                    }).catch(erro => console.error(erro))

                }).catch(erro => console.error(erro))

            }).catch(erro => console.error(erro))

        }).catch(erro => console.error(erro))

    }    

}