import { DB } from "../init-indexeddb";
import { CarregaUsuariosPorFuncao } from "../comum/carrega-usuarios-por-funcao";
import { SelectCustomizado } from "../comum/select-customizado";
import { CarregaLocais } from "./carrega-dados-de-locais";
import { CarregaObras } from "./carrega-dados-de-obras";
import { CarregaObrasComplemento } from "./carrega-dados-de-obras-complemento";
import { CarregaDepartamentos } from "../contratos/carrega-departamentos";
import { Erros } from "../../erros/erros";

export async function CarregaDadosDaAtividade(pagina_atual = '') {

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);
    
    if (grupo == 'atividades') {
    
        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
                                    
                async function RetornaDados(id) {
                    const retorno = await DB.atividades.get(Number(id))
                    return retorno
                }

                RetornaDados(identificador)
                .then(dados => {

                    CarregaLocais({
                        id_select: 'opcoes-locais-da-atividade',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-locais-da-atividade',
                            placeholder: 'Escolha ou digite um número aqui',
                            sem_resultados: 'Sem mais números!',
                            sem_resultados_na_pesquia: 'Nenhum local de atividade encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.local),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-locais-da-atividade-selecionado").value = Number(dados.local);
                    }).catch(e=>console.error('Erro: ',e))
            
                    CarregaObras({
                        id_select: 'opcoes-obras-da-atividade',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-obras-da-atividade',
                            placeholder: 'Escolha ou digite um número aqui',
                            sem_resultados: 'Sem mais números!',
                            sem_resultados_na_pesquia: 'Nenhuma obra encontrada!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.obra),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-obras-da-atividade-selecionado").value = Number(dados.obra);
                    }).catch(e=>console.error('Erro: ',e))
            
                    CarregaObrasComplemento({
                        id_select: 'opcoes-obras-complemento-da-atividade',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-obras-complemento-da-atividade',
                            placeholder: 'Escolha ou digite um número aqui',
                            sem_resultados: 'Sem mais números!',
                            sem_resultados_na_pesquia: 'Nenhuma obra/complemento encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.obra_complemento),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-obras-complemento-da-atividade-selecionado").value = Number(dados.obra_complemento);
                    }).catch(e=>console.error('Erro: ',e))


                    CarregaUsuariosPorFuncao({
                        id_select: 'opcoes-gestor-da-atividade',
                        placeholder: 'Escolha um gestor',
                        funcao: 2,
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-gestor-da-atividade',
                            placeholder: 'Escolha um gestor',
                            sem_resultados: 'Sem mais gestores!',
                            sem_resultados_na_pesquia: 'Nenhum gestor encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.gestor),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-gestor-da-atividade-selecionado").value = Number(dados.gestor);
                    }).catch(e=>console.error('Erro: ',e))
            
                    CarregaUsuariosPorFuncao({
                        id_select: 'opcoes-coordenador-da-atividade',
                        placeholder: 'Escolha um coordenador',
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-coordenador-da-atividade',
                            placeholder: 'Escolha um coordenador',
                            sem_resultados: 'Sem mais coordenadores!',
                            sem_resultados_na_pesquia: 'Nenhum coordenador encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.coordenador),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-coordenador-da-atividade-selecionado").value = Number(dados.coordenador);
                    }).catch(e=>console.error('Erro: ',e))
            
                    CarregaDepartamentos('opcoes-fiscalizacao-da-atividade')
                    .then(opcoes => {
                        SelectCustomizado({
                            id_do_select: 'opcoes-fiscalizacao-da-atividade',
                            placeholder: 'Escolha o dep. responsável',
                            sem_resultados: 'Sem mais dep. responsáveis!',
                            sem_resultados_na_pesquia: 'Nenhum departamento encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.responsavel_fiscalizacao),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-fiscalizacao-da-atividade-selecionado").value = Number(dados.responsavel_fiscalizacao);
                    }).catch(e=>console.error('Erro: ',e))

                    /**
                     * Campos para carregar os dados
                     */
                    document.getElementById("identificador").value = identificador;
                    document.getElementById("rotulo-da-atividade").value = dados.rotulo;
                    document.getElementById("classificacao-contabil-da-atividade").value = dados.classificacao_contabil_pep;
                    document.getElementById("escopo-da-atividade").value = dados.escopo;
                    document.getElementById("data-inicio-previsto-da-atividade").value = dados.data_inicio_previsto;
                    document.getElementById("data-termino-previsto-da-atividade").value = dados.data_termino_previsto;
                    document.getElementById("data-inicio-real-da-atividade").value = dados.data_inicio_real,
                    document.getElementById("data-termino-real-da-atividade").value = dados.data_termino_real;
                    document.getElementById("meta-energizacao-da-atividade").value = dados.energizacao;
                    document.getElementById("doc-autorizacao-da-atividade").value = dados.tipo_de_autorizacao;
                    document.getElementById("informacoes-complementares-da-atividade").value = dados.informacoes_complementares


                    console.log(`Dados da atividade ${identificador} - ${dados.rotulo} carregados com sucesso!`);
                })
                .catch(e => {
                    console.error(`${Erros.EL0003}\n Mensagem: `,e);
                });

    
            })
    
        }

    }

}