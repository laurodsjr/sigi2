import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaLocais(propriedades = {
    id_select,
    placeholder,
    pagina
}) {

    const {
        id_select = '',
        placeholder = null,
        pagina = ''
    } = propriedades

    if (document.getElementById(id_select) !== null) {

        return new Promise(function(resolve, reject) {

            // Checa se existem departamentos cadastrados
            DB.locais.count()
            .then(registros => {
                if (registros > 0) {
                    const departamentos = DB.locais.toArray()
                    .then( (opcao) => { 

                        let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha um local`}</option>`;
                        opcao.forEach((item) => {
                            exibir_lista += `<option value="${item.id}">${item.rotulo}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                        });

                        // Atualizar o elemento HTML com a lista de origens
                        const listaOrigens = document.getElementById(id_select);
                        listaOrigens.innerHTML = exibir_lista;
                        console.log(`Lista de opções de prestadores de serviços carregada!`);
                        resolve();
                    })
                    .catch(erro=>reject(`${Erros.EQ0006} \n Erro: ${erro}`));
                } else {
                    let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha um local`}</option>`;
                    const listaOrigens = document.getElementById(id_select);
                    if (pagina == 'lista.html') {
                        document.getElementById(`area-${id_select}`).classList.add('d-none');
                    }
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Não há opções de locais para listar!`);
                    resolve();
                }
            })
            .catch(erro=>reject(`${Erros.EQ0005} \n Erro: ${erro}`));

        })

    }

}