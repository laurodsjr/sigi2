import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaObrasComplemento(propriedades = {
    id_select,
    placeholder,
    pagina
}) {

    const {
        id_select = '',
        placeholder = null,
        pagina = ''
    } = propriedades

    if (document.getElementById(id_select) !== null) {

        return new Promise(function(resolve, reject) {

            // Checa se existem departamentos cadastrados
            DB.obras_complemento.count()
            .then(registros => {
                if (registros > 0) {
                    const departamentos = DB.obras_complemento.toArray()
                    .then( (opcao) => { 

                        let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha uma obra/complemento`}</option>`;
                        opcao.forEach((item) => {
                            exibir_lista += `<option value="${item.id}">${item.rotulo}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                        });

                        // Atualizar o elemento HTML com a lista de origens
                        const listaOrigens = document.getElementById(id_select);
                        listaOrigens.innerHTML = exibir_lista;
                        console.log(`Lista de opções de obra/complemento carregada!`);
                        resolve();
                    })
                    .catch(erro=>reject(`${Erros.EQ0010}! \n Erro: ${erro}`));
                } else {
                    let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha uma obra/complemento`}</option>`;
                    const listaOrigens = document.getElementById(id_select);
                    if (pagina == 'lista.html') {
                        document.getElementById(`area-${id_select}`).classList.add('d-none');
                    }
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Não há opções de obra/complemento para listar!`);
                    resolve();
                }
            })
            .catch(erro=>reject(`${Erros.EQ0009}! \n Erro: ${erro}`));

        })

    }

}