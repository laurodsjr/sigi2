import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";
import { ListagemAtividades } from "./listagem";

export function ExcluirAtividade() {

    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('listagem-de-atividades') !== null) {
            let listagem = document.getElementById('listagem-de-atividades');
            listagem.addEventListener('click', function(event) {
                if (event.target.matches('#excluir')) {
                    let id = event.target.getAttribute('identificador');
                    console.log('Solicitação de exclusão do identificador de atividade: ',id);

                    let confirmacao = confirm('Você tem certeza que deseja excluir esta atividade?');
                    if (confirmacao) {
                        DB.atividades.delete(Number(id))
                        .then(resposta => { 
                            alert(`Atividade ${id} excluida com sucesso!`);
                            console.log(`Atividade ${id} excluida com sucesso!`);
                            ListagemAtividades()
                        })
                        .catch(e => {
                            console.error(`${Erros.ED0001}\n Erro: `,e);
                        })
                    } else {
                        console.log('Operação cancelada pelo usuário!');
                    }
                    
                }
            })
        }
    });

}