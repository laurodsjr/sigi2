import { MontaLink } from "../../customizacao/monta-link";
import { TextoResumido } from "../../customizacao/texto-resumido";
import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";  

export function FiltrarListagemAtividades() {

    const
        local_da_atividade = document.getElementById('opcoes-locais-da-atividade-selecionado')?.value
    ;

    DB.locais.toArray()
    .then(local => {

        function Local(id) {
            if (local.length <= 0 || String(Number(id)) === "NaN") { return `${id} [Inconsistente]`; } 
            else {
                let locais_filtrados = local.filter(item => {
                    if (item.id == Number(id)) {
                        return item?.numero || item?.id
                    }
                });
                return {
                    status: Number(locais_filtrados[0]?.status) === 0 ? false : true,
                    resposta: `${locais_filtrados[0]?.rotulo}${Number(locais_filtrados[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                };
            }
        }

        DB.obras.toArray()
        .then(obra => {

            function Obra(id) {
                if (obra.length <= 0 || String(Number(id)) === "NaN") { return `${id} [Inconsistente]`; } 
                else {
                    let obras_filtradas = obra.filter(item => {
                        if (item.id == Number(id)) {
                            return item?.numero || item?.id
                        }
                    });
                    return {
                        status: Number(obras_filtradas[0]?.status) === 0 ? false : true,
                        resposta: `${obras_filtradas[0]?.rotulo}${Number(obras_filtradas[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                    };
                }
            }

            DB.obras_complemento.toArray()
            .then(obras_comp => {

                function ObrasComplento(id) {

                    if (obras_comp.length <= 0) { return id; } 
                    else {
                        let obras_comp_filtrado = obras_comp.filter(item => {
                            if (item.id == Number(id)) {
                                return item?.nome || item?.id
                            }
                        });
                        //console.log(prestador_filtrado);
                        return {
                            status: Number(obras_comp_filtrado[0]?.status) === 0 ? false : true,
                            resposta: `${obras_comp_filtrado[0]?.rotulo}${Number(obras_comp_filtrado[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                        };
                    }
                }
                        
                function FiltrarAtividades(parametros = {}) {
                    const {
                      localAtividade = ''
                    } = parametros;
                  
                    return new Promise(function(resolve, reject) {
                      let consulta = DB.atividades;
                  
                      if (localAtividade !== '') {
                        consulta = consulta.filter(registro => registro.local === localAtividade);
                      }
                      /*
                      if (rotuloDoContrato !== '') {
                        consulta = consulta.filter(registro => registro.rotulo === rotuloDoContrato);
                      }
                      */
                  
                      consulta.toArray()
                        .then(atividades => {
                          resolve({
                            registros: atividades.length,
                            dados: atividades
                          });
                        })
                        .catch(erro => {
                          console.error(`Houve um erro ao tentar filtrar as atividades! Erro: ${erro}`);
                          resolve({
                            registros: 0,
                            dados: []
                          });
                        });
                    });
                }                          

                
                FiltrarAtividades({
                    localAtividade: local_da_atividade
                })
                .then(registros => {
                    if (registros.registros > 0) {
                        FiltrarAtividades({
                            localAtividade: local_da_atividade
                        })
                        .then(({dados}) => {      
                            
                            const lista = dados;
                                
                            let exibir_tabela = `<table id="table-list" class="w-100">
                                <thead id="table-title">
                                    <tr>
                                        <th class="p-2">ID</th>
                                        <th class="p-2">RÓTULO</th>
                                        <th class="p-2">LOCAL</th>
                                        <th class="p-2">OBRA</th>
                                        <th class="p-2">OBRA/COMPLEMENTO</th>
                                        <th class="p-2 descricoes">ESCOPO</th>
                                        
                                        <th class="p-2 last">AÇÕES</th>
                                    </tr>
                                </thead>
                            <tbody>`;

                            lista.forEach((item, key) => {

                                //let consolidado = ( ContratosSAP(item.numero_do_contrato).status && Empresas(Number(item.empresa)).status && Responsavel(item.responsavel).status && DepResponsavel(Number(item.dep_responsavel)).status ) || item.consolidado ? '' : ` <span class="badge text-bg-warning">Não consolidado</span>`;

                                exibir_tabela += `<tr class="${key % 2 !== 0 ? 'even-line' : ''}">
                                    <td class="${'p-2'} text-center">${item.id}</td>
                                    <td class="${'p-2'}">${item.rotulo ? item.rotulo : `Sem rótulo`}</td>
                                    <td class="${'p-2'}">${Local(item.local).resposta}</td>
                                    <td class="${'p-2'}">${Obra(item.obra).resposta}</td>
                                    <td class="${'p-2'}">${ObrasComplento(Number(item.obra_complemento)).resposta}</td>
                                    <td class="${'p-2'} descricoes">${TextoResumido({texto: item.escopo, limite_de_letras: 128})}</td>
                                    <td class="${'p-2 actions'}">
                                        <a href="${MontaLink(`atividades/visualizar.html?id=${item.id}`)}">
                                            <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="atividades" identificador="${item.id}"></i>
                                        </a>
                                        <a href="${MontaLink(`atividades/editar.html?id=${item.id}`)}">
                                            <i class="fa-solid fa-pen-to-square" id="editar" tipo="atividades" identificador="${item.id}"></i>
                                        </a>
                                        <i class="fa-solid fa-trash-can" id="excluir" tipo="atividades" identificador="${item.id}"></i>
                                    </td>
                                </tr>`;
                            });

                            exibir_tabela += `</tdoby></table>`;

                            // Atualizar o elemento HTML com a lista de origens
                            if (document.getElementById("listagem-de-atividades") !== null) {
                                const listaOrigens = document.getElementById("listagem-de-atividades");
                                listaOrigens.innerHTML = exibir_tabela;
                                console.log(`Lista de atividades carregada!`);
                            }
                            
                        }).catch(e => {
                
                            console.error(`${Erros.EL0002}\n Mensagem: ${e}`);
                    
                        })

                    } else {
                        
                        if (document.getElementById("listagem-de-atividades") !== null) {
                            const listaOrigens = document.getElementById("listagem-de-atividades");
                            listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há atividades cadastrados!</p>';
                            console.log(`Não há atividades cadastrados!`);
                        }

                    }

                }).catch(e => console.error(`${Erros.EQ0016} Erro: `,e));

            }).catch(e => console.error(`${Erros.EQ0015} Erro: `,e));

        }).catch(e => console.error(`${Erros.EQ0014} Erro: `,e));

    }).catch(e => console.error(`${Erros.EQ0013} Erro: `,e));
}