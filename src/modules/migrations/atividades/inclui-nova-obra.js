import { DB } from "../init-indexeddb";

export function IncluiNovaObra(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        //console.log('Testa > ',Number(valor));
    
        if (Number(valor).toString() == 'NaN') {
            DB.obras
            .add({
                numero: valor,
                status: Number(0)
            })
            .then( (primaryKey) => { 
                console.log(`Nova obra cadastrada para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir a nova obra! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.obras.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Esta obra já foi revisada e aprovada!` : `Esta obra já foi cadastrada e será revisada!` );
                resolve({
                    id: valor,
                    consolidado: dados.status ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem da obra! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}