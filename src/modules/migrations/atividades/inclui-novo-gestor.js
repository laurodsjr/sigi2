import { DB } from "../init-indexeddb";

export function IncluiNovoGestor(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {
    
        if (Number(valor).toString() == 'NaN' && valor != '' && valor != undefined) {

            DB.usuarios
            .add({
                nome: valor,
                funcao: Number(2),
                status: 0
            })
            .then( (primaryKey) => { 
                console.log(`Novo gestor incluído para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir o gestor! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.usuarios.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Este gestor já foi revisado e aprovado!` : `Este gestor já foi cadastrado e será revisado!` );
                resolve({
                    id: valor,
                    consolidado: dados.funcao != 5 ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem do gestor! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}