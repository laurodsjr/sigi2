import { DB } from "../init-indexeddb";

export function IncluiNovoLocal(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        //console.log('Testa > ',Number(valor));
    
        if (Number(valor).toString() == 'NaN') {
            DB.locais
            .add({
                numero: valor,
                status: Number(0)
            })
            .then( (primaryKey) => { 
                console.log(`Novo local cadastrado para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir o novo local! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.locais.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Este local já foi revisado e aprovado!` : `Este local já foi cadastrado e será revisado!` );
                resolve({
                    id: valor,
                    consolidado: dados.status ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem do local! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}