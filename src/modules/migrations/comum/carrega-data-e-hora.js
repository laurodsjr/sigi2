export function DataEHora() {

    if (document.getElementById("data-hora") !== null) {

        function exibirDataHora() {
            var dataHoraElement = document.getElementById("data-hora");
            var dataAtual = new Date();
      
            var diaSemana = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];
            var mesNome = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
      
            var diaSemanaAtual = diaSemana[dataAtual.getDay()];
            var dia = adicionarZero(dataAtual.getDate());
            var mesAtual = mesNome[dataAtual.getMonth()];
            var ano = dataAtual.getFullYear();
      
            var horas = adicionarZero(dataAtual.getHours());
            var minutos = adicionarZero(dataAtual.getMinutes());
            var segundos = adicionarZero(dataAtual.getSeconds());
      
            var dataHoraFormatada = diaSemanaAtual + ', ' + dia + ' de ' + mesAtual + ' de ' + ano + ' às ' + horas + ':' + minutos + ':' + segundos;
      
            dataHoraElement.innerHTML = dataHoraFormatada;
        }
    
        // Função auxiliar para adicionar um zero à esquerda de números menores que 10
        function adicionarZero(numero) {
            return numero < 10 ? '0' + numero : numero;
        }
    
        // Chama a função inicialmente para exibir a data e hora atual
        exibirDataHora();
    
        // Atualiza a data e hora a cada segundo
        setInterval(exibirDataHora, 1000);

    }

}