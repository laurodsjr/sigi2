import { DB } from "../init-indexeddb";
import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros.js";

export function CarregaSolicitacoes() {

    if (document.getElementById("lista-solicitacoes") !== null) {

        // Checa se existem solicitações
        DB.solicitacao.count()
        .then(registros => {
            if (registros > 0) {
                DB.solicitacao.toArray()
                .then((opcao) => { 

                    //console.log('Solicitações: ',opcao)

                    let exibir_lista = `<option value="0">Escolha uma solicitação</option>`;
                    opcao.forEach((item) => {
                        exibir_lista += `<option value="${item.id}">#${ AcrescentadorDeZeros(item.id,4)}</option>`;
                    });

                    // Atualizar o elemento HTML com a lista de origens
                    const listaOrigens = document.getElementById("lista-solicitacoes");
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de opções de solicitação carregada!`);
                })
                .catch(erro=>console.error(`Houve um erro ao tentar listar as opções de solicitações disposníveis! \n Erro: ${erro}`));
            } else {
                console.log(`Não há opções de solicitação para listar!`);
            }
        })
        .catch(erro=>console.error(`Houve um erro ao tentar contar as opções de solicitações disposníveis! \n Erro: ${erro}`));

    }

}