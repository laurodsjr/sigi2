import { DB } from "../init-indexeddb";
import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros.js";

export function CarregaUsuariosPorFuncao(propriedades = {
    funcao: null,
    id_select: '',
    placeholder: null
}) {

    const { 
        funcao = null,
        id_select = '',
        placeholder = null
    } = propriedades

    if (document.getElementById(id_select) !== null) {

        return new Promise(function(resolve, reject) {

            // Checa se existem usuários
            if (funcao === null) {
                DB.usuarios
                .count()
                .then(registros => {
                    if (registros > 0) {
                        DB.usuarios
                        .toArray()
                        .then((opcao) => { 

                            //console.log('Solicitações: ',opcao)

                            let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha um usuário`}</option>`;
                            opcao.forEach((item) => {
                                exibir_lista += `<option value="${item.id}">${item.nome}${item.funcao == 5 ? ' <b>[Em análise]</b>' : ''}</option>`;
                            });

                            // Atualizar o elemento HTML com a lista de origens
                            if (document.getElementById("lista-usuarios") !== null) {
                                const listaOrigens = document.getElementById("lista-usuarios");
                                listaOrigens.innerHTML = exibir_lista;
                                console.log(`Lista de opções de usuários carregada!`);
                            }

                            if (document.getElementById(id_select) !== null) {
                                const listaOrigens = document.getElementById(id_select);
                                listaOrigens.innerHTML = exibir_lista;
                                console.log(`Lista de opções de usuários carregada!`);
                            }
                            resolve();                    
                        })
                        .catch(erro=>reject(`Houve um erro ao tentar listar as opções de usuários disposníveis! \n Erro: ${erro}`));
                    } else {
                        reject(`Não há opções de usuários para listar!`);
                    }
                })
                .catch(erro=>reject(`Houve um erro ao tentar contar as opções de usuários disposníveis! \n Erro: ${erro}`));
            } else {
                DB.usuarios
                .where('funcao')
                .equals(Number(funcao))
                .count()
                .then(registros => {
                    if (registros > 0) {
                        DB.usuarios
                        .where('funcao')
                        .equals(Number(funcao))
                        .toArray()
                        .then((opcao) => { 

                            //console.log('Solicitações: ',opcao)

                            let exibir_lista = Number(funcao) === 1 ? 
                                `<option value="">${placeholder !== null ? placeholder : `Escolha um administrador`}</option>` : 
                                    Number(funcao) === 2 ? 
                                        `<option value="">${placeholder !== null ? placeholder : `Escolha um gestor`}</option>` : 
                                            Number(funcao) === 3 ? 
                                            `<option value="">${placeholder !== null ? placeholder : `Escolha um engenheiro`}</option>` :
                                                `<option value="">${placeholder !== null ? placeholder : `Escolha um gerente`}</option>`;
                            opcao.forEach((item) => {
                                exibir_lista += `<option value="${item.id}">${item.nome}${item.funcao == 5 ? ' <b>[Em análise]</b>' : ''}</option>`;
                            });

                            // Atualizar o elemento HTML com a lista de origens
                            const listaOrigens = document.getElementById(
                                    Number(funcao) === 1 ? 
                                        "lista-administradores" : 
                                            Number(funcao) === 2 ? 
                                                "lista-gestor" : 
                                                    Number(funcao) === 3 ? 
                                                        "lista-engenheiros" : 
                                                            "lista-gerentes"
                            );

                            if (listaOrigens !== null) {
                                listaOrigens.innerHTML = exibir_lista;
                                console.log(`Lista de opções de  ${
                                    Number(funcao) === 1 ? 
                                        'adminitradores ' :
                                    Number(funcao) === 2 ? 
                                        'gestores ' :
                                    Number(funcao) === 3 ? 
                                        'engenheiros ' :
                                        'gerentes ' 
                                } carregada!`);
                            }

                            if (document.getElementById(id_select) !== null) {
                                const listaOrigens = document.getElementById(id_select);
                                listaOrigens.innerHTML = exibir_lista;
                                console.log(`Lista de opções de usuários carregada!`);
                            }

                            resolve();
                        })
                        .catch(erro=>reject(
                            `Houve um erro ao tentar listar as opções de ${
                                Number(funcao) === 1 ? 
                                    'adminitradores ' :
                                Number(funcao) === 2 ? 
                                    'gestores ' :
                                Number(funcao) === 3 ? 
                                    'engenheiros ' :
                                    'gerentes ' 
                            }disposníveis! \n Erro: ${erro}`)
                        );
                    } else {
                        reject(
                            `Não há opções de  ${
                                Number(funcao) === 1 ? 
                                    'adminitradores ' :
                                Number(funcao) === 2 ? 
                                    'gestores ' :
                                Number(funcao) === 3 ? 
                                    'engenheiros ' :
                                    'gerentes ' 
                            } para listar!`
                        );
                    }
                })
                .catch(erro=>reject(
                    `Houve um erro ao tentar contar as opções de ${
                        Number(funcao) === 1 ? 
                            'adminitradores ' :
                        Number(funcao) === 2 ? 
                            'gestores ' :
                        Number(funcao) === 3 ? 
                            'engenheiros ' :
                            'gerentes ' 
                    } disposníveis! \n Erro: ${erro}`)
                );
            }

        })

    }

}