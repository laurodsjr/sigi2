export function SelectCustomizado(propridades = {
    id_do_select: '',
    placeholder: '',
    sugestao_de_adicao: '',
    sem_resultados: '',
    sem_resultados_na_pesquia: '',
    lista_de_opcoes: [],
    adicionavel: false,
    customizar: true,
    pre_selecionado,
    pagina
}) {

    const { 
        id_do_select, 
        placeholder, 
        sem_resultados = '', 
        sem_resultados_na_pesquia = 'Nenhuma empresa encontrada!',
        sugestao_de_adicao = 'Clique para adicionar',
        lista_de_opcoes = [],
        adicionavel = false,
        customizar = true,
        pre_selecionado = null,
        pagina = ''
    } = propridades

    const opcoes = document.getElementById(id_do_select) !== null ? document.getElementById(id_do_select) : '';
    const opcoes_por_elemento = opcoes != '' ? opcoes.options : '';
    //const opcoes_por_elemento = opcoes.options;

    const total_por_select = opcoes_por_elemento.length - 1;

    let array_de_opcoes = []
    if (opcoes_por_elemento.length > 0) {
        for (let i = 0; i < opcoes_por_elemento.length; i++) {
            const opcao = opcoes_por_elemento[i];
            array_de_opcoes.push({
                value: opcao.value,
                label: opcao.label
            })
        }
    }

    if (opcoes !== '' && opcoes !== null && customizar === true) {

        if (opcoes.getAttribute('multiple') !== null) {

            const textoPlaceholder = placeholder;
            const select = new Choices(`#${id_do_select}`, {
                itemSelectText: sugestao_de_adicao,
                noChoicesText: sem_resultados,
                noResultsText: sem_resultados_na_pesquia,
                addItems: true,
                removeItemButton: true,
                shouldSort: false,
                classNames: {
                    containerInner: 'form-control bg-transparent',
                    input: 'bg-white',
                    inputCloned: 'border border-0 input-outline-none campo-select',
                }
            });
            
            select.setChoices(
                lista_de_opcoes
            );

            let opcoes_div = document.getElementById(`field-${id_do_select}`);
            let inputCampoSelect = opcoes_div.querySelector('.campo-select');

            if (pre_selecionado !== null) {
                if (total_por_select == pre_selecionado.length) {
                    inputCampoSelect.classList.add('hide-info');
                }
            }
            

            opcoes.addEventListener('change', () =>{
                let valorSelecionado = select.getValue(true);
                let opcao_selecionada = document.getElementById(`${id_do_select}-selecionado`);
                let field = document.getElementById(`field-${id_do_select}`);
                let aviso = field.querySelector('.text-danger');

                if (valorSelecionado.length >= 1) {

                    //console.log('Teste: ',opcao_selecionada)

                    if (total_por_select == valorSelecionado.length) {
                        inputCampoSelect.classList.add('hide-info');
                    }
                    opcao_selecionada.value = valorSelecionado; 
                    aviso.classList.add('d-none');
                } else {
                    opcao_selecionada.value = '';
                    inputCampoSelect.classList.remove('hide-info');
                }

                if (valorSelecionado.length < total_por_select) {
                    inputCampoSelect.classList.remove('hide-info');
                }

            })

            if (pre_selecionado !== null) {
                select.setChoiceByValue(pre_selecionado);

                if (pagina == 'visualizar.html') {
                    inputCampoSelect.classList.add('hide-info');
                }
            }   

            if (pagina === 'visualizar.html') {
                select.disable();
            }
    
        } else {
            
            const select = new Choices(`#${id_do_select}`, {
                silent: true,
                itemSelectText: sugestao_de_adicao,
                noChoicesText: sem_resultados,
                noResultsText: sem_resultados_na_pesquia,
                addItems: true,
                removeItemButton: true,
                shouldSort: false,
                classNames: {
                    containerInner: 'form-control bg-transparent',
                    input: 'bg-white',
                    inputCloned: 'border border-0 input-outline-none campo-select',
                }
            });

            if (adicionavel) {
                select.setChoices(
                    [
                        ...lista_de_opcoes,
                    ]
                );
            } else {
                select.setChoices(
                    lista_de_opcoes
                );
            }         

            let area = document.getElementById(`field-${id_do_select}`);
            let outra = document.getElementById(`outra-${id_do_select}`);
            let opcao_selecionada = document.getElementById(`${id_do_select}-selecionado`);
            let input = area.querySelector('input');
            let opcoes_iniciais = array_de_opcoes;

            let novas_opcoes = []

            input.addEventListener('input', function(event) {
                const textoOriginalDigitado = event.target.value;
                const textoDigitado = event.target.value;

                outra.value = textoOriginalDigitado

                let procuraValor = opcoes_iniciais.find(opcao => opcao.label.includes(textoDigitado));
                
                if (procuraValor === undefined) {
                    if (novas_opcoes.length <= 0) {
                        novas_opcoes.push({
                                value: textoDigitado,
                                label: textoOriginalDigitado
                        })
                    }
                    if (novas_opcoes.length == 1) {
                        select.clearChoices();
                        novas_opcoes[0] = {
                            value: textoDigitado,
                            label: textoOriginalDigitado
                        }
                        select.setValue([
                            {
                                value: textoDigitado,
                                label: `Adicionar <b>${textoOriginalDigitado}</b>`
                            }
                        ]);
                        opcao_selecionada.value = textoOriginalDigitado  
                    }
                
                }
                        
            });


            opcoes.addEventListener('change', () =>{
                let valorSelecionado = select.getValue(true);
                let opcao_selecionada = document.getElementById(`${id_do_select}-selecionado`);
                let outra_opcao = document.getElementById(`outra-${id_do_select}`);
                const field = document.getElementById(`field-${id_do_select}`); 
                const aviso = field.querySelector('.text-danger');

                //console.log('Respostas 1: ',valorSelecionado,'\nRespostas 2: ',opcao_selecionada.value, '\nRespostas 3: ',outra_opcao.value)

                if (valorSelecionado == undefined && opcao_selecionada.value != '' && outra_opcao.value == '') {
                    document.getElementById(`${id_do_select}`).innerHTML = `<option value="">${placeholder}</option>`;
                    select.setChoiceByValue('');
                    outra_opcao.value = '';
                    opcao_selecionada.value = ''; 
                } else if (valorSelecionado == undefined 
                    && ( typeof opcao_selecionada.value === 'string' || opcao_selecionada.value != '') 
                    && ( typeof outra_opcao.value === 'string' || outra_opcao.value != '')  
                ) {
                    //select.removeItem(outra_opcao.value);
                    outra_opcao.value = '';
                    opcao_selecionada.value = ''; 
                } else {
                    if (Number(outra_opcao.value) == 0) {    
                        
                        

                        select.clearChoices();
                        select.setValue([
                            ...array_de_opcoes,
                            ...[
                                {
                                    value: outra_opcao.value,
                                    label: outra_opcao.value
                                }
                            ],
                        ])            
                        select.setChoiceByValue(valorSelecionado);
                        outra_opcao = valorSelecionado;
                        opcao_selecionada.value = valorSelecionado;  
                        aviso.classList.add('d-none');        
                    } else {
                        select.clearChoices();
                        select.setValue([
                            ...array_de_opcoes,
                            ...[
                                {
                                    value: outra_opcao.value,
                                    label: valorSelecionado
                                }
                            ]
                        ]) 
                        //console.log('OP: ',outra_opcao.value, valorSelecionado);
                        opcao_selecionada.value = valorSelecionado
                        document.getElementById(`${id_do_select}-selecionado`).value = valorSelecionado
                        aviso.classList.add('d-none');
                    }
                } 

            })

            if (pre_selecionado !== null) {
                select.setChoiceByValue(String(Number(pre_selecionado)));
                //select.setChoiceByValue(pre_selecionado);
            }   

            if (pagina === 'visualizar.html') {
                select.disable();
            }
    
        }    

    }

    if (opcoes !== '' && opcoes !== null && customizar === false) {

        console.log('ID: ',id_do_select)
        const field = document.getElementById(`field-${id_do_select}`); 
        const aviso = field.querySelector('.text-danger');
        opcoes.addEventListener('change', (evento) =>{
            //console.log('Teste: ',evento.target.value)
            document.getElementById(`${id_do_select}-selecionado`).value = evento.target.value
            aviso.classList.add('d-none');
        })

        if (pre_selecionado !== null) {
            document.getElementById(`${id_do_select}`).value = pre_selecionado
        }
    }
    
}

// Função para capturar alterações nas opções
export function observarAlteracoesNoSelect(propridades = {
    id_do_select: '',
    placeholder: '',
    sugestao_de_adicao: '',
    sem_resultados: '',
    sem_resultados_na_pesquia: '',
    lista_de_opcoes: [],
    adicionavel: false,
    customizar: true,
    pre_selecionado,
    pagina
}) {

    const { 
        id_do_select, 
        placeholder, 
        sem_resultados = '', 
        sem_resultados_na_pesquia = 'Nenhuma empresa encontrada!',
        sugestao_de_adicao = 'Clique para adicionar',
        lista_de_opcoes = [],
        adicionavel = false,
        customizar = true,
        pre_selecionado = null,
        pagina = ''
    } = propridades

    const selectElement = document.getElementById(id_do_select);

    if (selectElement instanceof Node) {

        const observer = new MutationObserver(() => {
            // Quando o MutationObserver for ativado, chame a função SelectCustomizado novamente
            // com as mesmas propriedades para reconstruir o componente customizado.
            SelectCustomizado({
                id_do_select, 
                placeholder, 
                sem_resultados, 
                sem_resultados_na_pesquia,
                sugestao_de_adicao,
                lista_de_opcoes,
                adicionavel,
                customizar,
                pre_selecionado,
                pagina
            });

        });

        // Configurando o MutationObserver para observar alterações no elemento select
        const observerConfig = { childList: true, subtree: true };
        observer.observe(selectElement, observerConfig);

    } else {
        console.error('Elemento select não encontrado ou inválido.',document.getElementById(id_do_select),id_do_select);
    }
}