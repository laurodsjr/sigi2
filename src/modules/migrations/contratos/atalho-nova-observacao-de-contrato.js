export function AtalhoNovaObservacaoDoContrato() {
    if(document.getElementById('nova-observacao-contrato') !== null) {
        let link_atual = window.location.href.split('/');
        let monta_url = '';
        for (let contador = 0; contador <= (link_atual.length-3);contador++) {
            monta_url += link_atual[contador]+'/';
        }

        // Obtém o botão pelo ID
        let meuBotao = document.getElementById('nova-observacao-contrato');

        // Adiciona um evento de clique ao botão
        meuBotao.addEventListener('click', function() {
            // URL da página de destino
            let urlDestino = monta_url+'observacoes/incluir.html?tipo=contrato&id=';

            // Obtém o valor do atributo 'identificador'
            let identificador = meuBotao.getAttribute('identificador');

            // Redireciona para a página de destino com o valor do identificador
            location.href = urlDestino + identificador;
        });
    }
}