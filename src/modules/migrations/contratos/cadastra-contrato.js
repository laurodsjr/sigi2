import { DB } from "../init-indexeddb";
import { IncluiNovoDepResponsavel } from "./inclui-novo-dep-responsavel";
import { IncluiNovoNumeroDeContral } from "./inclui-novo-numero-de-contrato";
import { IncluiNovoPrestador } from "./inclui-novo-prestador";
import { IncluiNovoResponsavel } from "./inclui-novo-responsavel";
import { CadastraAtividadesDoContrato } from "../atividades-dos-contratos/cadastra-atividades-do-contrato";
import { Erros } from "../../erros/erros";

export function CadastraContrato() {

    /**
     * Recebendo os valores dos campos
     */
    let 
        numero_do_contrato = document.getElementById("opcoes-numero-do-contrato").value,
        numero_do_contrato_selecionado = document.getElementById("opcoes-numero-do-contrato-selecionado").value,
        rotulo_do_contrato = document.getElementById("rotulo-do-contrato").value,
        escopo_do_contrato = document.getElementById("escopo-do-contrato").value,
        empresa_contratatada = document.getElementById("opcoes-de-empresa-do-contrato").value,
        empresa_selecionada = document.getElementById("opcoes-de-empresa-do-contrato-selecionado").value,
        responsavel = document.getElementById("opcoes-para-responsavel").value,
        responsavel_selecionado = document.getElementById("opcoes-para-responsavel-selecionado").value,
        dep_responsavel = document.getElementById("opcoes-dep-responsavel").value,
        dep_responsavel_selecionado = document.getElementById("opcoes-dep-responsavel-selecionado").value,
        atividades = document.getElementById("opcoes-atividades-do-contrato-selecionado").value,
        atestado_ex_de_serv = document.getElementById("atestado-ex-de-serv-contrato").value,
        art_ct = document.getElementById("art-ct-contrato").value,
        ordem_de_servico = document.getElementById("ordem-de-servico-contrato").value,
        recebimento_provisorio = document.getElementById("recebimento-provisorio-contrato").value,
        data_rec_provisorio = document.getElementById("data-rec-provisorio-contrato").value,
        recebimento_definitivo = document.getElementById("recebimento-definitivo-contrato").value,
        data_rec_definitivo = document.getElementById("data-rec-definitivo-contrato").value,
        data_inicio_execucao = document.getElementById("data-incicio-execucao-contrato").value,
        data_termino_execucao = document.getElementById("data-termino-execucao-contrato").value,
        data_de_vigencia = document.getElementById("data-de-vigencia-contrato").value,
        data_base = document.getElementById("data-base-contrato").value,
        data_assinatura = document.getElementById("data-assinatura-contrato").value
    ;

    let fieldNumeroDoContrato = document.getElementById("field-opcoes-numero-do-contrato");
    let aviso = fieldNumeroDoContrato.querySelector('.text-danger');
    
    let fieldEscopoDoContrato = document.getElementById("field-escopo-do-contrato");
    let aviso2 = fieldEscopoDoContrato.querySelector('.text-danger');

    let fieldEmpresaContratada = document.getElementById("field-opcoes-de-empresa-do-contrato");
    let aviso3 = fieldEmpresaContratada.querySelector('.text-danger');

    let fieldResponsavel = document.getElementById("field-opcoes-para-responsavel");
    let aviso4 = fieldResponsavel.querySelector('.text-danger');

    let fieldDepResponsavel = document.getElementById("field-opcoes-dep-responsavel");
    let aviso5 = fieldDepResponsavel.querySelector('.text-danger');

    let fieldAtividades = document.getElementById("field-opcoes-atividades-do-contrato");
    let aviso6 = fieldAtividades.querySelector('.text-danger');


    if (
        (numero_do_contrato == '' || numero_do_contrato_selecionado == '') || 
        escopo_do_contrato == '' || 
        ( empresa_contratatada == '' || empresa_selecionada == '' ) || 
        ( responsavel == '' || responsavel_selecionado == '') ||
        ( dep_responsavel == '' || dep_responsavel_selecionado == '') ||
        ( atividades == '' || atividades.split(',')[0] == '' )
    ) {

        if (
            (numero_do_contrato == '' || numero_do_contrato_selecionado == '') && 
            escopo_do_contrato == '' && 
            ( empresa_contratatada == '' || empresa_selecionada == '' ) && 
            ( responsavel == '' || responsavel_selecionado == '' ) && 
            ( dep_responsavel == '' || dep_responsavel == '' ) &&
            ( atividades == '' || atividades.split(',')[0] == '' )
        ) {
            console.error(Erros.EE0004); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
            aviso3.classList.remove('d-none');
            aviso4.classList.remove('d-none');
            aviso5.classList.remove('d-none');
            aviso6.classList.remove('d-none');
        } 
        
        if (numero_do_contrato == '' || numero_do_contrato_selecionado == '') {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if (escopo_do_contrato == '') {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 
        
        if (empresa_contratatada == '' || empresa_selecionada == '') {
            aviso3.classList.remove('d-none');
        } else {
            aviso3.classList.add('d-none');
        }

        if (responsavel == '' || responsavel_selecionado == '') {
            aviso4.classList.remove('d-none');
        } else {
            aviso4.classList.add('d-none');
        }

        if (dep_responsavel == '' || dep_responsavel_selecionado == '') {
            aviso5.classList.remove('d-none');
        } else {
            aviso5.classList.add('d-none');
        }

        if (atividades == '' || atividades.split(',')[0] == '') {
            aviso6.classList.remove('d-none');
        } else {
            aviso6.classList.add('d-none');
        }

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');
        aviso3.classList.add('d-none');
        aviso4.classList.add('d-none');
        aviso5.classList.add('d-none');
        aviso6.classList.add('d-none');

        let timestamp = Date.now();
        let criacao = new Date(timestamp);
        let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));

        /**
         * Falta fazer as inclusões de campos adicionados e logica de inclusão
         */

        IncluiNovoNumeroDeContral({
            valor: numero_do_contrato_selecionado
        }).then((retorn_numero_de_contrato_sap) => {

            let numero_de_contrato_checado = retorn_numero_de_contrato_sap.id;

            IncluiNovoPrestador({
                valor: empresa_selecionada
            })
            .then((retorno_prestador) => {

                let prestador = retorno_prestador.id;

                IncluiNovoResponsavel({
                    valor: responsavel
                }).then((retorno_resp) => {

                    let retorno_responsavel = retorno_resp.id;

                    IncluiNovoDepResponsavel({
                        valor: dep_responsavel_selecionado
                    })
                    .then((retorno_dep_resp) => {

                        let retorno_dep_responsavel = retorno_dep_resp.id;

                        let consolidado = retorn_numero_de_contrato_sap.consolidado && retorno_prestador.consolidado && retorno_resp.consolidado && retorno_dep_resp.consolidado ? true : false;

                        let dados_verificados_para_cadastrar = {
                            numero_do_contrato: Number(numero_de_contrato_checado),
                            rotulo: rotulo_do_contrato, 
                            escopo: escopo_do_contrato, 
                            empresa: Number(prestador), 
                            responsavel: Number(retorno_responsavel), 
                            dep_responsavel: Number(retorno_dep_responsavel), 
                            atestado_de_ex_de_serv: atestado_ex_de_serv, 
                            art_ct, 
                            ordem_de_servico, 
                            recebimento_provisorio, 
                            data_rec_provisorio, 
                            recebimento_definitivo, 
                            data_rec_definitivo, 
                            data_inicio_execucao, 
                            data_termino_execucao, 
                            data_de_vigencia, 
                            data_base, 
                            status: 1, 
                            assinatuta: data_assinatura, 
                            autor: dados_autor.id,
                            data_de_publicacao: criacao,
                            consolidado
                        };
                
                        //console.log(`Para input:`,dados_verificados_para_cadastrar);
                        let link_atual = window.location.href.split('/');
                        let monta_url = '';
                        for (let contador = 0; contador <= (link_atual.length-2);contador++) {
                            monta_url += link_atual[contador]+'/';
                        }
                
                        DB.contratos.add(dados_verificados_para_cadastrar)
                        .then((resposta)=>{

                            //console.log('Contrato: ',resposta)

                            CadastraAtividadesDoContrato({
                                contrato: resposta,
                                atividades
                            })
                            .then((retorno)=>{
                                console.log(`Contrato cadastrado com êxito!`);
                                alert(`Tudo certo! Contrato cadastrado!`);
                                location.href=monta_url+'lista.html';
                            })
                            .catch(erro=>console.error(erro))
                            
                        })
                        .catch((erro)=>{
                            console.error(`${Erros.EE0005} \n Erro: `,erro);
                        })
                    
                    }).catch(erro => console.error(erro))

                }).catch(erro => console.error(erro))

            }).catch(erro => console.error(erro))

        }).catch(erro => console.error(erro))

    }    

}