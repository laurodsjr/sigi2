import { DB } from "../init-indexeddb";

export function CarregaAtividades(id_select = '', placeholder = 'Escolha uma ou mais atividades', contrato = 0) {

    if (document.getElementById(id_select) !== '') {

        return new Promise(function(resolve, reject) {

            if (contrato != 0) {

                function AtividadesDeContrato(parametros = {}) {
                    const {
                        contrato = 0
                    } = parametros

                    return new Promise(function(resolve, reject) {
                        let consulta = DB.atividades_dos_contratos
                        .where('id_contrato')
                        .equals(contrato)

                        consulta.toArray()
                        .then(retorno => {
                            resolve({
                                registros: retorno.length,
                                dados: retorno
                            });
                        })
                        .catch(erro => {
                            resolve({
                                registros: 0,
                                dados: []
                            });
                        });
                    })

                }

                AtividadesDeContrato({
                    contrato: contrato
                })
                .then(retorno => {

                    if (retorno.registros > 0 ) {
                        const atividades_por_id = retorno.dados.map(atividade => atividade.id);

                        // Checa se existem atividades cadastradas
                        DB.atividades
                        .where('id')
                        .anyOf(atividades_por_id)
                        .count()
                        .then(registros => {
                            if (registros > 0) {
                                DB.atividades
                                .where('id')
                                .anyOf(atividades_por_id)
                                .toArray()
                                .then( (opcao) => { 

                                    let exibir_lista = `<option value="">${placeholder}</option>`;
                                    opcao.forEach((item) => {
                                        exibir_lista += `<option value="${item.id}">${item.id} - ${item.rotulo}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                                    });

                                    // Atualizar o elemento HTML com a lista de origens
                                    const listaOrigens = document.getElementById(id_select);
                                    listaOrigens.innerHTML = exibir_lista;
                                    console.log(`Lista de opções de atividades carregada!`);
                                    resolve();
                                })
                                .catch(erro=>reject(`Houve um erro ao tentar listar as opções de atividades disponíveis! \n Erro: ${erro}`));
                            } else {
                                reject(`Não há opções de atividades para listar!`);
                            }
                        })
                        .catch(erro=>reject(`Houve um erro ao tentar contar as opções de atividades disponíveis! \n Erro: ${erro}`));                        

                    } else {
                        reject(`Não há atividades para este contrato!`)
                    }
                    
                })
                .catch(erro=>reject(`Houve um erro ao tentar contar as opções de atividades disponíveis por este contrato! \n Erro: ${erro}`));

            } else {            

                // Checa se existem atividades cadastradas
                DB.atividades.count()
                .then(registros => {
                    if (registros > 0) {
                        const atividades = DB.atividades.toArray()
                        .then( (opcao) => { 

                            let exibir_lista = `<option value="">${placeholder}</option>`;
                            opcao.forEach((item) => {
                                exibir_lista += `<option value="${item.id}">${item.id} - ${item.rotulo}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                            });

                            // Atualizar o elemento HTML com a lista de origens
                            const listaOrigens = document.getElementById(id_select);
                            listaOrigens.innerHTML = exibir_lista;
                            console.log(`Lista de opções de atividades carregada!`);
                            resolve();
                        })
                        .catch(erro=>reject(`Houve um erro ao tentar listar as opções de atividades disponíveis! \n Erro: ${erro}`));
                    } else {
                        reject(`Não há opções de atividades para listar!`);
                    }
                })
                .catch(erro=>reject(`Houve um erro ao tentar contar as opções de atividades disponíveis! \n Erro: ${erro}`));

            }

        })

    }
    
}