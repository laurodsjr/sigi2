import { DB } from "../init-indexeddb";
import { CarregaUsuariosPorFuncao } from "../comum/carrega-usuarios-por-funcao";
import { SelectCustomizado } from "../comum/select-customizado";
import { CarregaNumerosDeContratoDoSAP } from "./carrega-dados-de-numeros-de-contrato-sap";
import { CarregaDepartamentos } from "./carrega-departamentos";
import { CarregaPrestadoresDeServicos } from "./carrega-prestadores-de-servicos";
import { CarregaAtividades } from "./carrega-atividades";
import { CarregaAtividadesDoContrato } from "../atividades-dos-contratos/carrega-atividades-do-contrato";
import { Erros } from "../../erros/erros";

export async function CarregaDadosDoContrato(pagina_atual = '') {

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);
    
    if (grupo == 'contratos') {
    
        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
                                    
                async function RetornaDados(id) {
                    const retorno = await DB.contratos.get(Number(id))
                    return retorno
                }

                RetornaDados(identificador)
                .then(dados => {

                    CarregaNumerosDeContratoDoSAP({
                        id_select: 'opcoes-numero-do-contrato',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-numero-do-contrato',
                            placeholder: 'Escolha ou digite um número aqui',
                            sem_resultados: 'Sem mais números!',
                            sem_resultados_na_pesquia: 'Nenhuma número de contrato encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.numero_do_contrato),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-numero-do-contrato-selecionado").value = dados.numero_do_contrato;
                    }).catch(e=>console.error('Erro: ',e))
            
                    CarregaPrestadoresDeServicos({
                        id_select: 'opcoes-de-empresa-do-contrato',
                        pagina: pagina_atual
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-de-empresa-do-contrato',
                            placeholder: 'Escolha uma empresa',
                            sem_resultados: 'Sem mais empresas!',
                            sem_resultados_na_pesquia: 'Nenhuma empresa encontrada!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.empresa),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-de-empresa-do-contrato-selecionado").value = dados.empresa;
                    }).catch(e=>console.error('Erro: ',e))
                
                    CarregaUsuariosPorFuncao({
                        id_select: 'opcoes-para-responsavel',
                        placeholder: 'Escolha um responsável'
                    })
                    .then (()=>{
                        SelectCustomizado({
                            id_do_select: 'opcoes-para-responsavel',
                            placeholder: 'Escolha o responsável',
                            sem_resultados: 'Sem mais responsável!',
                            sem_resultados_na_pesquia: 'Nenhum responsável encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.responsavel),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-para-responsavel-selecionado").value = dados.responsavel;
                    }).catch(e=>console.error('Erro: ',e))    
                
                
                    CarregaDepartamentos('opcoes-dep-responsavel')
                    .then(opcoes => {
                        SelectCustomizado({
                            id_do_select: 'opcoes-dep-responsavel',
                            placeholder: 'Escolha o dep. responsável',
                            sem_resultados: 'Sem mais dep. responsáveis!',
                            sem_resultados_na_pesquia: 'Nenhum departamento encontrado!',
                            sugestao_de_adicao: 'Clique para selecionar',
                            adicionavel: true,
                            customizar: pagina_atual === 'lista.html' ? false : true,
                            pre_selecionado: Number(dados.dep_responsavel),
                            pagina: pagina_atual
                        });
                        document.getElementById("opcoes-dep-responsavel-selecionado").value = dados.dep_responsavel;
                    }).catch(e=>console.error('Erro: ',e))

                    CarregaAtividades('opcoes-atividades-do-contrato')
                    .then(opcoes => {

                        CarregaAtividadesDoContrato({
                            contrato: Number(dados.id)
                        })
                        .then((retorno_das_atividades_do_contrato)=>{
                            SelectCustomizado({
                                id_do_select: 'opcoes-atividades-do-contrato',
                                placeholder: 'Escolha uma ou mais atividades',
                                sem_resultados: 'Sem mais atividades!',
                                sem_resultados_na_pesquia: 'Nenhuma atividade encontrada!',
                                sugestao_de_adicao: 'Clique para selecionar',
                                adicionavel: true,
                                customizar: pagina_atual === 'lista.html' ? false : true,
                                pre_selecionado: retorno_das_atividades_do_contrato,
                                pagina: pagina_atual
                            });
                            document.getElementById("opcoes-atividades-do-contrato-selecionado").value = retorno_das_atividades_do_contrato.join(',');
                        }).catch(e=>console.error('Erro: ',e))

                    }).catch(e=>console.error('Erro: ',e))

                    /**
                     * Campos para carregar os dados
                     */
                    document.getElementById("identificador").value = identificador;
                    document.getElementById("rotulo-do-contrato").value = dados.rotulo;
                    document.getElementById("escopo-do-contrato").value = dados.escopo;
                    document.getElementById("atestado-ex-de-serv-contrato").value = dados.atestado_de_ex_de_serv;
                    document.getElementById("art-ct-contrato").value = dados.art_ct;
                    document.getElementById("ordem-de-servico-contrato").value = dados.ordem_de_servico;
                    document.getElementById("recebimento-provisorio-contrato").value = dados.recebimento_provisorio;
                    document.getElementById("data-rec-provisorio-contrato").value = dados.data_rec_provisorio;
                    document.getElementById("recebimento-definitivo-contrato").value = dados.recebimento_definitivo;
                    document.getElementById("data-rec-definitivo-contrato").value = dados.data_rec_definitivo;
                    document.getElementById("data-incicio-execucao-contrato").value = dados.data_inicio_execucao;
                    document.getElementById("data-termino-execucao-contrato").value = dados.data_termino_execucao;
                    document.getElementById("data-de-vigencia-contrato").value = dados.data_de_vigencia;
                    document.getElementById("data-base-contrato").value = dados.data_base;
                    document.getElementById("data-assinatura-contrato").value = dados.assinatuta;
                    document.getElementById("observacoes-contrato").setAttribute('identificador',identificador);
                    document.getElementById("nova-observacao-contrato").setAttribute('identificador',identificador);


                    console.log(`Dados do contrato ${identificador} - ${dados.rotulo} carregados com sucesso!`);
                })
                .catch(e => {
                    console.error(`${Erros.EL0005}\n Mensagem: `,e);
                });

    
            })
    
        }

    }

}