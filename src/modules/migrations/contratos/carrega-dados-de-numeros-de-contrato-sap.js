import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaNumerosDeContratoDoSAP(propriedades = {
    id_select,
    placeholder,
    pagina
}) {

    const {
        id_select = '',
        placeholder = null,
        pagina = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        // Checa se existem departamentos cadastrados
        DB.numeros_de_contrato_sap.count()
        .then(registros => {
            if (registros > 0) {
                const contratos_sap = DB.numeros_de_contrato_sap.toArray()
                .then( (opcao) => { 

                    let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha um contrato`}</option>`;
                    opcao.forEach((item) => {
                        exibir_lista += `<option value="${item.id}">${item.numero}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                    });

                    // Atualizar o elemento HTML com a lista de origens
                    const listaOrigens = document.getElementById(id_select);
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de opções de número de contrato carregada!`);
                    resolve();
                })
                .catch(erro=>reject(`${Erros.EQ0018} \n Erro: ${erro}`));
            } else {
                let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Inclua um novo contrato`}</option>`;
                const listaOrigens = document.getElementById(id_select);
                if (pagina == 'lista.html') {
                    document.getElementById(`area-${id_select}`).classList.add('d-none');
                }
                listaOrigens.innerHTML = exibir_lista;
                console.log(`Não há opções de número de contrato do SAP para listar!`);
                resolve();
            }
        })
        .catch(erro=>reject(`${Erros.EQ0017} \n Erro: ${erro}`));

    })

}