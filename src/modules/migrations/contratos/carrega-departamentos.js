import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaDepartamentos(id_select = '') {

    if (document.getElementById(id_select) !== '') {

        return new Promise(function(resolve, reject) {

            // Checa se existem departamentos cadastrados
            DB.departamentos.count()
            .then(registros => {
                if (registros > 0) {
                    const departamentos = DB.departamentos.toArray()
                    .then( (opcao) => { 
                        /*let retorno = [{
                            value: '',
                            label: 'Escolha um departamento responsável'
                        }];
                        opcao.forEach((item) => {
                            retorno.push({
                                value: item.id,
                                label: item.nome
                            })
                        })*/
                        let exibir_lista = `<option value="">Escolha um departamento</option>`;
                        opcao.forEach((item) => {
                            exibir_lista += `<option value="${item.id}">${item.nome}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                        });

                        // Atualizar o elemento HTML com a lista de origens
                        const listaOrigens = document.getElementById(id_select);
                        listaOrigens.innerHTML = exibir_lista;
                        console.log(`Lista de opções de departamento carregada!`);
                        resolve();
                    })
                    .catch(erro=>reject(`${Erros.EQ0012}! \n Erro: ${erro}`));
                } else {
                    reject(`Não há opções de departamento para listar!`);
                }
            })
            .catch(erro=>reject(`${Erros.EQ0010}! \n Erro: ${erro}`));

        })

    }
    
}