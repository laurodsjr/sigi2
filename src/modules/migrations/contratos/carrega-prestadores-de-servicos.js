import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";

export function CarregaPrestadoresDeServicos(propriedades = {
    id_select,
    placeholder,
    pagina
}) {

    const {
        id_select = '',
        placeholder = null,
        pagina = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        // Checa se existem departamentos cadastrados
        DB.prestadores_de_servico.count()
        .then(registros => {
            if (registros > 0) {
                const departamentos = DB.prestadores_de_servico.toArray()
                .then( (opcao) => { 

                    let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha uma empresa`}</option>`;
                    opcao.forEach((item) => {
                        exibir_lista += `<option value="${item.id}">${item.razao_social}${item.status == 0 ? ' <b>[Em análise]</b>' : ''}</option>`;
                    });

                    // Atualizar o elemento HTML com a lista de origens
                    const listaOrigens = document.getElementById(id_select);
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de opções de prestadores de serviços carregada!`);
                    resolve();
                })
                .catch(erro=>reject(`${Erros.EQ0020} \n Erro: ${erro}`));
            } else {
                let exibir_lista = `<option value="">${placeholder !== null ? placeholder : `Escolha uma empresa`}</option>`;
                const listaOrigens = document.getElementById(id_select);
                if (pagina == 'lista.html') {
                    document.getElementById(`area-${id_select}`).classList.add('d-none');
                }
                listaOrigens.innerHTML = exibir_lista;
                console.log(`Não há opções de prestadores de serviços para listar!`);
                resolve();
            }
        })
        .catch(erro=>reject(`${Erros.EQ0019} \n Erro: ${erro}`));

    })

}