import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";
import { ListagemContratos } from "./listagem";

export function ExcluirContrato() {

    document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('listagem-de-contratos') !== null) {
            let listagem = document.getElementById('listagem-de-contratos');
            listagem.addEventListener('click', function(event) {
                if (event.target.matches('#excluir')) {
                    let id = event.target.getAttribute('identificador');
                    console.log('Solicitação de exclusão do identificador de contrato: ',id);

                    let confirmacao = confirm('Você tem certeza que deseja excluir este contrato?');
                    if (confirmacao) {
                        DB.contratos.delete(Number(id))
                        .then(resposta => { 
                            alert(`Contrato ${id} excluido com sucesso!`);
                            console.log(`Contrato ${id} excluido com sucesso!`);
                            ListagemContratos()
                        })
                        .catch(e => {
                            console.error(`${Erros.ED0002}\n Erro: `,e);
                        })
                    } else {
                        console.log('Operação cancelada pelo usuário!');
                    }
                    
                }
            })
        }
    });

}