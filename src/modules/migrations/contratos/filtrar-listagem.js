import { MontaLink } from "../../customizacao/monta-link";
import { TextoResumido } from "../../customizacao/texto-resumido";
import { Erros } from "../../erros/erros";
import { DB } from "../init-indexeddb";  

export function FiltrarListagemContratos() {

    const
        numero_do_contrato = document.getElementById('opcoes-numero-do-contrato-selecionado')?.value,
        rotulo = document.getElementById('rotulo-do-contrato')?.value,
        empresa = document.getElementById('opcoes-de-empresa-do-contrato-selecionado')?.value,
        responsavel = document.getElementById('opcoes-para-responsavel-selecionado')?.value,
        dep_responsavel = document.getElementById('opcoes-dep-responsavel-selecionado')?.value
    ;

    DB.numeros_de_contrato_sap.toArray()
    .then(contratos_sap => {

        function ContratosSAP(id) {
            if (contratos_sap.length <= 0 || String(Number(id)) === "NaN") { return `${id} [Inconsistente]`; } 
            else {
                let contratos_filtrados = contratos_sap.filter(item => {
                    if (item.id == Number(id)) {
                        return item?.numero || item?.id
                    }
                });
                return {
                    status: Number(contratos_filtrados[0]?.status) === 0 ? false : true,
                    resposta: `${contratos_filtrados[0]?.numero}${Number(contratos_filtrados[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                };
            }
        }

        DB.prestadores_de_servico.toArray()
        .then(prestadores => {

            function Empresas(id) {

                if (prestadores.length <= 0) { return id; } 
                else {
                    let prestador_filtrado = prestadores.filter(item => {
                        if (item.id == Number(id)) {
                            return item?.nome || item?.id
                        }
                    });
                    //console.log(prestador_filtrado);
                    return {
                        status: Number(prestador_filtrado[0]?.status) === 0 ? false : true,
                        resposta: `${prestador_filtrado[0]?.razao_social}${Number(prestador_filtrado[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                    };
                }
            }

            DB.status_do_contrato.toArray()
            .then(status => {

                function StatusDoContrato(id) {
                    if (prestadores.length <= 0) { return id; } 
                    else {
                        let status_filtrado = status.filter(item => {
                            if (item.id == Number(id)) {
                                return item?.titulo || item?.id
                            }
                        });
                        let color = Number(id) == 1 ? 'primary' :
                            Number(id) == 2 ? 'success' :
                            Number(id) == 3 ? 'danger' :
                            Number(id) == 4 ? 'warning' :
                            Number(id) == 5 ? 'danger' :
                            Number(id) == 6 ? 'danger' :
                            Number(id) == 7 ? 'success' :
                            Number(id) == 8 ? 'danger' :
                            'warning';
                        return `<span class="badge text-bg-${color}">${status_filtrado[0]?.titulo}</span>`
                    }
                }

                DB.departamentos.toArray()
                .then(departamento => {

                    function DepResponsavel(id) {
                        let departamento_filtrado = departamento.filter(item => {
                            if (item.id == Number(id)) {
                                return item?.nome || item?.id
                            }
                        });
                        return {
                            status: Number(departamento_filtrado[0]?.status) === 0 ? false : true,
                            resposta: `${departamento_filtrado[0]?.nome}${Number(departamento_filtrado[0]?.status) === 0 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                        };
                    }

                    DB.usuarios.toArray()
                    .then(usuario => {

                        function Responsavel(id) {
                            let usuario_filtrado = usuario.filter(item => {
                                if (item.id == Number(id)) {
                                    return item?.nome || item?.id
                                }
                            });
                            return {
                                status: usuario_filtrado[0]?.funcao == 5 ? false : true,
                                resposta: `${usuario_filtrado[0]?.nome}${usuario_filtrado[0]?.funcao == 5 ? ' <i class="fa-solid fa-triangle-exclamation text-warning"></i>' : ''}`
                            };
                        }

                        function FiltrarContratos(parametros = {}) {
                            const {
                              numeroContrato = '',
                              rotuloDoContrato = '',
                              idEmpresa = '',
                              idResponsavel = '',
                              idDepResponsavel = ''
                            } = parametros;
                          
                            return new Promise(function(resolve, reject) {
                              let consulta = DB.contratos;
                          
                              if (numeroContrato !== '') {
                                consulta = consulta.filter(registro => registro.numero_do_contrato === numeroContrato);
                              }
                          
                              if (rotuloDoContrato !== '') {
                                consulta = consulta.filter(registro => registro.rotulo === rotuloDoContrato);
                              }
                          
                              if (idEmpresa !== '') {
                                consulta = consulta.filter(registro => registro.empresa === idEmpresa);
                              }
                          
                              if (idResponsavel !== '') {
                                consulta = consulta.filter(registro => registro.responsavel === idResponsavel);
                              }
                          
                              if (idDepResponsavel !== '') {
                                consulta = consulta.filter(registro => registro.dep_responsavel === idDepResponsavel);
                              }
                          
                              consulta.toArray()
                                .then(contratos => {
                                  resolve({
                                    registros: contratos.length,
                                    dados: contratos
                                  });
                                })
                                .catch(erro => {
                                  console.error(`Houve um erro ao tentar filtrar os contratos! Erro: ${erro}`);
                                  resolve({
                                    registros: 0,
                                    dados: []
                                  });
                                });
                            });
                        }                          

                        
                        FiltrarContratos({
                            numeroContrato: numero_do_contrato,
                            rotuloDoContrato: rotulo,
                            idEmpresa: empresa,
                            idResponsavel: responsavel,
                            idDepResponsavel: dep_responsavel
                        })
                        .then(retorno => {

                            if (retorno.registros > 0) {
                                FiltrarContratos({
                                    numeroContrato: numero_do_contrato,
                                    rotuloDoContrato: rotulo,
                                    idEmpresa: empresa,
                                    idResponsavel: responsavel,
                                    idDepResponsavel: dep_responsavel
                                })
                                .then((retorno2) => {               
                                        
                                    let exibir_tabela = `<table id="table-list" class="w-100">
                                        <thead id="table-title">
                                            <tr>
                                                <th class="p-2">ID</th>
                                                <th class="p-2 col-sap">SAP</th>
                                                <th class="p-2">RÓTULO</th>
                                                <th class="p-2 descricoes">ESCOPO</th>
                                                <th class="p-2">EMPRESA</th>
                                                <th class="p-2">RESPONSÁVEL</th>
                                                <th class="p-2">DEP. RESPONSÁVEL</th>
                                                <th class="p-2">STATUS</th>
                                                <th class="p-2 obs">OBS</th>
                                                <th class="p-2 last">AÇÕES</th>
                                            </tr>
                                        </thead>
                                    <tbody>`;

                                    retorno2.dados.forEach((item, key) => {

                                        let consolidado = ( ContratosSAP(item.numero_do_contrato).status && Empresas(Number(item.empresa)).status && Responsavel(item.responsavel).status && DepResponsavel(Number(item.dep_responsavel)).status ) || item.consolidado ? '' : ` <span class="badge text-bg-warning">Não consolidado</span>`;

                                        exibir_tabela += `<tr class="${key % 2 !== 0 ? 'even-line' : ''}">
                                            <td class="${'p-2'} text-center">${item.id}</td>
                                            <td class="${'p-2'} text-center col-sap">${ContratosSAP(item.numero_do_contrato).resposta}</td>
                                            <td class="${'p-2'}">${item.rotulo ? item.rotulo : `Sem rótulo`}</td>
                                            <td class="${'p-2'} descricoes">${TextoResumido({texto: item.escopo, limite_de_letras: 128})}</td>
                                            <td class="${'p-2'}">${Empresas(Number(item.empresa)).resposta}</td>
                                            <td class="${'p-2'}">${Responsavel(item.responsavel).resposta}</td>
                                            <td class="${'p-2'}">${DepResponsavel(Number(item.dep_responsavel)).resposta}</td>
                                            <td class="${'p-2'}">${StatusDoContrato(item.status)}${consolidado}</td>
                                            <td class="${'p-2'}">
                                                <a href="${MontaLink(`observacoes/lista.html?tipo=contrato&id=${item.id}`)}" data-bs-toggle="tooltip" data-bs-title="Ver observações deste contrato!" title="Ver observações deste contrato!">
                                                    <button class="btn btn-info"><i class="fas fa-sticky-note"></i></button>
                                                </a>
                                                <a href="${MontaLink(`observacoes/incluir.html?tipo=contrato&id=${item.id}`)}" data-bs-toggle="tooltip" data-bs-title="Nova observação para este contrato!" title="Nova observação para este contrato!">
                                                    <button class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                                </a>
                                            </tc>
                                            <td class="${'p-2 actions'}">
                                                <a href="${MontaLink(`contratos/visualizar.html?id=${item.id}`)}">
                                                    <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="contratos" identificador="${item.id}"></i>
                                                </a>
                                                <a href="${MontaLink(`contratos/editar.html?id=${item.id}`)}">
                                                    <i class="fa-solid fa-pen-to-square" id="editar" tipo="contratos" identificador="${item.id}"></i>
                                                </a>
                                                <i class="fa-solid fa-trash-can" id="excluir" tipo="contratos" identificador="${item.id}"></i>
                                            </td>
                                        </tr>`;
                                    });

                                    exibir_tabela += `</tdoby></table>`;

                                    // Atualizar o elemento HTML com a lista de origens
                                    if (document.getElementById("listagem-de-contratos") !== null) {
                                        const listaOrigens = document.getElementById("listagem-de-contratos");
                                        listaOrigens.innerHTML = exibir_tabela;
                                        console.log(`Lista de contratos carregada!`);
                                    }
                                    
                                }).catch(e => {
                        
                                    console.error(`${Erros.EL0007}\n Mensagem: ${e}`);
                            
                                })

                            } else {
                                
                                if (document.getElementById("listagem-de-contratos") !== null) {
                                    const listaOrigens = document.getElementById("listagem-de-contratos");
                                    listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há contratos cadastrados!</p>';
                                    console.log(`Não há contratos cadastrados!`);
                                }

                            }
                        })
                        .catch(e => {
                            console.error(`${Erros.EQ0032}\n Mensagem: `,e);
                        });

                    }).catch(e => console.error(`${Erros.EQ0031} Erro: `,e));

                }).catch(e => console.error(`${Erros.EQ0030} Erro: `,e));

            }).catch(e => console.error(`${Erros.EQ0029} Erro: `,e));

        }).catch(e => console.error(`${Erros.EQ0028} Erro: `,e));

    }).catch(e => console.error(`${Erros.EQ0027} Erro: `,e));

}