import { DB } from "../init-indexeddb";

export function IncluiNovoDepResponsavel(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {
    
        if (String(Number(valor)) == 'NaN' && valor != '' && valor != undefined) {

            DB.departamentos
            .add({
                nome: valor,
                status: Number(0)
            })
            .then( (primaryKey) => { 
                console.log(`Novo departamento responsável incluído para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir o novo departamento responsável! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.departamentos.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Este departamento responsável já foi revisado e aprovado!` : `Este departamento responsável já foi cadastrado e será revisado!` );
                resolve({
                    id: valor,
                    consolidado: dados.status ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem do departamento! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })            
        }

    })

}