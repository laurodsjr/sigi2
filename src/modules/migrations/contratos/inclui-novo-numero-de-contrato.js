import { DB } from "../init-indexeddb";

export function IncluiNovoNumeroDeContral(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        console.log('Testa > ',Number(valor), String(Number(valor)));
    
        if (String(Number(valor)) == 'NaN') {
            DB.numeros_de_contrato_sap
            .add({
                numero: valor,
                status: Number(0)
            })
            .then( (primaryKey) => { 
                console.log(`Novo número de contrato incluído para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir o novo número de contrato! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.numeros_de_contrato_sap.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Este número de contrato SAP já foi revisado e aprovado!` : `Este número de contrato SAP já foi cadastrado e será revisado!` );
                resolve({
                    id: valor,
                    consolidado: dados.status ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem do número de contrato SAP! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}