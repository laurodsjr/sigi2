import { DB } from "../init-indexeddb";

export function IncluiNovoPrestador(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {

        //console.log('Testa > ',Number(valor));
    
        if (String(Number(valor)) == 'NaN') {
            DB.prestadores_de_servico
            .add({
                razao_social: valor,
                status: Number(0)
            })
            .then( (primaryKey) => { 
                console.log(`Novo prestador de serviço incluído para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir o novo prestadores de serviços! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.prestadores_de_servico.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Este prestador já foi revisado e aprovado!` : `Este prestador já foi cadastrado e será revisado!` );
                resolve({
                    id: valor,
                    consolidado: dados.status ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem do prestador! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}