import { DB } from "../init-indexeddb";

export function IncluiNovoResponsavel(propriedades = {
    valor
}) {

    const {
        valor = ''
    } = propriedades

    return new Promise(function(resolve, reject) {
    
        if (String(Number(valor)) == 'NaN') {

            DB.usuarios
            .add({
                nome: valor,
                funcao: Number(5),
            })
            .then( (primaryKey) => { 
                console.log(`Novo responsavel incluído para aprovação!`, primaryKey);
                resolve({
                    id: primaryKey,
                    consolidado: false
                });
            })
            .catch(erro=>reject(`Houve um erro ao tentar incluir o novo responsável! \n Erro: ${erro}`));
        
        } else {
            async function RetornaDados(id) {
                const retorno = await DB.usuarios.get(Number(id))
                return retorno
            }
            RetornaDados(valor)
            .then(dados => {
                console.log( dados.status ? `Este responsável já foi revisado e aprovado!` : `Este responsável já foi cadastrado e será revisado!` );
                resolve({
                    id: valor,
                    consolidado: dados.funcao != 5 ? true : false
                });
            })
            .catch((e)=>{
                console.error(`Erro na checagem do responsável! Erro: ${e}`)
                resolve({
                    id: valor,
                    consolidado: false
                });
            })
        }

    })

}