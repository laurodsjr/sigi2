import { DB } from "../init-indexeddb";

export function IncluiPrestadorDeServicos(dados = {
    razao_social, 
    cnpj, 
    informacoes, 
    status, 
    autor
}) {

    const {
        razao_social = '', 
        cnpj = '', 
        informacoes = '', 
        status = 2, 
        autor = 0
    } = dados

    return new Promise(function(resolve, reject) {

        if (razao_social == '' && autor == 0) {
            reject(`O nome da empresa e o autor não foram encontrados!`);
        }

        if (razao_social == '') {
            reject(`O nome da empresa não foi informado!`);
        }

        if (autor == 0) {
            reject(`O autor não foi informado!`);
        }

        DB.prestadores_de_servico.add({
            razao_social, 
            cnpj, 
            informacoes, 
            status: Number(status), 
            autor: Number(autor)
        }).then(()=>{
            resolve(`Nova empresa cadastrada!`);
        })
        .catch(erro=>reject(`Ops, houve um erro ao cadastrar a empresa! Erro: ${erro}`));
        
    })
    
}