import { DB } from "../init-indexeddb";

export function InsereDepartamentosIniciais() {
    DB.departamentos.count()
    .then(registros => {
        if (registros < 1) {
            DB.departamentos.bulkAdd([
                {nome: 'PLAN.Centro-Oeste', informacoes: '', status: 1},
                {nome: 'PLAN.Sul', informacoes: '', status: 1},
                {nome: 'PLAN.Suldeste', informacoes: '', status: 1},
                {nome: 'OBRA.Centro-Oeste', informacoes: '', status: 1},
                {nome: 'OBRA.Sul', informacoes: '', status: 1},
                {nome: 'OBRA.Suldeste', informacoes: '', status: 1},
                {nome: 'GER', informacoes: '', status: 1},
                {nome: 'COMPARTILHADO', informacoes: '', status: 1}
            ]).then((e) => {
    
                console.log(`Departamentos inciais carregados!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar os departamentos iniciais! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo para os departamentos iniciais!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de departamentos iniciais!`);
    });
}