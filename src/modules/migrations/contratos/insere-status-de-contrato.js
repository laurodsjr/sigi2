import { DB } from "../init-indexeddb";

export function InsereStatusDeContrato() {
    DB.status_do_contrato.count()
    .then(registros => {
        if (registros < 1) {
            DB.status_do_contrato.bulkAdd([
                {titulo: 'Novo', descricao: ''},
                {titulo: 'Vigente', descricao: ''},
                {titulo: 'Encerrado', descricao: ''},
                {titulo: 'Concluido com Pendências', descricao: ''},
                {titulo: 'Suspenso', descricao: ''},
                {titulo: 'Cancelado', descricao: ''},
                {titulo: 'Vigente Vigente - CCI/CCT/PV/CD/...', descricao: ''},
                {titulo: 'Encerrado -  CCI/CCT/PV/CD/....', descricao: ''},
                {titulo: 'Indefinido *', descricao: ''}
            ]).then((e) => {
    
                console.log(`Status de contrato carregados!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar os status de contrato! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em status de contrato!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de status de contrato pré-cadastrados!`);
    });
}