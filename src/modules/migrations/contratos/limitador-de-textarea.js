export function LimitadorDeTextarea(propriedades ={
    limite_de_caracteres,
    id_do_text_area
}) {

    const {
        limite_de_caracteres = 256
    } = propriedades

    document.addEventListener('DOMContentLoaded', function() {
        let escopos_contratacao = document.querySelectorAll('.espaco-de-texto');
        escopos_contratacao.forEach(function(escopo_contratacao) {
            let texto = escopo_contratacao.querySelector('textarea');
            texto.maxLength = limite_de_caracteres;
            let espaco_limite = escopo_contratacao.querySelector('.limite')
            espaco_limite.innerHTML=`Restam <b>${Number(limite_de_caracteres)}</b> caracteres`
            let limite = limite_de_caracteres;
            texto.addEventListener('input', function(event) {
                let tamanho = event.target.value.length;
                if (Number(limite-tamanho) < 1) {
                    espaco_limite.classList.add('text-danger')
                } else {
                    espaco_limite.classList.remove('text-danger')
                }
                espaco_limite.innerHTML=`Restam <b>${Number(limite-tamanho)}</b> caracteres`;
            });
        });
    });

}