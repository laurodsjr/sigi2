import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros";
import { DB } from "../init-indexeddb";

export async function CarregaDadosDeEmpreedimento() {

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);
    
    if (grupo == 'empreendimentos') {
    
        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
    
                DB.solicitacao.toArray()
                .then(solicitacao => {
    
                    function FiltraASolicitacao(id) {
                        return solicitacao.filter(item => {
                            if (item.id == id) {
                                return item?.titulo || item?.id
                            }
                        });
                    }
    
                    DB.empreendimentos.count()
                    .then(registros => {
                        if (registros > 0) {
    
                            async function RetornaDados(id) {
                                const retorno = await DB.empreendimentos.get(Number(id))
                                return retorno
                            }
    
                            RetornaDados(identificador)
                            .then((dados)=>{
                                
                                let solicitacao = FiltraASolicitacao(dados.solicitacao)[0]?.id ? `#${AcrescentadorDeZeros(FiltraASolicitacao(dados.solicitacao)[0]?.id,4)}` : dados.solicitacao;
    
                                document.getElementById('titulo-empreendimento').value = dados.titulo;
                                document.getElementById('escopo-empreendimento').value = dados.escopo;
    
                                if (pagina == 'visualizar.html') {
                                    document.getElementById('lista-solicitacoes').value = solicitacao;
                                } else {
                                    document.getElementById('identificador').value = identificador;
    
                                    // Obtenha uma referência ao elemento select
                                    let selectSolicitacao = document.getElementById('lista-solicitacoes');
                                    // Defina o valor que você deseja selecionar
                                    let solicitacaoDefinida = String(dados.solicitacao);
                                    // Percorra as opções para encontrar o valor correspondente
                                    for (let i = 0; i < selectSolicitacao.options.length; i++) {
                                        if (selectSolicitacao.options[i].value === solicitacaoDefinida) {
                                            // Atribua o valor encontrado ao elemento select
                                            selectSolicitacao.value = solicitacaoDefinida;
                                            //selectSolicitacao.selectedIndex = -1;
                                            break;
                                        }
                                    }
                                    document.getElementById('lista-solicitacoes-edit').value = dados.solicitacao;
    
                                }
    
                            })
    
                        } else {
                            console.log(`Não há dados para serem exibidos!`);
                        }
                    })
                    .catch(e => {
                        console.error(`Erro ao tentar contar os registros da lista de empreendimentos cadastrados!\n Mensagem: `,e);
                    });
    
                })
                .catch(e=>{
                    console.error(`Erro ao tentar listar as origens para os empreendimentos cadastrados!\n Mensagem: `,e);
                });
    
            })
    
        }

    }

}