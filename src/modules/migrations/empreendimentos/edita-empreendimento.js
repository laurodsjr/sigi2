import { DB } from "../init-indexeddb";

export function EditaEmpreendimento() {

    /**
     * Recebendo os valores dos campos
     */
    let 
    identificador = Number(document.getElementById("identificador").value),
    solicitacao = Number(document.getElementById("lista-solicitacoes").value),
    titulo = document.getElementById("titulo-empreendimento").value,
    escopo = document.getElementById("escopo-empreendimento").value;


    let fieldSolicitacao = document.getElementById("field-solicitacoes-empreendimento");
    let aviso = fieldSolicitacao.querySelector('.text-danger');
    
    let fieldTitulo = document.getElementById("field-titulo-empreendimento");
    let aviso2 = fieldTitulo.querySelector('.text-danger');

    let fieldEscopoEmpreendimento = document.getElementById("field-escopo-empreendimento");
    let aviso3 = fieldEscopoEmpreendimento.querySelector('.text-danger');

    if (solicitacao == 0 || titulo == '' || escopo == '') {

        if (solicitacao == 0 && titulo == '' && escopo == '') {
            console.error('Os campos: Solicitação, Título e Escopo, são obrigatórios!'); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
            aviso3.classList.remove('d-none');
        } 
        
        if (solicitacao == 0) {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if (titulo == '') {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 
        
        if (escopo == '') {
            aviso3.classList.remove('d-none');
        } else {
            aviso3.classList.add('d-none');
        }

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');
        aviso3.classList.add('d-none');

        //let timestamp = Date.now();
        //let criacao = new Date(timestamp);
        //let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));


        let dados_verificados_para_cadastrar = {
            solicitacao: Number(solicitacao),
            titulo: titulo,
            escopo: escopo
        };

        //console.log(`Para input:`,dados_verificados_para_cadastrar);
        let link_atual = window.location.href.split('/');
        let monta_url = '';
        for (let contador = 0; contador <= (link_atual.length-2);contador++) {
            monta_url += link_atual[contador]+'/';
        }

        DB.empreendimentos.update(Number(identificador),dados_verificados_para_cadastrar)
        .then((resposta)=>{
            console.log(`Emoreendimento atualizado com êxito!`);
            alert(`Tudo certo! Empreendimento atualizado!`);
            location.href=monta_url+'lista.html';
        })
        .catch((erro)=>{
            console.error(`Erro ao atualizar o empreendimento: \n Erro: `,erro);
        })

    }      

}