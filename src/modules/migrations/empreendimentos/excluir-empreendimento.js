import { DB } from "../init-indexeddb";
import { ListagemEmpreendimentos } from "./listagem";

export function ExcluirEmpreendimento(id = 0) {
    
    let confirmacao = confirm('Você tem certeza que deseja excluir este empreendimento?');

    if (confirmacao) {
        DB.empreendimentos.delete(Number(id))
        .then(resposta => { 
            alert(`Empreendimento ${id} excluido com sucesso!`);
            console.log(`Empreendimento ${id} excluido com sucesso!`);
            ListagemEmpreendimentos();
        })
        .catch(e => {
            console.error(`Houve um erro ao tentar remover este empreendimento!\n Erro: `,e);
        })
    } else {
        console.log('Operação cancelada pelo usuário!');
    }

}