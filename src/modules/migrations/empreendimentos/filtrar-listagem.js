import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros";
import { ConverteData } from "../../customizacao/formata-data";
import { MontaLink } from "../../customizacao/monta-link";
import { DB } from "../init-indexeddb";  

export function FiltrarListagemEmpreendimentos() {

    let solicitacao_filtro = Number(document.getElementById('lista-solicitacoes')?.value) || 0,
    titulo_filtro = document.getElementById('titulo-empreendimento')?.value || '';

    DB.solicitacao.toArray()
    .then(solicitacao => {

        function FiltraASolicitacao(id) {
            return solicitacao.filter(item => {
                if (item.id == id) {
                    return item?.titulo || `#${AcrescentadorDeZeros(item?.id,4)}`
                }
            });
        }
        
        DB.empreendimentos
        .where({
            solicitacao: solicitacao_filtro
        })
        .filter(item => item.titulo.includes(titulo_filtro))
        .count()
        .then(registros => {
            if (registros > 0) {
                DB.empreendimentos
                .where({
                    solicitacao: solicitacao_filtro
                })
                .filter(item => item.titulo.includes(titulo_filtro))
                .toArray()
                .then((lista) => {    
                        
                    let exibir_tabela = `<table id="table-list" class="w-100">
                        <thead id="table-title">
                            <tr>
                                <th class="p-2">IDENTIFICADOR</th>
                                <th class="p-2">TÍTULO</th>
                                <th class="p-2">SOLICITAÇÃO</th>
                                <th class="p-2">ESCOPO</th>
                                <th class="p-2 last">AÇÕES</th>
                            </tr>
                        </thead>
                    <tbody>`;

                    lista.forEach((item, key) => {

                        let solicitacao = FiltraASolicitacao(item.solicitacao)[0]?.id ? `#${AcrescentadorDeZeros(FiltraASolicitacao(item.solicitacao)[0]?.id,4)}` : item.solicitacao;

                        exibir_tabela += `<tr">
                            <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.id}</td>
                            <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.titulo}</td>
                            <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${solicitacao}</td>
                            <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.escopo ? item.escopo : `-` }</td>
                            <td class="${key % 2 !== 0 ? 'even-line p-2 actions' : 'p-2 actions'}">
                                <a href="${MontaLink(`empreendimentos/visualizar.html?id=${item.id}`)}">
                                    <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="empreendimentos" identificador="${item.id}"></i>
                                </a>
                                <a href="${MontaLink(`empreendimentos/editar.html?id=${item.id}`)}">
                                    <i class="fa-solid fa-pen-to-square" id="editar" tipo="empreendimentos" identificador="${item.id}"></i>
                                </a>
                                <i class="fa-solid fa-trash-can" id="excluir" tipo="empreendimentos" identificador="${item.id}"></i>
                            </td>
                        </tr>`;
                    });

                    exibir_tabela += `</tdoby></table>`;

                    // Atualizar o elemento HTML com a lista de origens
                    if (document.getElementById("listagem-de-empreendimentos") !== null) {
                        const listaOrigens = document.getElementById("listagem-de-empreendimentos");
                        listaOrigens.innerHTML = exibir_tabela;
                        console.log(`Pesquisa de empreendimentos carregada!`);
                    }
                    
                }).catch(e => {
        
                    console.error(`Erro ao carregar a lista de empreendimentos para filtragem!\n Mensagem: ${e}`);
            
                })

            } else {
                
                if (document.getElementById("listagem-de-empreendimentos") !== null) {
                    const listaOrigens = document.getElementById("listagem-de-empreendimentos");
                    listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há empreendimentos cadastrados para este filtro!</p>';
                    console.log(`Não há empreendimentos cadastrados para este filtro!`);
                }

            }
        })
        .catch(e => {
            console.error(`Erro ao tentar contar os registros de empreendimentos cadastrados para filtragem!\n Mensagem: `,e);
        });

    })
    .catch(e=>{
        console.error(`Erro ao tentar listar as solicitações cadastradas para rotular os empreendimentos para filtragem!\n Mensagem: `,e);
    });
}