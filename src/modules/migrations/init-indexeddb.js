export const DB = new Dexie("SiGi2DeskChecking");

export function InitDB() {

  /**
   * Iniciando o bancos na versão 1.0
   * 
   */
  
  DB.version(17).stores({
    funcoes_de_usuarios: `++id, titulo, descricao`,
    usuarios: `++id, nome, &email, &matricula, senha, funcao, status`,
    acessos_de_usuarios: `++id, usuario, pagina`,
    tipo_de_observacao: `++id, titulo, descricao`,
    observacoes: `++id, observacao, tipo, autor, data_de_criacao`,
    origem: `++id, titulo, descricao`,
    tipo_de_solicitacao: `++id, titulo, descricao`,
    status_da_solicitacao: `++id, titulo, descricao`,
    solicitacao: `++id, tipo, origem, status, escopo, empreendimentos, data_de_criacao, receita_pre_definida, prazo_de_execucao, pep, responsavel, local_responsavel, autor`,
    observacoes_da_solicitacao: `solicitacao, observacao`,
    empreendimentos: `++id, solicitacao, titulo, escopo`,
    empreendimentos_da_solicitacao: 'solicitacao, empreendimento',
    projeto_basico: `++id, titulo, descricao, anexo, data_de_publicacao, capex, autor`,
    status_da_reuniao: `++id, titulo, descricao`,
    solicitacao_de_reuniao: `++id, solicitacao, gestor, data_programada, engenheiro_responsavel, data_da_solicitacao, status`,
    reunioes: `++id, solicitacao_de_reuniao, observacao, data_de_realizacao, ata`,
    participantes_da_reuniao: `reuniao, participante`,
    status_visita_tecnica: `++id, titulo, descricao`,
    solicitacao_de_visita_tecnica: `++id, gestor, data_programada, projeto_basico, engenheiro_responsavel, data_da_solicitacao, status`,
    visita_tecnica: `++id, solicitacao, data_da_realizacao, escopo_detalhado, observacao`,
    tipo_de_orcamento: `++id, titulo, descricao`,
    status_de_orcamento: `++id, titulo, descricao`,
    arquivos_de_orcamentos: `++id, titulo, anexo, publicacao, tipo, valor, versao, autor`,
    solicitacao_de_orcamento: `++id, gestor, visita_tecnica, projeto_basico, orcamentista, data_da_solicitacao`,
    agrupamento_de_contratos: `solicitacao_de_orcamento, arquivo_de_orcamento`,
    orcamento_geral: `++id, agrupamento, titulo, data_do_orcamento, status`,
    notas_tecnicas: `++id, solicitacao, anexo, data_da_publicacao, autor`,
    cotacoes: `++id, nota_tecnica, anexo, data_de_publicacao`,
    numeros_de_contrato_sap: '++id, numero, status, autor',
    prestadores_de_servico: '++id, razao_social, cnpj, informacoes, status, autor',
    departamentos: '++id, nome, descricao, status, autor',
    status_do_contrato: '++id, titulo, descricao, tipo',
    contratos: '++id, rotulo, numero_do_contrato, escopo, empresa, responsavel, dep_responsavel, atestado_de_ex_de_serv, art_ct, ordem_de_servico, recebimento_provisorio, data_rec_provisorio, recebimento_definitivo, data_rec_definitivo, data_inicio_execucao, data_termino_execucao, data_de_vigencia, data_base, status, assinatuta, autor, data_de_publicacao, consolidado',
    atividades: '++id, rotulo, local, obra, obra_complemento, classificacao_contabil_pep, tipo_de_autorizacao, escopo, informacoes_complementares, data_inicio_previsto, data_termino_previsto, data_incio_real, data_termino_real, energizacao, gestor, coordenador, responsavel_fiscalizacao, autor, data_de_publicacao',
    locais: '++id, rotulo, descricao, status',
    obras: '++id, rotulo, descricao, status',
    obras_complemento: '++id, rotulo, descricao, status',
    atividades_dos_contratos: '++id, id_contrato, id_atividade',
    observacoes_de_contrato: '++id, id_observacao, id_contrato',
    referencias_aeic: '++id, rotulo, descricao, status',
    categorias_de_aeic: '++id, rotulo, descricao',
    aeic: '++id, contrato, atividade, referencia, mes_ano, valor, reajuste, reajustado, categoria, autor, publicacao'
  });

}