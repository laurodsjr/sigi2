import { DB } from "../init-indexeddb";

export function CadastraObservacao() {

    /**
     * Recebendo os valores dos campos
     */
    let 
        tipo = document.getElementById("tipo").value,
        campoDoTipo,
        titulo,
        tabela,
        id_do_tipo,
        observacao = document.getElementById("texto-observacao").value
    ;

    if (tipo == 'solicitacoes') {
        titulo = 'Solicitação';
        tabela = 'observacoes_da_solicitacao';
        id_do_tipo = 1;
        campoDoTipo = Number(document.getElementById("lista-solicitacoes").value);
    }

    if (tipo == 'contrato') {
        titulo = 'Contrato';
        tabela = 'observacoes_de_contrato';
        id_do_tipo = 3;
        campoDoTipo = Number(document.getElementById("lista-contratos").value);
    }

    let field = document.getElementById(`field-lista-${tipo}`);
    let aviso = field.querySelector('.text-danger');
    
    let fieldTitulo = document.getElementById("field-texto-observacao");
    let aviso2 = fieldTitulo.querySelector('.text-danger');

    if (campoDoTipo == 0 || observacao == '') {

        if (campoDoTipo == 0 && observacao == '') {
            console.error(`Os campos: ${titulo} e Observação são obrigatórios!`); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
        } 
        
        if (campoDoTipo == 0) {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if (observacao == '') {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');

        let timestamp = Date.now();
        let criacao = new Date(timestamp);
        let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));


        let dados_verificados_para_cadastrar = {
            //solicitacao: Number(solicitacao),
            observacao: observacao,
            autor: Number(dados_autor.id),
            tipo: Number(id_do_tipo),
            data_de_criacao: criacao
        };

        //console.log(`Para input:`,dados_verificados_para_cadastrar);
        let link_atual = window.location.href.split('/');
        let monta_url = '';
        for (let contador = 0; contador <= (link_atual.length-2);contador++) {
            monta_url += link_atual[contador]+'/';
        }

        DB.observacoes.add(dados_verificados_para_cadastrar)
        .then((resposta)=>{
            
            let registra
            if (tipo == 'solicitacoes') {
                registra = DB[tabela].add({
                    solicitacao: Number(campoDoTipo),
                    observacao: resposta
                })
            }
            if (tipo == 'contrato') {
                registra = DB[tabela].add({
                    id_observacao: resposta,
                    id_contrato: Number(campoDoTipo)
                })
            }
            registra
            .then(resposta=>{
                console.log(`Observação cadastrada com êxito!`);
                alert(`Tudo certo! Observação cadastrada!`);
                location.href=monta_url+'lista.html?tipo='+tipo;
            })
            .catch((erro)=>{
                console.error(`Erro ao vincular cadastro da observação com a ${titulo}: \n Erro: `,erro);
            })

            
        })
        .catch((erro)=>{
            console.error(`Erro ao cadastrar a observação: \n Erro: `,erro);
        })

    }    

}