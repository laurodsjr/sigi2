export async function CarregaOpcaoPorURL(parametros = {
    pagina: ''
}) {
    const {
        pagina = '',
    } = parametros

    let 
        id_do_vinculo,
        tipo = document.getElementById("tipo").value
    ;

    if (tipo == 'solicitacoes') {
        id_do_vinculo = 'lista-solicitacoes';
    }

    if (tipo == 'contrato') {
        id_do_vinculo = 'lista-contratos';
    }

    if ( pagina == 'incluir.html' ) {
        window.addEventListener('load', () => {

            const parametros_url = new URLSearchParams(window.location.search);
            const identificador = parametros_url.get('id');

            let select = document.getElementById(id_do_vinculo);

            let optionSelecionada = select.querySelector(`option[value="${identificador}"]`);            
            if (optionSelecionada) {
                optionSelecionada.selected = true;
                console.log('TESTE: ',select,identificador)
            }
            
        })
    }
}