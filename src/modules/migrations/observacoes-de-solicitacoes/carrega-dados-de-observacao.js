import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros";
import { DB } from "../init-indexeddb";

export async function CarregaDadosDeObservacao() {

    let 
        tipo = document.getElementById("tipo").value,
        titulo,
        tabela,
        id_do_tipo,
        id_do_vinculo,
        coluna,
        coluna_resposta
    ;

    if (tipo == 'solicitacoes') {
        titulo = 'Solicitação';
        tabela = 'observacoes_da_solicitacao';
        id_do_tipo = 1;
        id_do_vinculo = 'lista-solicitacoes';
        coluna = 'observacao';
        coluna_resposta = 'observacao';
    }

    if (tipo == 'contrato') {
        titulo = 'Contrato';
        tabela = 'observacoes_de_contrato';
        id_do_tipo = 3;
        id_do_vinculo = 'lista-contratos';
        coluna = 'id_observacao';
        coluna_resposta = 'id_contrato'
    }

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = Number(parametros_url.get('id'));

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);
    
    if (grupo == 'observacoes') {
    
        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
    
                DB[tabela].toArray()
                .then(retorno => { 
    
                    function Filtra(id) {
                        return retorno.filter(item => {
                            if (item[coluna] == id) {
                                return `#${AcrescentadorDeZeros(item[coluna],4)}`
                            }
                        });
                    }
    
                    DB.observacoes
                    .count()
                    .then(registros => {
                        if (registros > 0) {
    
                            async function RetornaDados(id) {
                                const retorno = await DB.observacoes.get(Number(id))
                                return retorno
                            }
    
                            RetornaDados(identificador)
                            .then((dados)=>{
                                
                                //let vinculo = FiltraASolicitacao(item.id)[0]?.solicitacao ? `#${AcrescentadorDeZeros(FiltraASolicitacao(item.id)[0]?.solicitacao,4)}` : 'Não disponível';
    
                                let vinculo = Filtra(dados.id)[0][coluna_resposta] ? `#${AcrescentadorDeZeros(Filtra(dados.id)[0][coluna_resposta],4)}` : 'Não disponível';

                                document.getElementById('texto-observacao').value = dados.observacao;
    
                                if (pagina == 'visualizar.html') {
    
                                    // Obtenha uma referência ao elemento select
                                    let selectSolicitacao = document.getElementById(id_do_vinculo);
                                    selectSolicitacao.disabled = true;
                                    // Defina o valor que você deseja selecionar
                                    let solicitacaoDefinida = String(Filtra(dados.id)[0][coluna_resposta]);

                                    let optionSelecionada = selectSolicitacao.querySelector(`option[value="${solicitacaoDefinida}"]`);
                                    if (optionSelecionada) {
                                        optionSelecionada.selected = true;
                                    }
    
                                }
    
                            })
    
                        } else {
                            console.log(`Não há dados para serem exibidos!`);
                        }
                    })
                    .catch(e => {
                        console.error(`Erro ao tentar contar os registros da lista de observações cadastradas!\n Mensagem: `,e);
                    });
    
                })
                .catch(e=>{
                    console.error(`Erro ao tentar listar as solicitações para a observação cadastrada!\n Mensagem: `,e);
                });
    
            })
    
        }

    }

}