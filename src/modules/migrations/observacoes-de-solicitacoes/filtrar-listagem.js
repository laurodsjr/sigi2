import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros";
import { ConverteData } from "../../customizacao/formata-data";
import { MontaLink } from "../../customizacao/monta-link";
import { DB } from "../init-indexeddb";  

export function FiltrarListagemObservacoes(parametros = {
    tipo: '',
    id: 0
}) {

    const {
        tipo = '',
        id = 0
    } = parametros

    let 
        tabela,
        titulo_tabela_view,
        coluna,
        filtragem
    ;

    if (tipo == 'solicitacao' || tipo == '') {
        tabela = 'observacoes_da_solicitacao';
        titulo_tabela_view = 'SOLICITAÇÃO';
        coluna = 'observacao';
        filtragem = 'lista-solicitacoes';
    }

    if (tipo == 'contrato') {
        tabela = 'observacoes_de_contrato';
        titulo_tabela_view = 'CONTRATO';
        coluna = 'id_observacao';
        filtragem = 'lista-contratos';
    }

    let filtro = Number(document.getElementById(filtragem)?.value) || 0,
    texto_filtro = document.getElementById('texto-observacao')?.value || '';

    console.log('Value: ',filtro)

    if (filtro != 0) {

        let aplicando_filtro = tipo == 'solicitacao' ? {
            solicitacao: filtro
        } : tipo == 'contrato' ? {
            id_contrato: filtro
        } : {
            solicitacao: filtro
        }

        DB[tabela]
        .where(aplicando_filtro)
        .toArray()
        .then(retorno => {

            function Filtra(id) {
                return retorno.filter(item => {
                    if (item[coluna] == id) {
                        return `#${AcrescentadorDeZeros(item[coluna],4)}`
                    }
                });
            }      

            function FiltrarObservacoes(parametros = {}) {
                const {
                    texto_filtro = '',
                    id = 0
                } = parametros;
              
                return new Promise(function(resolve, reject) {
                  let consulta = DB.observacoes;
              
                  if (texto_filtro !== '') {
                    consulta = consulta.filter(registro => registro.observacao.includes(texto_filtro));
                  }
              
                  if (id !== 0) {
                    consulta = consulta.filter(registro => registro.id === id);
                  }
              
                  consulta.toArray()
                    .then(observacao => {
                      resolve({
                        registros: observacao.length,
                        dados: observacao
                      });
                    })
                    .catch(erro => {
                      console.error(`Houve um erro ao tentar filtrar as observações! Erro: ${erro}`);
                      resolve({
                        registros: 0,
                        dados: []
                      });
                    });
                });
            }

            FiltrarObservacoe({
                texto_filtro: texto_filtro,
                id: id
            })
            .then(retorno => {
                if (retorno.registros > 0) {
                    FiltrarObservacoe({
                        texto_filtro: texto_filtro,
                        id: id
                    })
                    .then((lista) => {               
                            
                        let exibir_tabela = `<table id="table-list" class="w-100">
                            <thead id="table-title">
                                <tr>
                                    <th class="p-2">ID</th>
                                    <th class="p-2">${titulo_tabela_view}</th>
                                    <th class="p-2 descricoes">OBSERVAÇÃO</th>
                                    <th class="p-2 last">AÇÕES</th>
                                </tr>
                            </thead>
                        <tbody>`;

                        lista.dados.forEach((item, key) => {

                            let solicitacao = Filtra(item.id)[0][coluna] ? `#${AcrescentadorDeZeros(Filtra(item.id)[0][coluna],4)}` : 'Não disponível';

                            exibir_tabela += `<tr class="${key % 2 !== 0 ? 'even-line' : ''}">
                                <td class="${'p-2'}">${item.id}</td>
                                <td class="${'p-2'}">${solicitacao}</td>
                                <td class="${'p-2'} descricoes">${item.observacao ? item.observacao : `-` }</td>
                                <td class="${'p-2 actions'}">
                                    <a href="${MontaLink(`observacoes/visualizar.html?id=${item.id}`)}">
                                        <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="observacoes" identificador="${item.id}"></i>
                                    </a>
                                </td>
                            </tr>`;
                        });

                        exibir_tabela += `</tdoby></table>`;

                        // Atualizar o elemento HTML com a lista de origens
                        if (document.getElementById("listagem-de-observacoes") !== null) {
                            const listaOrigens = document.getElementById("listagem-de-observacoes");
                            listaOrigens.innerHTML = exibir_tabela;
                            console.log(`Lista de observações filtradas carregada!`);
                        }
                        
                    }).catch(e => {
            
                        console.error(`Erro ao carregar a lista filtrada de observações!\n Mensagem: ${e}`);
                
                    })

                } else {
                    
                    if (document.getElementById("listagem-de-observacoes") !== null) {
                        const listaOrigens = document.getElementById("listagem-de-observacoes");
                        listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há observações para este filtro!</p>';
                        console.log(`Não há observações para este filtro!`);
                    }

                }
            })
            .catch(e => {
                console.error(`Erro ao tentar contar os registros de observações relacionados ao filtro!\n Mensagem: `,e);
            });

        })
        .catch(e=>{
            console.error(`Erro ao tentar listar as solicitações cadastradas para rotular as observações filtradas!\n Mensagem: `,e);
        });

    } else {
    
        let aplicando_filtro = tipo == 'solicitacao' ? {
            solicitacao: filtro
        } : tipo == 'contrato' ? {
            id_contrato: filtro
        } : {
            solicitacao: filtro
        }

        DB[tabela]
        .where(aplicando_filtro)
        .toArray()
        .then(retorno => {

            function Filtra(id) {
                return retorno.filter(item => {
                    if (item[coluna] == id) {
                        return `#${AcrescentadorDeZeros(item[coluna],4)}`
                    }
                });
            }            

            DB.observacoes
            .filter(item => item.observacao.includes(texto_filtro))
            .count()
            .then(registros => {
                if (registros > 0) {
                    DB.observacoes
                    .filter(item => item.observacao.includes(texto_filtro))
                    .toArray()
                    .then((lista) => {               
                            
                        let exibir_tabela = `<table id="table-list" class="w-100">
                            <thead id="table-title">
                                <tr>
                                    <th class="p-2">ID</th>
                                    <th class="p-2">SOLICITAÇÃO</th>
                                    <th class="p-2 descricoes">OBSERVAÇÃO</th>
                                    <th class="p-2 last">AÇÕES</th>
                                </tr>
                            </thead>
                        <tbody>`;

                        lista.forEach((item, key) => {

                            let solicitacao = Filtra(item.id)[0][coluna] ? `#${AcrescentadorDeZeros(Filtra(item.id)[0][coluna],4)}` : 'Não disponível';

                            exibir_tabela += `<tr class="${key % 2 !== 0 ? 'even-line' : ''}">
                                <td class="${'p-2'}">${item.id}</td>
                                <td class="${'p-2'}">${solicitacao}</td>
                                <td class="${'p-2'} descricoes">${item.observacao ? item.observacao : `-` }</td>
                                <td class="${'p-2 actions'}">
                                    <a href="${MontaLink(`observacoes/visualizar.html?id=${item.id}`)}">
                                        <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="observacoes" identificador="${item.id}"></i>
                                    </a>
                                </td>
                            </tr>`;
                        });

                        exibir_tabela += `</tdoby></table>`;

                        // Atualizar o elemento HTML com a lista de origens
                        if (document.getElementById("listagem-de-observacoes") !== null) {
                            const listaOrigens = document.getElementById("listagem-de-observacoes");
                            listaOrigens.innerHTML = exibir_tabela;
                            console.log(`Lista de observações filtradas carregada!`);
                        }
                        
                    }).catch(e => {
            
                        console.error(`Erro ao carregar a lista filtrada de observações!\n Mensagem: ${e}`);
                
                    })

                } else {
                    
                    if (document.getElementById("listagem-de-observacoes") !== null) {
                        const listaOrigens = document.getElementById("listagem-de-observacoes");
                        listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há observações para este filtro!</p>';
                        console.log(`Não há observações para este filtro!`);
                    }

                }
            })
            .catch(e => {
                console.error(`Erro ao tentar contar os registros de observações relacionados ao filtro!\n Mensagem: `,e);
            });

        })
        .catch(e=>{
            console.error(`Erro ao tentar listar as solicitações cadastradas para rotular as observações filtradas!\n Mensagem: `,e);
        });   

    }

}