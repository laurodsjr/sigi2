import { DB } from "../init-indexeddb";

export function ListaDeOpcoes(parametros = {
    tipo: ''
}) {
    const {
        tipo = ''
    } = parametros

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    if (tipo == 'contrato') {
        DB.contratos.toArray()
        .then((opcao)=>{
            let exibir_lista = `<option value="0">Escolha o contrato</option>`;

            opcao.forEach((item) => {
                exibir_lista += `<option value="${item.id}" ${identificador == item.id ? 'selected' : ''}>${item.id} - ${item.rotulo}</option>`;
            });

            // Atualizar o elemento HTML com a lista de contratos
            const listaOpcoesDeContrato = document.getElementById('lista-contratos');
            listaOpcoesDeContrato.innerHTML = exibir_lista;
            console.log(`Lista de opções de de contrato para observações carregada!`);
        })
        .catch(erro => console.error(`Houve um erro ao listar opnções de contrato para observações! Erro: ${erro}`));
    }    
}