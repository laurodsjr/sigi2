import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros";
import { ConverteData } from "../../customizacao/formata-data";
import { MontaLink } from "../../customizacao/monta-link";
import { DB } from "../init-indexeddb";  

export function ListagemObservacoes(parametros = {
    tipo: ''
}) {

    const {
        tipo = ''
    } = parametros

    let 
        tabela,
        titulo_tabela_view,
        coluna
    ;

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parseInt(parametros_url.get('id'));
    const tipo_url = parametros_url.get('tipo');


    if (tipo_url == 'solicitacao' || tipo_url == '') {
        tabela = 'observacoes_da_solicitacao';
        titulo_tabela_view = 'SOLICITAÇÃO';
        coluna = 'observacao';
    } else if (tipo_url == 'contrato') {
        tabela = 'observacoes_de_contrato';
        titulo_tabela_view = 'CONTRATO';
        coluna = 'id_observacao';
    }

    function FiltrarContratosPorID() {
        return new Promise(function(resolve, reject) {
            let consulta = DB[tabela];

            if (tipo_url == 'contrato' && identificador > 0) {
                consulta = consulta.filter(registro => registro.id_contrato === identificador);
            }

            consulta.toArray()
            .then(resultado => {
                resolve(resultado);
            })
            .catch(erro => {
                console.error(`Houve um erro ao tentar filtrar os contratos! Erro: ${erro}`);
                reject(erro)
            });
        });
    }

    FiltrarContratosPorID()
    .then(retorno => {

        function Filtra(id) {
            return retorno.filter(item => {
                if (item[coluna] == id) {
                    return `#${AcrescentadorDeZeros(item[coluna],4)}`
                }
            });
        }

        let ids = retorno.map(item => {
            return item[coluna];
        });
        
        DB.observacoes
        .where('id')
        .anyOf(ids)
        .count()
        .then(registros => {


            if (registros > 0) {
                DB.observacoes
                .where('id')
                .anyOf(ids)
                .toArray()
                .then((lista) => {    

                        
                    let exibir_tabela = `<table id="table-list" class="w-100">
                        <thead id="table-title">
                            <tr>
                                <th class="p-2">ID</th>
                                <th class="p-2">${titulo_tabela_view}</th>
                                <th class="p-2 descricoes">OBSERVAÇÃO</th>
                                <th class="p-2 last">AÇÕES</th>
                            </tr>
                        </thead>
                    <tbody>`;

                    lista.forEach((item, key) => {

                        let solicitacao = Filtra(item.id)[0][coluna] ? `#${AcrescentadorDeZeros(Filtra(item.id)[0][coluna],4)}` : 'Não disponível';

                        exibir_tabela += `<tr class="${key % 2 !== 0 ? 'even-line' : ''}">
                            <td class="${'p-2'}">${item.id}</td>
                            <td class="${'p-2'}">${solicitacao}</td>
                            <td class="${'p-2'} descricoes">${item.observacao ? item.observacao : `-` }</td>
                            <td class="${'p-2 actions'}">
                                <a href="${MontaLink(`observacoes/visualizar.html?id=${item.id}&tipo=${tipo}`)}">
                                    <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="observacoes" identificador="${item.id}"></i>
                                </a>
                            </td>
                        </tr>`;
                    });

                    exibir_tabela += `</tdoby></table>`;

                    // Atualizar o elemento HTML com a lista de origens
                    if (document.getElementById("listagem-de-observacoes") !== null) {
                        const listaOrigens = document.getElementById("listagem-de-observacoes");
                        listaOrigens.innerHTML = exibir_tabela;
                        console.log(`Lista de observações carregada!`);
                    }
                    
                }).catch(e => {
        
                    console.error(`Erro ao carregar a lista de observações!\n Mensagem: ${e}`);
            
                })

            } else {
                
                if (document.getElementById("listagem-de-observacoes") !== null) {
                    const listaOrigens = document.getElementById("listagem-de-observacoes");
                    listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há observações cadastradas!</p>';
                    console.log(`Não há observações cadastradas!`);
                }

            }
        })
        .catch(e => {
            console.error(`Erro ao tentar contar os registros de observações cadastrados!\n Mensagem: `,e);
        });

    })
    .catch(e=>{
        console.error(`Erro ao tentar listar as solicitações cadastradas para rotular as observações!\n Mensagem: `,e);
    });
}