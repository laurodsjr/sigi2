export function SelectBaseadoEmTipo(parametros = {
    tipo: '',
    pagina: ''
}) {

    const {
        tipo = '',
        pagina = ''
    } = parametros

    let selecionador;

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = Number(parametros_url.get('id'));

    if (tipo == 'solicitacao') {
        selecionador = `<input type="hidden" id="tipo" value="solicitacao" />
        <div id="field-lista-solicitacao" class="mb-3">
            <div class="d-flex align-items-start">   
                <label for="lista-solicitacoes" class="form-label pt-2 pe-3">Solicitação${pagina === 'incluir.html' ? ` *`: ''}</label>
                <input type="hidden" id="lista-solicitacoes-edit">
                <div class="w-100">
                    <select id="lista-solicitacoes" class="form-select w-100 field-select">
                        <option value="0">Escolha a solicitação</option>
                    </select>
                    <p class="text-danger m-0 d-none">Campo obrigatório</p>
                </div>
            </div>            
        </div>`;
    }

    if (tipo == 'contrato') {
        selecionador = `<input type="hidden" id="tipo" value="contrato" />
        <div id="field-lista-contrato" class="mb-3">
            <div class="d-flex align-items-start">    
                <label for="lista-contratos" class="form-label pt-2 pe-3">Contrato${pagina === 'incluir.html' ? ` *`: ''}</label>
                <input type="hidden" id="lista-contratos-edit">
                <div class="w-100">
                    <select id="lista-contratos" class="form-select w-100 field-select" ${pagina === 'incluir.html' && identificador > 0 ? 'disabled' : ''}>
                        <option value="0">Escolha o contrato</option>
                    </select>
                    <p class="text-danger m-0 d-none">Campo obrigatório</p>
                </div>
            </div>
        </div>`;
    }

    if (document.getElementById('espaco-do-select') !== null) {
        document.getElementById('espaco-do-select').innerHTML = selecionador
    }

}