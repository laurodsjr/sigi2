import { DB } from "../init-indexeddb";

export function CarregaTiposDeObservacoes() {
    DB.tipo_de_observacao.count()
    .then(registros => {
        if (registros < 1) {
            DB.tipo_de_observacao.bulkAdd([
                {titulo: 'Solicitação', descricao: ''},
                {titulo: 'Orçamento', descricao: ''},
                {titulo: 'Contrato', descricao: ''}
            ]).then((e) => {
    
                console.log(`Tipos de observações carregados!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar tipos de observações! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em tipos de observações!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de tipos de observações!`)
    });
}