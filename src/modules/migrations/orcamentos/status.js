import { DB } from "../init-indexeddb";

export function StatusDeOrcamento() {
    DB.status_de_orcamento.count()
    .then(registros => {
        if (registros < 1) {
            DB.status_de_orcamento.bulkAdd([
                {titulo: 'Novo', descricao: 'Opção padrão'},
                {titulo: 'Em análise', descricao: ''},
                {titulo: 'Aprovado', descricao: ''},
                {titulo: 'Rejeitado', descricao: ''}
            ]).then((e) => {
    
                console.log(`Status para orçamentos carregadas!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar status para orçamentos! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em status para orçamentos!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de status para orçamentos!`);
    });
}