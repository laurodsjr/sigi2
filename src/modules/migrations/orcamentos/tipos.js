import { DB } from "../init-indexeddb";

export function TiposDeOrcamento() {
    DB.tipo_de_orcamento.count()
    .then(registros => {
        if (registros < 1) {
            DB.tipo_de_orcamento.bulkAdd([
                {titulo: 'Mão de obra', descricao: ''},
                {titulo: 'Equipamento', descricao: ''}
            ]).then((e) => {
    
                console.log(`Tipos de orçamentos carregadas!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar tipos de orçamentos! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em tipos de orçamentos!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de tipos de orçamentos!`);
    });
}