import { DB } from "../init-indexeddb";

export async function CadastraReuniao() {

    /**
     * Recebendo os valores dos campos
     */
    let 
    solicitacao         = Number(document.getElementById("lista-solicitacoes-de-reuniao").value) || 0,
    participantes       = document.getElementById("lista-participantes").value,
    resumo              = document.getElementById("resumo-reuniao").value,
    data_da_reuniao     = document.getElementById("data-da-reuniao").value,
    input_ata           = document.getElementById('ata-reuniao'),
    ata                 = input_ata.files[0];

    // Verifique se um arquivo PDF foi selecionado
    if (ata && ata.type === 'application/pdf') {
        try {
            // Converta o arquivo PDF em um array de bytes (Uint8Array)
            const arrayBuffer = await ata.arrayBuffer();
            const byteArray = new Uint8Array(arrayBuffer);
          

            let fieldSolicitacao = document.getElementById("field-lista-solicitacoes-de-reuniao");
            let aviso = fieldSolicitacao.querySelector('.text-danger');
            
            let field_participantes = document.getElementById("field-lista-participantes");
            let aviso2 = field_participantes.querySelector('.text-danger');

            let field_data_da_reuniao = document.getElementById("field-data-da-reuniao");
            let aviso3 = field_data_da_reuniao.querySelector('.text-danger');

            if (solicitacao == 0 || participantes.length == 0 || data_da_reuniao == '') {

                if (solicitacao == 0 && participantes.length == 0 && data_da_reuniao == '') {
                    console.error('Os campos: Solicitação, Participantes e Data da Reunião, são obrigatórios!'); 
                    aviso.classList.remove('d-none');
                    aviso2.classList.remove('d-none');
                    aviso3.classList.remove('d-none');
                } 
                
                if (solicitacao == '') {
                    aviso.classList.remove('d-none');
                } else {
                    aviso.classList.add('d-none');
                } 
                
                if (participantes.length == 0) {  
                    aviso2.classList.remove('d-none');
                } else {
                    aviso2.classList.add('d-none');
                } 
                
                if (data_da_reuniao == '') {
                    aviso3.classList.remove('d-none');
                } else {
                    aviso3.classList.add('d-none');
                }

            } else {

                aviso.classList.add('d-none');
                aviso2.classList.add('d-none');
                aviso3.classList.add('d-none');

                let timestamp = Date.now();
                let criacao = new Date(timestamp);
                let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));

                const dados_da_sessao = JSON.parse(sessionStorage.getItem("usuario_autenticado"));
                const { id } = dados_da_sessao;

                let dados_verificados_para_cadastrar = {
                    solicitacao: solicitacao,
                    gestor: dados_autor?.id,
                    data_programada: data_da_reuniao,
                    engenheiro_responsavel: id,
                    data_da_solicitacao: criacao,
                    ata: ata.name,
                    status: 1
                };

                //console.log(`Para input:`,dados_verificados_para_cadastrar);
                let link_atual = window.location.href.split('/');
                let monta_url = '';
                for (let contador = 0; contador <= (link_atual.length-2);contador++) {
                    monta_url += link_atual[contador]+'/';
                }

                console.log('Output: ',dados_verificados_para_cadastrar);

                /*DB.solicitacao_de_reuniao.add(dados_verificados_para_cadastrar)
                .then((resposta)=>{
                    console.log(`Solicitação cadastrada com éxito!`);
                    alert(`Tudo certo! Solicitação cadastrada!`);
                    location.href=monta_url+'lista.html';
                })
                .catch((erro)=>{
                    console.error(`Erro ao cadastrar a solicitação: \n Erro: `,erro);
                })*/

            } 

          // Limpe o campo de entrada
          input_ata.value = '';

        } catch (error) {
            console.error('Houve um erro! Mensagem: ',error);
        }
    }

       

}