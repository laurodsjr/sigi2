import { DB } from "../init-indexeddb";

export function CarregaParticipantes() {

    if (document.getElementById("lista-participantes") !== null) {

        // Checa se existem solicitações
        DB.usuarios.count()
        .then(registros => {
            if (registros > 0) {
                DB.usuarios.toArray()
                .then((opcao) => { 

                    //console.log('Solicitações: ',opcao)

                    let exibir_lista = ``;
                    opcao.forEach((item) => {
                        exibir_lista += `<option value="${item.id}">${item.nome}</option>`;
                    });

                    // Atualizar o elemento HTML com a lista de origens
                    const listaOrigens = document.getElementById("lista-participantes");
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de opções de participantes carregada!`);
                })
                .catch(erro=>console.error(`Houve um erro ao tentar listar as opções de participantes disposníveis! \n Erro: ${erro}`));
            } else {
                console.log(`Não há opções de participantes para listar!`);
            }
        })
        .catch(erro=>console.error(`Houve um erro ao tentar contar as opções de participantes disposníveis! \n Erro: ${erro}`));
    }

}