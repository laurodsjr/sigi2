import { DB } from "../init-indexeddb";

export function StatusDeReunioes() {
    DB.status_visita_tecnica.count()
    .then(registros => {
        if (registros < 1) {
            DB.status_visita_tecnica.bulkAdd([
                {titulo: 'Programada', descricao: 'Padrão inicial'},
                {titulo: 'Adiada', descricao: ''},
                {titulo: 'Realizada', descricao: ''},
                {titulo: 'Atrasada', descricao: ''}
            ]).then((e) => {
    
                console.log(`Status para visitas técnicas carregadas!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar status para visitas técnicas! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em status para visitas técnicas!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de status para visitas técnicas!`);
    });
}