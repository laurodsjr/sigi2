import { DB } from "../init-indexeddb";

export async function CarregaDadosDeSolicitacaoDeReuniao() {

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);

    if (grupo == 'solicitacoes-de-reuniao') {

        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
    
                DB.tipo_de_solicitacao.toArray()
                .then(tipos => {

                    function FiltraOTipo(id) {
                        return tipos.filter(tipo => {
                            if (tipo.id == id) {
                                return tipo.titulo
                            }
                        });
                    }
                
                    DB.solicitacao_de_reuniao
                    .count()
                    .then(registros => {
                        if (registros > 0) {

                            async function RetornaDados(id) {
                                const retorno = await DB.solicitacao_de_reuniao.get(Number(id))
                                return retorno
                            }

                            RetornaDados(identificador)
                            .then((dados)=>{


                                if (pagina == 'visualizar.html') {
                                    //document.getElementById('lista-origens').value = origem;
                                    //document.getElementById('lista-documentos').value = tipo;

                                    document.getElementById('lista-solicitacoes').value = dados.solicitacao;
                                    document.getElementById('lista-engenheiros').value = dados.engenheiro_responsavel;
                                    //document.getElementById('prazo').value = dados.prazo_de_execucao;
                                } else {
                                    document.getElementById('identificador').value = identificador;

                                    // Obtenha uma referência ao elemento select
                                    let selectSolicitacao = document.getElementById('lista-solicitacoes');
                                    // Defina o valor que você deseja selecionar
                                    let solicitacaoDefinida = dados.solicitacao;
                                    // Percorra as opções para encontrar o valor correspondente
                                    /*for (let i = 0; i < selectSolicitacao.options.length; i++) {
                                        if (selectSolicitacao.options[i].value === solicitacaoDefinida) {
                                            // Atribua o valor encontrado ao elemento select
                                            //selectSolicitacao.value = solicitacaoDefinida;
                                            selectSolicitacao.selectedIndex = -1;
                                            break;
                                        }
                                    }*/
                                    document.getElementById('lista-solicitacoes').value = dados.solicitacao;
                                    document.getElementById('lista-solicitacoes-edit').value = dados.solicitacao;

                                    // Obtenha uma referência ao elemento select
                                    let selectEngenheiro = document.getElementById('lista-engenheiros');
                                    // Defina o valor que você deseja selecionar
                                    let engenheiroDefinido = dados.engenheiro_responsavel;
                                    // Percorra as opções para encontrar o valor correspondente
                                    for (let i = 0; i < selectEngenheiro.options.length; i++) {
                                        if (selectEngenheiro.options[i].value === engenheiroDefinido) {
                                            // Atribua o valor encontrado ao elemento select
                                            selectEngenheiro.value = engenheiroDefinido;
                                            //selectEngenheiro.selectedIndex = -1;
                                            break;
                                        }
                                    }
                                    document.getElementById('lista-engenheiros').value = dados.engenheiro_responsavel;
                                }

                                document.getElementById('data-programada').value = dados.data_programada;

                            })

                        } else {
                            console.log(`Não há dados para serem exibidos!`);
                        }
                    })
                    .catch(e => {
                        console.error(`Erro ao tentar contar os registros da lista de solicitações cadastradas!\n Mensagem: `,e);
                    });

                })
                .catch(e => {
                    console.error(`Erro ao tentar listar os tipos de documentos para as solicitações cadastradas!\n Mensagem: `,e);
                });
            })
    
        }

    }

}