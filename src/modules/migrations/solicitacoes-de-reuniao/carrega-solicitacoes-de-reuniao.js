import { DB } from "../init-indexeddb";

/**
 * Níveis de acess
 * 
 *  1: Administrador
 *  2: Gestor
 *  3: Engenheiro
 *  4: Gerente
 */

export function CarregaSolicitacoesDeReuniao() {

    if (document.getElementById("lista-solicitacoes-de-reuniao") !== null) {

        const dados_da_sessao = JSON.parse(sessionStorage.getItem("usuario_autenticado"));
        const { id, funcao } = dados_da_sessao;
        
        if (funcao === 1) {
            // Checa se existem solicitações de reuniões com status 1 : Programada
            DB.solicitacao_de_reuniao
            .where('status').equals(1)
            .count()
            .then(registros => {
                if (registros > 0) {
                    DB.solicitacao_de_reuniao
                    .where('status').equals(1)
                    .toArray()
                    .then((opcao) => { 

                        //console.log('Solicitações: ',opcao)

                        let exibir_lista = `<option value="0">Escolha a solicitação da reunião</option>`;
                        opcao.forEach((item) => {
                            exibir_lista += `<option value="${item.id}">#${item.id} por Gestor: ${item.gestor}</option>`;
                        });

                        // Atualizar o elemento HTML com a lista de origens
                        const listaOrigens = document.getElementById("lista-solicitacoes-de-reuniao");
                        listaOrigens.innerHTML = exibir_lista;
                        console.log(`Lista de solicitações de reunião carregada!`);
                    })
                    .catch(erro=>console.error(`Houve um erro ao tentar listar as solicitações de reunião disposníveis! \n Erro: ${erro}`));
                } else {
                    console.log(`Não há opções de solicitações de reunião para listar!`);
                }
            })
            .catch(erro=>console.error(`Houve um erro ao tentar contar as solicitações de reunião disposníveis! \n Erro: ${erro}`));
        }

        if (funcao === 3) {
            // Checa se existem solicitações de reuniões com status 1 : Programada
            DB.solicitacao_de_reuniao
            .where('status').equals(1)
            .and((registro) => registro.engenheiro_responsavel === id)
            .count()
            .then(registros => {
                if (registros > 0) {
                    DB.solicitacao_de_reuniao
                    .where('status').equals(1)
                    .and((registro) => registro.engenheiro_responsavel === id)
                    .toArray()
                    .then((opcao) => { 

                        //console.log('Solicitações: ',opcao)

                        let exibir_lista = `<option value="0">Escolha a solicitação da reunião</option>`;
                        opcao.forEach((item) => {
                            exibir_lista += `<option value="${item.id}">#${item.id} por Gestor: ${item.gestor}</option>`;
                        });

                        // Atualizar o elemento HTML com a lista de origens
                        const listaOrigens = document.getElementById("lista-solicitacoes-de-reuniao");
                        listaOrigens.innerHTML = exibir_lista;
                        console.log(`Lista de solicitações de reunião carregada!`);
                    })
                    .catch(erro=>console.error(`Houve um erro ao tentar listar as solicitações de reunião disposníveis! \n Erro: ${erro}`));
                } else {
                    console.log(`Não há opções de solicitações de reunião para listar!`);
                }
            })
            .catch(erro=>console.error(`Houve um erro ao tentar contar as solicitações de reunião disposníveis! \n Erro: ${erro}`));
        }

    }

}