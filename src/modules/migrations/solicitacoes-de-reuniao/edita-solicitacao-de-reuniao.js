import { DB } from "../init-indexeddb";

export function EditaSolicitacaoDeReuniao() {

    /**
     * Recebendo os valores dos campos
     */
    let 
    identificador       = Number(document.getElementById("identificador").value) || 0,
    solicitacao         = Number(document.getElementById("lista-solicitacoes").value) || 0,
    engenheiro          = document.getElementById("lista-engenheiros").value,
    data_programada     = document.getElementById("data-programada").value;

    let fieldSolicitacao = document.getElementById("field-solicitacoes");
    let aviso = fieldSolicitacao.querySelector('.text-danger');
    
    let field_engenheiro = document.getElementById("field-engenheiros");
    let aviso2 = field_engenheiro.querySelector('.text-danger');

    let field_data_programada = document.getElementById("field-data-programada");
    let aviso3 = field_data_programada.querySelector('.text-danger');

    if (solicitacao == 0 || engenheiro == 0 || data_programada == '') {

        if (solicitacao == 0 && engenheiro == 0 && data_programada == '') {
            console.error('Os campos: Solicitação, Engenheiro Responsável e Data programada, são obrigatórios!'); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
            aviso3.classList.remove('d-none');
        } 
        
        if (solicitacao == '') {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if (engenheiro == 0) {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 
        
        if (data_programada == '') {
            aviso3.classList.remove('d-none');
        } else {
            aviso3.classList.add('d-none');
        }

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');
        aviso3.classList.add('d-none');

        let timestamp = Date.now();
        let criacao = new Date(timestamp);
        let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));

        let dados_verificados_para_cadastrar = {
            solicitacao: solicitacao,
            gestor: dados_autor?.id,
            data_programada: data_programada,
            engenheiro_responsavel: Number(engenheiro),
            data_da_solicitacao: criacao,
            status: 1
        };

        let link_atual = window.location.href.split('/');
        let monta_url = '';
        for (let contador = 0; contador <= (link_atual.length-2);contador++) {
            monta_url += link_atual[contador]+'/';
        }

        DB.solicitacao_de_reuniao.update(Number(identificador),dados_verificados_para_cadastrar)
        .then((resposta)=>{
            console.log(`Solicitação de reunião atualizada com éxito!`);
            alert(`Tudo certo! Solicitação de reunião atualizada!`);
            location.href=monta_url+'lista.html';
        })
        .catch((erro)=>{
            console.error(`Erro ao atualizar a solicitação de reunião: \n Erro: `,erro);
        })

    }    

}