import { DB } from "../init-indexeddb";
import { ListagemSolicitacoesDeReunioes } from "./listagem";

export function ExcluirSolicitacaoDeReuniao(id = 0) {
    
    let confirmacao = confirm('Você tem certeza que deseja excluir esta solicitação de reunião?');

    if (confirmacao) {
        DB.solicitacao_de_reuniao.delete(Number(id))
        .then(resposta => { 
            alert(`Solicitação de reunião ${id} excluida com sucesso!`);
            console.log(`Solicitação de reunião ${id} excluida com sucesso!`);
            ListagemSolicitacoesDeReunioes();
        })
        .catch(e => {
            console.error(`Houve um erro ao tentar remover esta solicitação de reunião!\n Erro: `,e);
        })
    } else {
        console.log('Operação cancelada pelo usuário!');
    }

}