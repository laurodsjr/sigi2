import { AcrescentadorDeZeros } from "../../customizacao/acrescer-zeros";
import { ConverteData } from "../../customizacao/formata-data";
import { MontaLink } from "../../customizacao/monta-link";
import { DB } from "../init-indexeddb";  

export function ListagemSolicitacoesDeReunioes() {

    DB.usuarios.toArray()
    .then(usuario => {

        function MostraONomeDoUsuario(id) {
            return usuario.filter(item => {
                if (item.id == id) {
                    return item?.nome
                }
            });
        }

        DB.solicitacao.toArray()
        .then(solicitacao => {

            function FiltraASolicitacao(id) {
                return solicitacao.filter(item => {
                    if (item.id == id) {
                        return item?.titulo || `#${AcrescentadorDeZeros(item?.id,4)}`
                    }
                });
            }
        
            DB.solicitacao_de_reuniao
            .count()
            .then(registros => {
                if (registros > 0) {
                    DB.solicitacao_de_reuniao
                    .toArray()
                    .then((lista) => {               
                        
                        let exibir_tabela = `<table id="table-list" class="w-100">
                            <thead id="table-title">
                                <tr>
                                    <th class="p-2">IDENTIFICADOR</th>
                                    <th class="p-2">SOLICITAÇÃO DE REUNIÃO</th>
                                    <th class="p-2">GESTOR</th>
                                    <th class="p-2">PROGRAMADO PARA</th>
                                    <th class="p-2">ENG. RESPONSÁVEL</th>
                                    <th class="p-2 last">AÇÕES</th>
                                </tr>
                            </thead>
                        <tbody>`;

                        lista.forEach((item, key) => {

                            let solicitacao = FiltraASolicitacao(item.solicitacao)[0]?.id ? `#${AcrescentadorDeZeros(FiltraASolicitacao(item.solicitacao)[0]?.id,4)}` : item.solicitacao;

                            exibir_tabela += `<tr">
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.id}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${solicitacao}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${MostraONomeDoUsuario(item.gestor)[0]?.nome ? MostraONomeDoUsuario(item.gestor)[0]?.nome : item.gestor}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.data_programada ? ConverteData(item.data_programada) : `-`}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${MostraONomeDoUsuario(item.engenheiro_responsavel)[0]?.nome ? MostraONomeDoUsuario(item.engenheiro_responsavel)[0]?.nome : `-` }</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2 actions' : 'p-2 actions'}">
                                    <a href="${MontaLink(`solicitacoes-de-reuniao/visualizar.html?id=${item.id}`)}">
                                        <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="solicitacoes" identificador="${item.id}"></i>
                                    </a>
                                    <a href="${MontaLink(`solicitacoes-de-reuniao/editar.html?id=${item.id}`)}">
                                        <i class="fa-solid fa-pen-to-square" id="editar" tipo="solicitacoes" identificador="${item.id}"></i>
                                    </a>
                                    <i class="fa-solid fa-trash-can" id="excluir" tipo="solicitacoes-de-reuniao" identificador="${item.id}"></i>
                                </td>
                            </tr>`;
                        });

                        exibir_tabela += `</tdoby></table>`;

                        // Atualizar o elemento HTML com a lista de origens
                        if (document.getElementById("listagem-de-solicitacoes-de-reuniao") !== null) {
                            const listaOrigens = document.getElementById("listagem-de-solicitacoes-de-reuniao");
                            listaOrigens.innerHTML = exibir_tabela;
                            console.log(`Lista de solicitações de reuniões carregada!`);
                        }
                        
                    }).catch(e => {
            
                        console.error(`Erro ao carregar a lista de solicitações de reuniões!\n Mensagem: ${e}`);
                
                    })

                } else {
                    
                    if (document.getElementById("listagem-de-solicitacoes-de-reuniao") !== null) {
                        const listaOrigens = document.getElementById("listagem-de-solicitacoes-de-reuniao");
                        listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há solicitações de reuniões cadastradas!</p>';
                        console.log(`Não há solicitações cadastradas de reuniões!`);
                    }

                }
            })
            .catch(e => {
                console.error(`Erro ao tentar contar os registros da lista de solicitações de reuniões cadastradas!\n Mensagem: `,e);
            });

        })
        .catch(e => {
            console.error(`Erro ao tentar listar os tipos de documentos para as solicitações de reunião cadastradas!\n Mensagem: `,e);
        });

    })
    .catch(e=>{
        console.error(`Erro ao tentar listar os usuários para mostrar nomes relacionados as solicitações de reunião cadastradas!\n Mensagem: `,e);
    })
    
}