import { DB } from "../init-indexeddb";

export async function CarregaDadosDeSolicitacao() {

    const parametros_url = new URLSearchParams(window.location.search);
    const identificador = parametros_url.get('id');

    let link_atual = window.location.pathname.split('/');
    let grupo = link_atual.at(-2);
    let pagina = link_atual.at(-1);

    if (grupo == 'solicitacoes') {

        if (pagina == 'visualizar.html' || pagina == 'editar.html') {

            document.addEventListener('DOMContentLoaded', () => {
    
                DB.origem.toArray()
                .then(origens => {
    
                    function FiltraOTitulo(id) {
                        return origens.filter(origem => {
                            if (origem.id == id) {
                                return origem.titulo
                            }
                        });
                    }
    
                    DB.tipo_de_solicitacao.toArray()
                    .then(tipos => {
    
                        function FiltraOTipo(id) {
                            return tipos.filter(tipo => {
                                if (tipo.id == id) {
                                    return tipo.titulo
                                }
                            });
                        }
                    
                        DB.solicitacao.count()
                        .then(registros => {
                            if (registros > 0) {
    
                                async function RetornaDados(id) {
                                    const retorno = await DB.solicitacao.get(Number(id))
                                    return retorno
                                }
    
                                RetornaDados(identificador)
                                .then((dados)=>{
                                    
                                    let origem = FiltraOTitulo(Number(dados.origem))[0]?.titulo ? FiltraOTitulo(Number(dados.origem))[0]?.titulo : dados.origem;
                                    let tipo = FiltraOTipo(Number(dados.tipo))[0]?.titulo ? FiltraOTipo(Number(dados.tipo))[0]?.titulo : dados.tipo;
    
                                    document.getElementById('escopo').value = dados.escopo;
                                    document.getElementById('pep').value = dados.pep;
                                    document.getElementById('moeda').value = dados.receita_pre_definida;
                                    document.getElementById('prazo').value = dados.prazo_de_execucao;
    
                                    if (pagina == 'visualizar.html') {
                                        document.getElementById('lista-origens').value = origem;
                                        document.getElementById('lista-documentos').value = tipo;
                                    } else {
                                        document.getElementById('identificador').value = identificador;
    
                                        // Obtenha uma referência ao elemento select
                                        let selectOrigem = document.getElementById('lista-origens');
                                        // Defina o valor que você deseja selecionar
                                        let origemDefinida = dados.origem;
                                        // Percorra as opções para encontrar o valor correspondente
                                        /*for (let i = 0; i < selectOrigem.options.length; i++) {
                                            if (selectOrigem.options[i].value === origemDefinida) {
                                                // Atribua o valor encontrado ao elemento select
                                                selectOrigem.value = origemDefinida;
                                                //selectOrigem.selectedIndex = -1;
                                                break;
                                            }
                                        }*/
                                        document.getElementById('lista-de-documentos-edit').value = dados.origem;
    
                                        // Obtenha uma referência ao elemento select
                                        let selectTipo = document.getElementById('lista-documentos');
                                        // Defina o valor que você deseja selecionar
                                        let origemTipo = dados.tipo;
                                        // Percorra as opções para encontrar o valor correspondente
                                        /*for (let i = 0; i < selectTipo.options.length; i++) {
                                            if (selectTipo.options[i].value === origemTipo) {
                                                // Atribua o valor encontrado ao elemento select
                                                selectTipo.value = origemTipo;
                                                //selectTipo.selectedIndex = -1;
                                                break;
                                            }
                                        }*/
                                        document.getElementById('lista-origens-edit').value = dados.tipo;

                                        document.getElementById('lista-origens').value = dados.origem;
                                        document.getElementById('lista-documentos').value = dados.tipo;
                                    }
    
                                })
    
                            } else {
                                console.log(`Não há dados para serem exibidos!`);
                            }
                        })
                        .catch(e => {
                            console.error(`Erro ao tentar contar os registros da lista de solicitações cadastradas!\n Mensagem: `,e);
                        });
    
                    })
                    .catch(e => {
                        console.error(`Erro ao tentar listar os tipos de documentos para as solicitações cadastradas!\n Mensagem: `,e);
                    });
    
                })
                .catch(e=>{
                    console.error(`Erro ao tentar listar as origens para as solicitações cadastradas!\n Mensagem: `,e);
                });
    
            })
    
        }

    }

}