import { DB } from "../init-indexeddb";

export function CarregaOrigensDeSolicitacoes() {
    DB.origem.count()
    .then(registros => {
        //console.log('Teste: ',registros)
        if (registros > 1) {
            DB.origem.toArray()
            .then((lista) => {
                //console.log('Lista: ',lista)
                let exibir_lista = `<option value="0">Escolha a origem</option>`;
                lista.forEach((item) => {
                    exibir_lista += `<option value="${item.id}">${item.titulo}</option>`;
                });

                // Atualizar o elemento HTML com a lista de origens
                if (document.getElementById("lista-origens") !== null) {
                    const listaOrigens = document.getElementById("lista-origens");
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de origens carregada!`);
                }
            }).catch(e => {
    
                console.error(`Erro ao carregar a lista de origens! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Não há origens cadastradas!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros da lista de origens cadastradas!`);
    });
}