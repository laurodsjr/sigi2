import { DB } from "../init-indexeddb";

export function CarregaStatusDeSolicitacoes() {
    DB.status_da_solicitacao.count()
    .then(registros => {
        //console.log('Teste: ',registros)
        if (registros > 1) {
            DB.status_da_solicitacao.toArray()
            .then((lista) => {
                //console.log('Lista: ',lista)
                let exibir_lista = `<option value="0">Escolha o status</option>`;
                lista.forEach((item) => {
                    exibir_lista += `<option value="${item.id}">${item.titulo}</option>`;
                });

                // Atualizar o elemento HTML com a lista de origens
                if (document.getElementById("lista-status-solicitacoes") !== null) {
                    const listaOrigens = document.getElementById("lista-status-solicitacoes");
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de status de solicitação carregada!`);
                }
            }).catch(e => {
    
                console.error(`Erro ao carregar a lista de status de solicitações! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Não há status de solicitações cadastrados!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros da lista de status de solicitação cadastrados!`);
    });
}