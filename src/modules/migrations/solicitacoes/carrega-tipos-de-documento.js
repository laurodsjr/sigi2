import { DB } from "../init-indexeddb";

export function CarregaTiposDeDocSolicitacoes() {
    DB.tipo_de_solicitacao.count()
    .then(registros => {
        //console.log('Teste: ',registros)
        if (registros > 1) {
            DB.tipo_de_solicitacao.toArray()
            .then((lista) => {
                //console.log('Lista: ',lista)
                let exibir_lista = `<option value="0">Escolha o tipo de documento</option>`;
                lista.forEach((item) => {
                    exibir_lista += `<option value="${item.id}">${item.titulo}</option>`;
                });

                // Atualizar o elemento HTML com a lista de origens
                if (document.getElementById("lista-documentos") !== null) {
                    const listaOrigens = document.getElementById("lista-documentos");
                    listaOrigens.innerHTML = exibir_lista;
                    console.log(`Lista de tipos de documentos carregada!`);
                }
            }).catch(e => {
    
                console.error(`Erro ao carregar a lista de documentos! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Não há lista de de documentos para carregar!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros da lista de de documentos cadastrados!`);
    });
}