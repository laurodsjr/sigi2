import { DB } from "../init-indexeddb";

export function EditaSolicitacao() {

    /**
     * Recebendo os valores dos campos
     */
    let 
    identificador = document.getElementById("identificador").value,
    origem = document.getElementById("lista-origens-edit").value,
    lista_documentos = document.getElementById("lista-de-documentos-edit").value,
    escopo = document.getElementById("escopo").value,
    pep = document.getElementById("pep").value,
    moeda = document.getElementById("moeda").value,
    prazo = document.getElementById("prazo").value;

    let fieldOrigem = document.getElementById("field-origem");
    let aviso = fieldOrigem.querySelector('.text-danger');
    
    let fiel_doc = document.getElementById("field-tipo");
    let aviso2 = fiel_doc.querySelector('.text-danger');

    let field_escopo = document.getElementById("field-escopo");
    let aviso3 = field_escopo.querySelector('.text-danger');

    if (origem == '' || lista_documentos == '' || escopo == '') {

        if (origem == '' && lista_documentos == '' && escopo == '') {
            console.error('Os campos: Origem, Tipo de documento e Escopo, são obrigatórios!'); 
            aviso.classList.remove('d-none');
            aviso2.classList.remove('d-none');
            aviso3.classList.remove('d-none');
        } 
        
        if (origem == '') {
            aviso.classList.remove('d-none');
        } else {
            aviso.classList.add('d-none');
        } 
        
        if (lista_documentos == '') {  
            aviso2.classList.remove('d-none');
        } else {
            aviso2.classList.add('d-none');
        } 
        
        if (escopo == '') {
            aviso3.classList.remove('d-none');
        } else {
            aviso3.classList.add('d-none');
        }

    } else {

        aviso.classList.add('d-none');
        aviso2.classList.add('d-none');
        aviso3.classList.add('d-none');

        let timestamp = Date.now();
        let criacao = new Date(timestamp);
        let dados_autor = JSON.parse(sessionStorage.getItem('usuario_autenticado'));


        let dados_verificados_para_cadastrar = {
            tipo: Number(lista_documentos),
            origem: Number(origem),
            status: 1,
            data_de_criacao: criacao,
            receita_pre_definida: moeda,
            prazo_de_execucao: prazo,
            pep: pep,
            autor: dados_autor.matricula,
            escopo: escopo
        };

        //console.log(`Para input:`,dados_verificados_para_cadastrar);
        let link_atual = window.location.href.split('/');
        let monta_url = '';
        for (let contador = 0; contador <= (link_atual.length-2);contador++) {
            monta_url += link_atual[contador]+'/';
        }

        DB.solicitacao.update(Number(identificador),dados_verificados_para_cadastrar)
        .then((resposta)=>{
            console.log(`Solicitação atualizada com éxito!`,`#ID: ${Number(identificador)}`,dados_verificados_para_cadastrar);
            alert(`Tudo certo! Solicitação atualizada!`);
            location.href=monta_url+'lista.html';
        })
        .catch((erro)=>{
            console.error(`Erro ao atualizar a solicitação: \n Erro: `,erro);
        })

    }    

}