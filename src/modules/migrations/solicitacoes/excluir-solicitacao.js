import { DB } from "../init-indexeddb";
import { ListagemSolicitacoes } from "./listagem";

export function ExcluirSolicitacao(id = 0) {
    
    let confirmacao = confirm('Você tem certeza que deseja excluir esta solicitação?');

    if (confirmacao) {
        DB.solicitacao.delete(Number(id))
        .then(resposta => { 
            alert(`Solicitação ${id} excluida com sucesso!`);
            console.log(`Solicitação ${id} excluida com sucesso!`);
            ListagemSolicitacoes();
        })
        .catch(e => {
            console.error(`Houve um erro ao tentar remover esta solicitação!\n Erro: `,e);
        })
    } else {
        console.log('Operação cancelada pelo usuário!');
    }

}