import { ConverteData } from "../../customizacao/formata-data";
import { MontaLink } from "../../customizacao/monta-link";
import { DB } from "../init-indexeddb";  

export function FiltrarListagemSolicitacoes() {

    let origem_filtro = Number(document.getElementById('lista-origens')?.value) || 0;
    let status_filtro = Number(document.getElementById('lista-status-solicitacoes')?.value) || 1;
    let pep_filtro = document.getElementById('pep')?.value || '';

    DB.origem.toArray()
    .then(origens => {

        function FiltraOTitulo(id) {
            return origens.filter(origem => {
                if (origem.id == id) {
                    return origem.titulo
                }
            });
        }

        DB.tipo_de_solicitacao.toArray()
        .then(tipos => {

            function FiltraOTipo(id) {
                return tipos.filter(tipo => {
                    if (tipo.id == id) {
                        return tipo.titulo
                    }
                });
            }
        
            async function Registros() {

                let contador = await DB.solicitacao.where({
                    origem: origem_filtro,
                    status: status_filtro,
                    pep: pep_filtro
                }).count();

                return contador;

            }
            
            
            Registros()            
            .then(registros => {

                if (registros > 0) {

                    DB.solicitacao.where({
                        origem: origem_filtro,
                        status: status_filtro,
                        pep: pep_filtro
                    }).toArray()
                    .then((lista) => {     
                        
                        let exibir_tabela = `<table id="table-list" class="w-100">
                            <thead id="table-title">
                                <tr>
                                    <th class="p-2">IDENTIFICADOR</th>
                                    <th class="p-2">ORIGEM</th>
                                    <th class="p-2">DOCUMENTO</th>
                                    <th class="p-2">PRAZO</th>
                                    <th class="p-2">RECEITA PRÉ-DEFINIDA</th>
                                    <th class="p-2">P.E.P.</th>
                                    <th class="p-2 last">AÇÕES</th>
                                </tr>
                            </thead>
                        <tbody>`;

                        lista.forEach((item, key) => {

                            let origem = FiltraOTitulo(item.origem)[0]?.titulo ? FiltraOTitulo(item.origem)[0]?.titulo : item.origem;
                            let tipo = FiltraOTipo(item.tipo)[0]?.titulo ? FiltraOTipo(item.tipo)[0]?.titulo : item.tipo;

                            exibir_tabela += `<tr">
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.id}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${origem}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${tipo}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.prazo_de_execucao ? ConverteData(item.prazo_de_execucao) : `-`}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.receita_pre_definida ? item.receita_pre_definida : `-` }</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2' : 'p-2'}">${item.pep ? item.pep : `-`}</td>
                                <td class="${key % 2 !== 0 ? 'even-line p-2 actions' : 'p-2 actions'}">
                                    <a href="${MontaLink(`solicitacoes/visualizar.html?id=${item.id}`)}">
                                        <i class="fa-solid fa-magnifying-glass-plus" id="visualizar" tipo="solicitacoes" identificador="${item.id}"></i>
                                    </a>
                                    <a href="${MontaLink(`solicitacoes/editar.html?id=${item.id}`)}">
                                        <i class="fa-solid fa-pen-to-square" id="editar" tipo="solicitacoes" identificador="${item.id}"></i>
                                    </a>
                                    <i class="fa-solid fa-trash-can" id="excluir" tipo="solicitacoes" identificador="${item.id}"></i>
                                </td>
                            </tr>`;
                        });

                        exibir_tabela += `</tdoby></table>`;

                        // Atualizar o elemento HTML com a lista de origens
                        if (document.getElementById("listagem-de-solicitacoes") !== null) {
                            const listaOrigens = document.getElementById("listagem-de-solicitacoes");
                            listaOrigens.innerHTML = exibir_tabela;
                            console.log(`Lista de solicitações com filtros carregada!`);
                        }
                        
                    }).catch(e => {
            
                        console.error(`Erro ao carregar a lista de solicitações com filtros!\n Mensagem: ${e}`);
                
                    })

                } else {
                    
                    if (document.getElementById("listagem-de-solicitacoes") !== null) {
                        const listaOrigens = document.getElementById("listagem-de-solicitacoes");
                        listaOrigens.innerHTML = '<p class="text-center text-italic p-3">Não há solicitações cadastradas!</p>';
                        console.log(`Não há solicitações cadastradas para os filtros!`);
                    }

                }
            })
            .catch(e => {
                console.error(`Erro ao tentar contar os registros da lista de solicitações cadastradas para os filtros!\n Mensagem: `,e);
            });

        })
        .catch(e => {
            console.error(`Erro ao tentar listar os tipos de documentos para as solicitações cadastradas para os filtros!\n Mensagem: `,e);
        });

    })
    .catch(e=>{
        console.error(`Erro ao tentar listar as origens para as solicitações cadastradas para os filtros!\n Mensagem: `,e);
    });
}