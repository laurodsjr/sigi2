import { DB } from "../init-indexeddb";

export function OrigensDeSolicitacoes() {
    DB.origem.count()
    .then(registros => {
        if (registros < 1) {
            DB.origem.bulkAdd([
                {titulo: 'ANEEL', descricao: ''},
                {titulo: 'ONS', descricao: ''},
                {titulo: 'Interna', descricao: 'Feita por Furnas mesmo!'}
            ]).then((e) => {
    
                console.log(`Origens para solicitações carregadas!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar origens para solicitações! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em origens para solicitações!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de origens para solicitações!`);
    });
}