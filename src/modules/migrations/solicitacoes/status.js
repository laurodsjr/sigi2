import { DB } from "../init-indexeddb";

export function StatusDeSolicitacoes() {
    DB.status_da_solicitacao.count()
    .then(registros => {
        if (registros < 1) {
            DB.status_da_solicitacao.bulkAdd([
                {titulo: 'Nova', descricao: 'Opção padrão'},
                {titulo: 'Em análise', descricao: ''},
                {titulo: 'Aprovada', descricao: ''},
                {titulo: 'Rejeitada', descricao: ''}
            ]).then((e) => {
    
                console.log(`Status para solicitações carregadas!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar status para solicitações! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em status para solicitações!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de status para solicitações!`);
    });
}