import { DB } from "../init-indexeddb";

export function CarregaTiposDeSolicitacoes() {
    DB.tipo_de_solicitacao.count()
    .then(registros => {
        if (registros < 1) {
            DB.tipo_de_solicitacao.bulkAdd([
                {titulo: 'R.E.A.', descricao: ''},
                {titulo: 'Despacho', descricao: ''},
                {titulo: 'Ofício', descricao: ''},
                {titulo: 'Carta', descricao: ''},
                {titulo: 'PMI', descricao: ''}
            ]).then((e) => {
    
                console.log(`Tipos de solicitações carregados!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar tipos de solicitações! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em tipos de solicitações!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de tipos de solicitações!`)
    });
}