import { DB } from "../init-indexeddb";

export function InserirUsuario() {
    DB.usuarios.count()
    .then(registros => {
        if (registros < 1) {
            DB.usuarios.bulkAdd([
                {nome: 'Alfredo Martins', email: 'fc111111@furnas.com.br', matricula: 'fc111111', senha: '123', funcao: 1, status: 1},
                {nome: 'José Silva', email: 'fc222222@furnas.com.br', matricula: 'fc222222', senha: '123', funcao: 2, status: 1},
                {nome: 'Maria Oliveira', email: 'fc333333@furnas.com.br', matricula: 'fc333333', senha: '123', funcao: 3, status: 1},
                {nome: 'Jertrudes Souza', email: 'fc444444@furnas.com.br', matricula: 'fc444444', senha: '123', funcao: 4, status: 1},
                {nome: 'Antônio Guedes', email: 'fc555555@furnas.com.br', matricula: 'fc555555', senha: '123', funcao: 1, status: 1},
                {nome: 'Joana Firmino', email: 'fc666666@furnas.com.br', matricula: 'fc666666', senha: '123', funcao: 2, status: 1},
                {nome: 'Anna Cunha', email: 'fc777777@furnas.com.br', matricula: 'fc777777', senha: '123', funcao: 3, status: 1},
                {nome: 'Margateth Fagundes', email: 'fc888888@furnas.com.br', matricula: 'fc888888', senha: '123', funcao: 4, status: 1}
            ]).then((e) => {
    
                console.log(`Funções iniciais carregadas!`,e);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar funções de usuários! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em usuários!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros usuários pré-cadastradas!`);
    });
}