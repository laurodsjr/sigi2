import { DB } from "../init-indexeddb";

export function InserirFuncaoDeUsuario(
    funcoes_iniciais = true,
    tipo_de_insercao = 0, 
    insercao_unica = {
        titulo: '',
        descricao: ''
    }, 
    insercao_multipla = []
) {

    if (funcoes_iniciais) {

        /**
         * Inserindo as funções básicas
         */

        DB.funcoes_de_usuarios.count()
        .then(registros => {
            if (registros < 1) {

                DB.funcoes_de_usuarios.bulkAdd([
                    {titulo: 'Administrador', descricao: ''},
                    {titulo: 'Gestor', descricao: ''},
                    {titulo: 'Engenheiro', descricao: ''},
                    {titulo: 'Gerente', descricao: ''},
                    {titulo: 'Anônimo', descricao: 'Função coringa apenas para aprovação de usuário!'},
                    {titulo: 'Coordenador', descricao: ''},
                ]).then((e) => {
        
                    console.log(`Funções iniciais carregadas!`);
              
                }).catch(e => {
        
                    console.error(`Erro ao carregar funções de usuários! \n Mensagem: ${e}`);
              
                })
    
            } else {
                console.log(`Nada a carregar de novo!`);
            }
        })
        .catch(e => {
            console.error(`Erro ao tentar contar os registros de funções pré-cadastradas!`)
        });


    } else {

        /**
         * Verificando o tipo de inserção
         *  Se o valor for 0 => inserção única
         *  Se o valor for 1 => inserção em massa
         */

        if (tipo_de_insercao == 0) {
            /**
             * Inserindo por dados enviados no cabeçalho
             * 
             */
            if (insercao_unica.titulo != '') {
                DB.funcoes_de_usuarios.add(insercao_unica).then(() => {
        
                    console.log(`Função "${insercao_unica.titulo}" inserida com sucesso!`);
              
                }).catch(e => {
        
                    console.error(`Erro ao carregar função "${insercao_unica.titulo}"! \n Mensagem: ${e}`);
              
                })
            } else {
                console.error(`Ops, você o título não está definido!`);
            }
        }

        if (tipo_de_insercao == 1) {
            /**
             * Inserindo por dados enviados no cabeçalho
             * 
             */
            if (insercao_multipla.length > 0) {

                DB.funcoes_de_usuarios.bulkAdd(insercao_multipla).then(() => {
        
                    console.log(`Funções em lote carregadas!`);
              
                }).catch(e => {
        
                    console.error(`Erro ao carregar funções de usuário em lote! \n Mensagem: ${e}`);
              
                })

            } else {
                console.error(`Ops, o conjunto está vazio!`);
            }
        }        

    }

}