import { DB } from "../init-indexeddb";

export function StatusDeVisitasTecnicas() {
    DB.status_da_reuniao.count()
    .then(registros => {
        if (registros < 1) {
            DB.status_da_reuniao.bulkAdd([
                {titulo: 'Programada', descricao: 'Padrão inicial'},
                {titulo: 'Adiada', descricao: ''},
                {titulo: 'Realizada', descricao: ''},
                {titulo: 'Atrasada', descricao: ''}
            ]).then((e) => {
    
                console.log(`Status para reuniões carregadas!`);
          
            }).catch(e => {
    
                console.error(`Erro ao carregar status para reuniões! \n Mensagem: ${e}`);
          
            })
        } else {
            console.log(`Nada a carregar de novo em status para reuniões!`);
        }
    })
    .catch(e => {
        console.error(`Erro ao tentar contar os registros de status para reuniões!`);
    });
}