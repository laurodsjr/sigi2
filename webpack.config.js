const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/main.js', // Arquivo de entrada principal
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js' // Nome do arquivo de saída
  }
};
